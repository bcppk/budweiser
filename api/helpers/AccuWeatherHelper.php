<?php
namespace app\helpers;

use Yii;


class AccuWeatherHelper
{
    public function getLocationDetails($lat,$lng)
    {
        $url = 'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=AawputallCyQ8TwX0qorDTbkkuwCida3&q='.$lat.'%2C'.$lng.'';
        $response = $this->httpGet($url);
        return $response;
    }

    public function getWeatherDetails($location_key)
    {
        $url = 'http://dataservice.accuweather.com/currentconditions/v1/'.$location_key.'?apikey=AawputallCyQ8TwX0qorDTbkkuwCida3';
        $response = $this->httpGet($url);
        return $response;
    }

    public function getLocationDetailsTemp($lat,$lng)
    {
        $url = 'http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=JzlCA0DN1sd1fSJHIvE0vqbeo8vi4fWa&q='.$lat.'%2C'.$lng.'';
        $response = $this->httpGet($url);
        return $response;
    }

    public function getWeatherDetailsTemp($location_key)
    {
        $url = 'http://dataservice.accuweather.com/currentconditions/v1/'.$location_key.'?apikey=JzlCA0DN1sd1fSJHIvE0vqbeo8vi4fWa';
        $response = $this->httpGet($url);
        return $response;
    }



    public function httpGet($url)
    {
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        //  curl_setopt($ch,CURLOPT_HEADER, false);

        $output=curl_exec($ch);

        curl_close($ch);
        return $output;
    }

}