<?php
namespace app\helpers;

use Yii;


class AppHelper
{
    public function getUniqueRandomNum(){
        function make_seed()
        {
            list($usec, $sec) = explode(' ', microtime());
            return $sec + $usec * 1000000;
        }
        srand(make_seed());
        $randval = mt_rand(100000,999999);
        return $randval;
    }

    public function getUniqueAlphaNumCode(){
        $unique_code = md5(uniqid(rand(), true));
        return $unique_code;
    }


}