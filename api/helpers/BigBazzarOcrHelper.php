<?php
namespace app\helpers;

use Yii;

class BigBazzarOcrHelper {

    private $error;
    private $text;

    function __constructor(){
        $this->error 	= false;
    }

    public function isError(){
        if($this->error){
            return true;
        }
        return false;
    }

    public function getError(){
        return $this->error;
    }

    public function convertType($string){

        $realstring = $string;

        $val['storeType']			 = 'Big Bazzar';
        $val['store_id']			 = 3;
        $val['invoiceno'] 			 = $this->invNo($realstring);
        $val['full_invoiceno'] 		 = $this->fullInvNo($realstring);
        $val['customer_mobileno']	 = $this->getCustomerMobNo($realstring);
        $val['date'] 	 			 = $this->billDate($realstring);
        $val['storeaddress'] 	 	 = $this->getBigBazzarStoreAddress($realstring);
        $val['GST']		 	 		 = $this->getGSTTIN($realstring);
        $val['CIN']		 	 		 = $this->getCIN($realstring);
        $val['itemdetails']			 = $this->getItemDetails($realstring);
        $val['total_price']			 = $this->getTotalPrice($realstring);
        $val['tax_amount']		 	 = $this->getTaxAmount($realstring);
        $val['item_full_details']	 = $this->getItemDetailsFulldata($realstring);

        return $val;
    }


    public function getCustomerMobNo($str){
        $customer_section = '';
        $string = preg_replace('/[^A-Za-z0-9\-\s\/]/', '', $str);
        $customer_section = $this->strafter($string,'Mobile NO');
        if (preg_match('/MI[^\n]+/m', $customer_section, $matches)){
            $substring		= $matches[0];
            $customer_section = preg_replace("/MI/i", "", $substring);
        }
        if(preg_match('/[0-9]{10}/', $customer_section, $matches)){
            return $matches[0];
        }
        else{
            return $customer_section;
        }
    }


    public function invNo($string){

        if (preg_match('/MEMO N0.[^\s]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/MEMO N0./i", "", $substring);
            return $substring;
        }
        else if(preg_match('/MEMO NO.[^\s]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/MEMO NO./i", "", $substring);
            return $substring;
        }

    }

    public function fullInvNo($string){

        if (preg_match('/Tax Invoice Number[^\s]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/Tax Invoice Number/i", "", $substring);
            return $substring;
        }
        else if (preg_match('/Tax Invoice Number [^\s]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/Tax Invoice Number/i", "", $substring);
            return $substring;
        }

    }

    public function billDate($string){

        if (preg_match('/St:[^St]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/St:/i", "", $substring);
            $substring = preg_replace("/St:[^\s]+/m", "", $substring);
            $substring = preg_replace("/St:[^\n]+/m", "", $substring);
            $substring = preg_replace('/[*+]+/m', '', $substring);

            $substring_before_g = $this->strbefore($substring,'G');

            if(!isset($substring_before_g))
                $substring = $this->strbefore($substring,'N');
            else
                $substring = $substring_before_g;

            if(preg_match('/[0-9][^\s]+/m',$substring,$matches)){
                $substring 	= $this->strafter($substring,$matches[0]);
            }

            return trim($substring);
        }


    }


    public function getGSTTIN($string){

        if (preg_match('/GST TIN:[^\n]+/m', $string, $matches)){
            $substring = $matches[0];
            $substring = preg_replace("/GST TIN:/i", "", $substring);
            $substring = $this->strbefore($substring,'W.E.F');
            return $substring;
        }
        else if (preg_match('/GST TIN: [^\n]+/m', $string, $matches)){
            $substring = $matches[0];
            $substring = preg_replace("/GST TIN:/i", "", $substring);
            $substring = $this->strbefore($substring,'W.E.F');
            return $substring;
        }
    }

    public function getCIN($string){

        if (preg_match('/CIN NO:[^\s]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/CIN NO:/i", "", $substring);
            return $substring;
        }
    }

    public function getBigBazzarStoreAddress($string){
        $stroaddress = $this->strafter($string,'SUPPLY');
        if(!isset($stroaddress))
            $stroaddress = $this->strafter($string,'SURLY');

        $stroaddress = $this->strbefore($stroaddress,'GST');

        return trim($stroaddress);
    }

    public function getItemDetails($string){

        $itemdetails = $this->strafter($string,'Rs.');
        if(!isset($itemdetails))
            $itemdetails = $this->strafter($string,'Rs');
        if(isset($itemdetails)){
            $itemdetails_sub = $this->strbefore($itemdetails,'SUBTOTAL');
            if(!isset($itemdetails_sub))
                $itemdetails = $this->strbefore($itemdetails,'TOTAL');
            else
                $itemdetails = $itemdetails_sub;
        }

        $items_array     = preg_split("/((?<!\\\|\r)\n)|((?<!\\\)\r\n)/", $itemdetails);
        $ItemDetails 	 = array();
        foreach($items_array AS $value){
            $data =  $this->isLetter($value);
            if($data){
                $replace_value 	   = preg_replace('/([0-9]+\.[0-9]+)/', '', $value);
                $dot_replace_value = preg_replace('/[.-]+/m', '', $replace_value);
                $ItemDetails[] 	   = $dot_replace_value;
            }

        }

        return $ItemDetails;
    }

    public function getItemDetailsFulldata($string){

        $itemdetails = $this->strafter($string,'Rs.');
        if(!isset($itemdetails))
            $itemdetails = $this->strafter($string,'Rs');
        if(isset($itemdetails)){
            $itemdetails_sub = $this->strbefore($itemdetails,'SUBTOTAL');
            if(!isset($itemdetails_sub))
                $itemdetails = $this->strbefore($itemdetails,'TOTAL');
            else
                $itemdetails = $itemdetails_sub;
        }


        return $itemdetails;
    }

    public function isLetter($string) {
        return preg_match('/^[a-zA-z]/i', trim($string));
    }


    public function getTotalPrice($string){

        $total_price_string = $this->strafter($string,'SUBTOTAL');

        if (preg_match('/([0-9]+\.[0-9]+)/', $total_price_string, $matches)){
            $substring		= $matches[0];

            return round(floatval($substring),2);
        }

    }

    public function getTaxAmount($string){
        $total_price_string = $this->strafter($string,'SUBTOTAL');
        $tax_amount_string 	=  $this->strafter($total_price_string,'CGST');
        $tax_amount_string 	=  $this->strbefore($tax_amount_string,'SGST');

        if (preg_match_all('/([0-9]+\.[0-9]+)/', $tax_amount_string, $matches)){
            $substring['base_amount'] 	   = isset($matches[0][0])?$matches[0][0]:$tax_amount_string;
            $substring['cgst_tax_amount']  = isset($matches[0][1])?$matches[0][1]:$tax_amount_string;
            $substring['sgst_tax_amount']  = isset($matches[0][1])?$matches[0][1]:$tax_amount_string;

            return $substring;
        }

    }

    public function strbefore($string, $substring) {

        $pos = strpos($string, $substring);
        if ($pos === false)
            return NULL;
        else
            return(substr($string, 0, $pos));
    }

    public function strafter($string, $substring) {

        $pos = strpos($string, $substring);
        if ($pos === false)
            return NULL;
        else
            return(substr($string, $pos+strlen($substring)));
    }
	
}

?>