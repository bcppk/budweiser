<?php
namespace app\helpers;

use Yii;

class CromaOcrHelper {

    private $error;
    private $text;

    function __constructor(){
        $this->error 	= false;
    }

    public function isError(){
        if($this->error){
            return true;
        }
        return false;
    }

    public function getError(){
        return $this->error;
    }

    public function convertType($string){

        $realstring = $string;

        $val['storeType']			 = 'Croma';
        $val['store_id']			 = 1;
        $val['customername']		 = $this->customerName($realstring);
        $val['customer_mobileno']	 = $this->getCustomerMobNo($realstring);
        $val['invoiceno'] 			 = $this->invNo($realstring);
        $val['date'] 	 			 = $this->billDate($realstring);
        $val['total_price']			 = $this->getTotalPrice($realstring);
        $val['credit_cardno']		 = $this->creditCardInfo($realstring);
        $val['cash_mode']			 = $this->cashInfo($realstring);
        $val['giftcard_details']	 = $this->giftCardDetails($realstring);
        $val['item_name']			 = $this->getItemName($realstring);
        $val['item_code']			 = $this->getItemCode($realstring);
        $val['tax_amount']		 	 = $this->getTaxAmount($realstring);
        $val['storeaddress'] 	 	 = $this->getCromaStoreAddress($realstring);
        $val['itemdetails']			 = $this->getItemDetails($realstring);
        $val['CIN']		 	 		 = $this->getCIN($realstring);

        return $val;
    }

    public function customerName($str){

        $string = preg_replace('/[^A-Za-z0-9\-\s\/]/', '', $str);

        preg_match('/\s+Customer Name\s+(?P<val>\w+\s\w+)/i', $string, $matches);
        if(!isset($matches['val']))
            preg_match('/\s+Custoner Name\s+(?P<val>\w+)/i', $string, $matches);
        if(!isset($matches['val']))
            preg_match('/\s+Custoner Nane\s+(?P<val>\w+)/i', $string, $matches);
        if(!isset($matches['val']))
            preg_match('/\s+Nan\s+(?P<val>\w+)/i', $string, $matches);
        if(!isset($matches['val']))
            preg_match('/\s+Custoner\s+(?P<val>\w+)/i', $string, $matches);
        if(isset($matches['val'])){
            $after_first_name = $this->strafter($str,$matches['val']);
            if (preg_match('/[^ \s]+/m', $after_first_name, $matches1)){
                $substring		= isset($matches1[0])?$matches1[0]:'';
                $name =$matches['val'].' '.$substring;
                return trim($name);
            }
            else{
                return $matches['val'];
            }
        }
    }

    public function getCustomerMobNo($str){
        $string = preg_replace('/[^A-Za-z0-9\-\s\/]/', '', $str);
        $customer_section = $this->strafter($string,'Created');
        if(!isset($customer_section))
            $customer_section = $this->strafter($string,'Tine');
        if(!isset($customer_section))
            $customer_section = $this->strafter($string,'Time');
        if(!isset($customer_section))
            $customer_section = $this->strafter($string,'Tre');

        if(preg_match('/[0-9]{10}/', $customer_section, $matches)){
            return $matches[0];
        }
    }


    public function invNo($string){

        if (preg_match('/InuNo:[^\s]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/InuNo:/i", "", $substring);
            return $substring;
        }
        else if(preg_match('/InvNo:[^\s]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/InvNo:/i", "", $substring);
            return $substring;
        }
        else if(preg_match('/Inulo:[^\s]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/Inulo:/i", "", $substring);
            return $substring;
        }
        else if(preg_match('/Inilo:[^\s]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/Inilo:/i", "", $substring);
            return $substring;
        }


    }

    public function billDate($string){

        if (preg_match('/Tine:[^Till]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/Tine:/i", "", $substring);
            $substring = preg_replace("/Inv[^\n]+/m", "", $substring);
            $substring = preg_replace("/Inu[^\n]+/m", "", $substring);
            $substring = preg_replace("/Created [^\n]+/m", "", $substring);
            return $substring;
        }
        else if(preg_match('/Time:[^ill]+/m', $string, $matches)){

            $substring		= $matches[0];
            $substring = preg_replace("/Time:/i", "", $substring);
            $substring = preg_replace("/Inv[^\n]+/m", "", $substring);
            $substring = preg_replace("/Inu[^\n]+/m", "", $substring);
            $substring = preg_replace("/Created [^\n]+/m", "", $substring);
            return $substring;
        }
        else if(preg_match('/Tine:[^Bill]+/m', $string, $matches)){

            $substring		= $matches[0];
            $substring = preg_replace("/Tine:/i", "", $substring);
            $substring = preg_replace("/Inv[^\n]+/m", "", $substring);
            $substring = preg_replace("/Inu[^\n]+/m", "", $substring);
            $substring = preg_replace("/Created [^\n]+/m", "", $substring);
            return $substring;
        }
        else if(preg_match('/Time:[^ill]+/m', $string, $matches)){

            $substring		= $matches[0];
            $substring = preg_replace("/Time:/i", "", $substring);
            $substring = preg_replace("/Inv[^\n]+/m", "", $substring);
            $substring = preg_replace("/Inu[^\n]+/m", "", $substring);
            $substring = preg_replace("/Created [^\n]+/m", "", $substring);
            return $substring;
        }
        else if(preg_match('/Tre:[^Till]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/Tre:/i", "", $substring);
            $substring = preg_replace("/Inv[^\n]+/m", "", $substring);
            $substring = preg_replace("/Inu[^\n]+/m", "", $substring);
            $substring = preg_replace("/Created [^\n]+/m", "", $substring);
            return $substring;
        }
        else if(preg_match('/Tre:[^ill]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/Tre:/i", "", $substring);
            $substring = preg_replace("/Inv[^\n]+/m", "", $substring);
            $substring = preg_replace("/Inu[^\n]+/m", "", $substring);
            $substring = preg_replace("/Created [^\n]+/m", "", $substring);
            return $substring;
        }

    }

    public function getCromaStoreAddress($string){

        $stroaddress = $this->strbefore($string,'InuNo');

        if(!isset($stroaddress))
            $stroaddress = $this->strbefore($string,'InvNo');
        if(!isset($stroaddress))
            $stroaddress = $this->strbefore($string,'Inulo');
        if(!isset($stroaddress))
            $stroaddress = $this->strbefore($string,'Inilo');


        return trim($stroaddress);
    }

    public function getItemDetails($string){

        $itemdetails = $this->strafter($string,'Custoner Region');
        if(!isset($itemdetails))
            $itemdetails = $this->strafter($string,'Customer Region');
        if(!isset($itemdetails))
            $itemdetails = $this->strafter($string,'Custoner Regian');
        if(!isset($itemdetails))
            $itemdetails = $this->strafter($string,'Customer Regian');
        if(!isset($itemdetails))
            $itemdetails = $this->strafter($string,'IMEI#');
        if(!isset($itemdetails))
            $itemdetails = $this->strbefore($itemdetails,'Total');
        if(!isset($itemdetails))
            $itemdetails = $this->strbefore($itemdetails,'otal');



        return str_replace('\t',',',$itemdetails);

    }

    public function getItemName($string){

        $fullItemdetails = $this->getItemDetails($string);
        $items_array     = preg_split("/((?<!\\\|\r)\n)|((?<!\\\)\r\n)/", $fullItemdetails);

        $item_name		 = (isset($items_array[0])&& !empty($items_array[0]))?$items_array[0]:(isset($items_array[1])?$items_array[1]:'');

        return $item_name;
    }

    public function getItemCode($string){

        $item_name 		 = $this->getItemName($string);
        if(isset($item_name) && !empty($item_name))
        {
            $after_item_name = $this->strafter($string,$item_name);
            if (preg_match('/[^ ]+/m', $after_item_name, $matches)){
                $substring		= $matches[0];
                return (INT)trim($substring);
            }
        }
    }

    public function getTotalPrice($string){

        if (preg_match('/Total:[^ ]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/Total:/i", "", $substring);

            return round(floatval($substring),2);
        }
        if (preg_match('/Total :[^ ]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/Total :/i", "", $substring);

            return round(floatval($substring),2);
        }
        if (preg_match('/otal[^ ]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/otal/i", "", $substring);

            return round(floatval($substring),2);
        }
        if (preg_match('/otal:[^ ]+/m', $string, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/otal:/i", "", $substring);

            return round(floatval($substring),2);
        }
    }

    public function creditCardInfo($string){

        $paymentSection = $this->strafter($string,'Card No:');
        if (preg_match('/x[^\s]+/m', $paymentSection, $matches)){
            $substring		= $matches[0];
            $substring = preg_replace("/Tax/i", "", $substring);
            return $substring;
        }
        else if(preg_match('/^[0-9]+/m', $paymentSection, $matches)){
            return $matches[0];
        }

    }

    public function cashInfo($string){

        $cash_section = [];
        $cashpaidSection = $this->strafter($string,'Cash');
        if(isset($cashpaidSection))
        {
            if (preg_match('/[0-9]+/m', $cashpaidSection, $matches)){
                $cash_section['paid_cash']= $matches[0];
            }
        }
        $cashreturnSection = $this->strafter($string,'Returned:');
        if(isset($cashreturnSection))
        {

            $giftcashreturnSection = $this->strbefore($cashreturnSection,'Gift');

            if(!isset($giftcashreturnSection))
                $giftcashreturnSection = $this->strbefore($cashreturnSection,'Tax');


            if(preg_match('/[^ ]*$/', $giftcashreturnSection, $results)){

                $str					  	   = trim($results[0]);
                $return_section_array    	   = preg_split("/((?<!\\\|\r)\n)|((?<!\\\)\r\n)/", $str);
                $return_amount	  		  	   = isset($return_section_array[2])?$return_section_array[2]:(isset($return_section_array[1])?$return_section_array[1]:'');

                $cash_section['changes_return']= (float)$return_amount;
            }

        }
        return $cash_section;
    }

    public function giftCardDetails($string){

        $giftCardSection = $this->strafter($string,'Gift');
        if(isset($giftCardSection))
            $giftCardSection = $this->strbefore($giftCardSection,'Tax');

        return trim($giftCardSection);
    }

    public function getTaxAmount($string){

        $cin_section = false;
        $taxpaymentSection = $this->strbefore($string,'CIN');
        if(!isset($taxpaymentSection))
        {
            $taxpaymentSection = $this->strafter($string,'TAXES');
            $cin_section = true;
        }

        if(preg_match('/[^ ]*$/', $taxpaymentSection, $results)){
            $str=trim($results[0]);
            $tax_array     = preg_split("/((?<!\\\|\r)\n)|((?<!\\\)\r\n)/", $str);
            if($cin_section)
                $tax_amount	   = isset($tax_array[1])?$tax_array[1]:$tax_array[0];
            else
                $tax_amount	   = isset($tax_array[2])?$tax_array[2]:(isset($tax_array[1])?$tax_array[1]:$tax_array[0]);

            return $tax_amount;
        }
    }

    public function getCIN($string){

        $str = preg_replace('/[^A-Za-z0-9\-\s\/]/', '', $string);
        if(preg_match('/\s+CIN\s+(?P<val>\w+)/i', $str, $matches)){
            return $matches['val'];
        }
    }


    public function strbefore($string, $substring) {

        $pos = strpos($string, $substring);
        if ($pos === false)
            return NULL;
        else
            return(substr($string, 0, $pos));
    }

    public function strafter($string, $substring) {

        $pos = strpos($string, $substring);
        if ($pos === false)
            return NULL;
        else
            return(substr($string, $pos+strlen($substring)));
    }
	
}

?>