<?php
namespace app\helpers;

use Yii;
use app\helpers\OcrImagetoTextHelper;

class GetOcrDataHelper
{
    function ocrStep1($billsanp_id)
    {
        $query = "SELECT Billsnap.id AS billsnap_id ,
						 Billsnap.bill_image AS image,
						 Billsnap.status
				FROM billsnap AS Billsnap
				WHERE Billsnap.id = ".$billsanp_id." 
				      AND Billsnap.status = 0
				ORDER BY Billsnap.id";

        $result = Yii::$app->db->createCommand($query)->queryAll();
        if(isset($result[0]) && !empty($result[0]))
        {
                $img   				= $result[0]['image'];
                $billsnap_id        = $result[0]['billsnap_id'];
                $file_size			= $this->getFileSize($img);
                if($file_size > 4024000 )
                {
                    $status_update = "UPDATE billsnap SET status=100,billsnap_comments='File Size Too Large!',updated_date=now() WHERE id =".$billsnap_id;
                    $query = Yii::$app->db->createCommand($status_update)->execute();
                }
                else
                {
                    $response			= new OcrImagetoTextHelper();
                    $data		 		= $response->getData($img);
                     /*echo "<pre/>";
                    print_r($data);
                    exit;*/
                    if(isset($data['check_product']))
                    {
                        if($data['check_product']=='invalid_image')
                        {
                            $status_update = "UPDATE billsnap SET status=101,billsnap_comments='Invalid Bill!',updated_date=now() WHERE id =".$billsnap_id;
                            $query = Yii::$app->db->createCommand($status_update)->execute();
                        }
                        else if($data['check_product']=='yes')
                        {
                            $status_update 			= "UPDATE billsnap SET bill_raw_data = '".pg_escape_string($data['ocr_raw_data'])."' ,status=104,billsnap_comments='Product Matched',updated_date=now() WHERE id =".$billsnap_id;
                             $query = Yii::$app->db->createCommand($status_update)->execute();
                        }
                        else
                        {
                            $status_update 			= "UPDATE billsnap SET bill_raw_data = '".pg_escape_string($data['ocr_raw_data'])."' ,status=104,billsnap_comments='Product Not Matched',updated_date=now() WHERE id =".$billsnap_id;
                            $query = Yii::$app->db->createCommand($status_update)->execute();
                        }
                    }
                }
        }
        return 1;
    }



    function getFileSize($img){
        $ch = curl_init($img);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLOPT_NOBODY, TRUE);
        $data = curl_exec($ch);
        $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);

        curl_close($ch);
        return $size;
    }



}