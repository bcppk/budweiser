<?php

namespace app\helpers;

use Yii;

class OcrImagetoTextHelper {
    private $error;
    private $text;
    private $arrayType;

    function __constructor(){
        $this->error 	= false;
    }

    public function googleOcr($img){

        $api_key = 'AIzaSyD-U3o9uNFlk_kaF8QWHrn2oOeKrSS4HMw';
        $cvurl = "https://vision.googleapis.com/v1/images:annotate?key=" . $api_key;
        $type = "TEXT_DETECTION";

        $data = file_get_contents($img);
        $base64 = base64_encode($data);


        $r_json ='{
			"requests": [
				{
				  "image": {
					"content":"' . $base64. '"
				  },
				  "features": [
					  {
						"type": "' .$type. '",
						"maxResults": 200
					   }
				  ]
				}
			]
		}';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $cvurl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $r_json);
        $json_response = curl_exec($curl);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ( $status != 200 ) {
            die("Error: $cvurl failed status $status" );
        }


        $data = json_decode($json_response);
        if(isset($data->responses[0]->textAnnotations[0]->description) && !empty($data->responses[0]->textAnnotations[0]->description)){
            $this->text 	 = $data->responses[0]->textAnnotations[0]->description;
            $this->arrayType = preg_split("/((?<!\\\|\r)\n)|((?<!\\\)\r\n)/", $data->responses[0]->textAnnotations[0]->description);
        }else{
            $this->error = "No Text";
        }
    }

    public function getText(){
        return $this->text;
    }

    public function isError(){
        if($this->error){
            return true;
        }
        return false;
    }

    public function getError(){
        return $this->error;
    }

    public function getarrayType(){
        return $this->arrayType;
    }

    public function getData($img)
    {
        $this->googleOcr($img);
        $text=$this->getText();
        $data=[];
        /*echo "<pre/>";
        print_r($text);exit;*/
        if($text)
        {
            $string = preg_replace('/[^A-Za-z0-9\-\s\/]/', '', $text);

            if(preg_match('/budweiser/', strtolower($string), $matches)){
                $data['check_product'] = 'yes';
                $data['ocr_raw_data']  = $text;
                return $data;
            }
            else if(preg_match('/budwiser/', strtolower($string), $matches)){
                $data['check_product'] = 'yes';
                $data['ocr_raw_data']  = $text;
                return $data;
            }
            else if(preg_match('/budheiser/', strtolower($string), $matches)){
                $data['check_product'] = 'yes';
                $data['ocr_raw_data']  = $text;
                return $data;
            }
            else if(preg_match('/budneiser/', strtolower($string), $matches)){
                $data['check_product'] = 'yes';
                $data['ocr_raw_data']  = $text;
                return $data;
            }
            else if(preg_match('/budmeiser/', strtolower($string), $matches)){
                $data['check_product'] = 'yes';
                $data['ocr_raw_data']  = $text;
                return $data;
            }
            else if(preg_match('/budveiser/', strtolower($string), $matches)){
                $data['check_product'] = 'yes';
                $data['ocr_raw_data']  = $text;
                return $data;
            }
            else if(preg_match('/budveiser/', strtolower($string), $matches)){
                $data['check_product'] = 'yes';
                $data['ocr_raw_data']  = $text;
                return $data;
            }
            else if(preg_match('/budh/', strtolower($string), $matches)){
                $data['check_product'] = 'yes';
                $data['ocr_raw_data']  = $text;
                return $data;
            }
            else if(preg_match('/budw/', strtolower($string), $matches)){
                $data['check_product'] = 'yes';
                $data['ocr_raw_data']  = $text;
                return $data;
            }
            else if(preg_match('/budm/', strtolower($string), $matches)){
                $data['check_product'] = 'yes';
                $data['ocr_raw_data']  = $text;
                return $data;
            }
            else{
                $data['check_product'] = 'no';
                $data['ocr_raw_data']  = $text;
                return $data;
            }
        }
        else
        {
            $data['check_product']		 = 'invalid_image';
            return $data;
        }

    }


}

    
 
?>