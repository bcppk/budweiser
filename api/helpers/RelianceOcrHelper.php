<?php
namespace app\helpers;

use Yii;


class RelianceOcrHelper {
	 
	 private $error;
	 private $text;
	 
	 function __constructor(){
		$this->error 	= false;
	 }
	 
	public function isError(){
		if($this->error){
			return true;
		}
		return false;
	}
	
	public function getError(){
		return $this->error;
	}
	
	public function convertType($string){
		
		$realstring = $string;

		$val['customername']		 = $this->customerName($realstring);
		$val['customer_mobileno']	 = $this->getCustomerMobNo($realstring);
		$val['customer_email']		 = $this->getCustomerEmail($realstring);
		$val['supply_state_code']	 = $this->getSupplyStateCode($realstring);
		$val['customer_type']		 = $this->getCustomerType($realstring);
		$val['gstno']		 		 = $this->getGSTNo($realstring);
		$val['item_name']			 = $this->getItemName($realstring);
		
		
		
		/*$val['invoiceno'] 			 = $this->invNo($realstring);
		$val['date'] 	 			 = $this->billDate($realstring);
		$val['total_price']			 = $this->getTotalPrice($realstring);
		$val['credit_cardno']		 = $this->creditCardInfo($realstring);
		
		$val['item_code']			 = $this->getItemCode($realstring);
	 	$val['tax_amount']		 	 = $this->getTaxAmount($realstring); 
	
		*/
		$val['reliancestoreadd'] 	 = $this->getRelianceStoreAdd($realstring);
		

		return $val;
	 }  
	 
	 public function customerName($str){
		 
		$string = preg_replace('/[^A-Za-z0-9\-\s\/]/', '', $str); 
		preg_match('/\s+Customer Address\s+(?P<val>\w+\s\w+)/i', $string, $matches);
		if(!isset($matches['val']))
		preg_match('/\s+Custoner Addres\s+(?P<val>\w+)/i', $string, $matches);
		if(!isset($matches['val']))
		preg_match('/\s+Address\s+(?P<val>\w+)/i', $string, $matches);
		if(!isset($matches['val']))
		preg_match('/\s+Customer\s+(?P<val>\w+)/i', $string, $matches);
		if(isset($matches['val'])){
			$after_first_name = $this->strafter($str,$matches['val']);
			if (preg_match('/[^ \s]+/m', $after_first_name, $matches1)){
				$substring		= isset($matches1[0])?$matches1[0]:'';
				$name =$matches['val'].' '.$substring;
				return trim($name);
			}
			else{
				return $matches['val'];
			}
		}
	}
	
	public function getCustomerMobNo($str){
		
		$string = preg_replace('/[^A-Za-z0-9\-\s\/]/', '', $str); 
 
		$customer_section = $this->strafter($string,'Customer Address');
		if(!isset($customer_section))
			$customer_section = $this->strafter($string,'Custoner Addres');
		if(!isset($customer_section))
			$customer_section = $this->strafter($string,'Custoner Address');

		if(preg_match('/[0-9]{10}/', $customer_section, $matches)){
			return $matches[0];
		}
	}
	
	public function getCustomerEmail($str){
		
		if(preg_match('/[a-z0-9_\-\+]+@[a-z0-9\-]+\.([a-z]{2,3})(?:\.[a-z]{2})?/i', $str, $matches))
			return $matches[0];
	}
	
	public function getSupplyStateCode($str){
		
		$string = preg_replace('/[^A-Za-z0-9\-\s\/]/', '', $str); 
		preg_match('/\s+Place of Supply State Code\s+(?P<val>\w+\s\w+)/i', $string, $matches);
		if(!isset($matches['val']))
			preg_match('/\s+Place of Supply State\s+(?P<val>\w+)/i', $string, $matches);
		if(!isset($matches['val']))
			preg_match('/\s+Place of Supply\s+(?P<val>\w+)/i', $string, $matches);
		if(!isset($matches['val']))
			preg_match('/\s+Supply State Code\s+(?P<val>\w+)/i', $string, $matches);

		if(isset($matches['val'])){
			return trim($matches['val']);
		}
	}
	
	public function getCustomerType($str){
		
		$string = preg_replace('/[^A-Za-z0-9\-\s\/]/', '', $str); 
		preg_match('/\s+Customer Type\s+(?P<val>\w+\s)/i', $string, $matches);
		if(!isset($matches['val']))
			preg_match('/\s+Customer Typ\s+(?P<val>\w+\s)/i', $string, $matches);
		if(!isset($matches['val']))
			preg_match('/\s+Custoner Type\s+(?P<val>\w+\s)/i', $string, $matches);
		if(!isset($matches['val']))
			preg_match('/\s+Type\s+(?P<val>\w+\s)/i', $string, $matches);
		if(isset($matches['val'])){
			return trim($matches['val']);
		}
	}
	
	public function getGSTNo($str){
		
		$string = preg_replace('/[^A-Za-z0-9\-\s\/]/', '', $str); 
		preg_match('/\s+Supply State GSTN Number\s+(?P<val>\w+\s)/i', $string, $matches);
		if(!isset($matches['val']))
			preg_match('/\s+GSTN Number\s+(?P<val>\s)/i', $string, $matches);
		if(!isset($matches['val']))
			preg_match('/\s+Nusber\s+(?P<val>\w+\s)/i', $string, $matches);
		if(!isset($matches['val']))
			preg_match('/\s+GSTN Nusber+(?P<val>\w+\s)/i', $string, $matches);
		if(!isset($matches['val']))
			preg_match('/\s+upply State 6STN Nusber+(?P<val>\w+\s)/i', $string, $matches);

		if(isset($matches['val'])){
			preg_match('/[^Number]+/m', $matches['val'], $substring);
			return trim($substring[0]);
		}
	}

	
	public function invNo($string){
		
	 	if (preg_match('/InuNo:[^ ]+/m', $string, $matches)){
			$substring		= $matches[0];
			$substring = preg_replace("/InuNo:/i", "", $substring);
			return $substring;
		}
		else if(preg_match('/InvNo:[^ ]+/m', $string, $matches)){
			$substring		= $matches[0];
			$substring = preg_replace("/InvNo:/i", "", $substring);
			return $substring;
		}
	}
	
	public function billDate($string){
		
	 	if (preg_match('/Tine:[^ill]+/m', $string, $matches)){
			$substring		= $matches[0];
			$substring = preg_replace("/Tine:/i", "", $substring);
			return $substring;
		}
		else if(preg_match('/Time:[^Till]+/m', $string, $matches)){
			$substring		= $matches[0];
			$substring = preg_replace("/Time:/i", "", $substring);
			return $substring;
		}
		else if(preg_match('/Tine:[^Bill]+/m', $string, $matches)){
			$substring		= $matches[0];
			$substring = preg_replace("/Tine:/i", "", $substring);
			return $substring;
		}
		else if(preg_match('/Time:[^ill]+/m', $string, $matches)){
			$substring		= $matches[0];
			$substring = preg_replace("/Time:/i", "", $substring);
			return $substring;
		}
			
	}
	
	public function getRelianceStoreAdd($string){
		
		$stroaddress = $this->strbefore($string,'Tax');
		if(!isset($stroaddress))
			$stroaddress = $this->strbefore($string,'Tax Invcice');
		if(!isset($stroaddress))
			$stroaddress = $this->strbefore($string,'Tax Invoice');

		return trim($stroaddress);
	}
	
	public function getItemName($str){
	
		$itemnamesection = $this->strafter($str,'HSN/SAC');
		if(!isset($itemnamesection))
			$itemnamesection = $this->strafter($str,'HSN/SA');
		if(!isset($itemnamesection))
			$itemnamesection = $this->strafter($str,'ItemName HSN/SAC');
		
		$items_array     = preg_split("/((?<!\\\|\r)\n)|((?<!\\\)\r\n)/", $itemnamesection);
		
		$item_name		 = (isset($items_array[0])&& !empty($items_array[0]))?$items_array[0]:(isset($items_array[1])?$items_array[1]:'');
		
		$item_name		 = preg_replace("/[^ ]*$/", "", $item_name);
		

		return $item_name;
		 
	}
	
	public function getItemCode($string){
		
		$item_name 		 = $this->getItemName($string);
		$after_item_name = $this->strafter($string,$item_name);
		if (preg_match('/[^ ]+/m', $after_item_name, $matches)){
			$substring		= $matches[0];
			return trim($substring);
		}
	}
	
	public function getTotalPrice($string){
		
	 	if (preg_match('/Total:[^ ]+/m', $string, $matches)){
			$substring		= $matches[0];
			$substring = preg_replace("/Total:/i", "", $substring);
			$substring = preg_replace("/Credit/i", "", $substring);
			return trim($substring);
		}
	}
	
	public function creditCardInfo($string){
		
		$paymentSection = $this->strafter($string,'PAYMENT DETAILS');
		if (preg_match('/x[^ ]+/m', $paymentSection, $matches)){
			$substring		= $matches[0];
			$substring = preg_replace("/Tax/i", "", $substring);
			return $substring;
		}
		
	}
	
	public function getTaxAmount($string){
		
		$taxpaymentSection = $this->strbefore($string,'CIN');
		if(preg_match('/[^ ]*$/', $taxpaymentSection, $results)){
			$str=trim($results[0]);
			$tax_array     = preg_split("/((?<!\\\|\r)\n)|((?<!\\\)\r\n)/", $str);
			$tax_amount	   = isset($tax_array[1])?$tax_array[1]:$tax_array[0];
			return $tax_amount;
		}
	}
	
	
	public function strbefore($string, $substring) {
	 
	 $pos = strpos($string, $substring);
	  if ($pos === false)
	   return $string;
	  else  
	   return(substr($string, 0, $pos));
	}
	
	public function strafter($string, $substring) {
	  
	  $pos = strpos($string, $substring);
	  if ($pos === false)
	   return $string;
	  else  
	   return(substr($string, $pos+strlen($substring)));
	}
	
}

?>