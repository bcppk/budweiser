<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property integer $id
 * @property json $billsnap_data
 * @property string $billsnap_comments
 * @property string $bill_image
 * @property integer $status
 * @property integer $store_verify
 * @property integer $store_id
 * @property timestamp $created_date
 * @property timestamp $updated_date
 * @property integer $booking_id
 * @property string $invoice_no
 * @property string $store_name
 */
class Billsnap extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'billsnap';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bill_image'], 'required'],
            [['created_date','updated_date'], 'safe'],
            [['store_verify','store_id','booking_id'], 'integer'],
            [['billsnap_data','billsnap_comments','bill_image','invoice_no','store_name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bill_image' => 'Bill Image',
            'created_date' => 'Createdon',
        ];
    }

    public function checkDuplicateInvoice($invoice_no,$store_name){
        $sql = "SELECT id 
                FROM billsnap 
                WHERE UPPER(invoice_no) = '".strtoupper($invoice_no)."' 
                      AND UPPER(store_name) = '".strtoupper($store_name)."' ";
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return isset($data[0]['id'])?$data[0]['id']:0;
    }

}
