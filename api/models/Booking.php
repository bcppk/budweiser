<?php
namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "booking".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $customername
 * @property string $email
 * @property string $mobile
 * @property integer $city_id
 * @property timestamp $created_on
 * @property string $ipaddress
 * @property string $address
 * @property string $otp
 * @property integer $status
 * @property string $lat
 * @property string $lng
 * @property string $offercode
 * @property integer $state_id
 * @property string $tn_answer
 * @property integer $reason
 */
class Booking extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    const STATUS_UNCONFIRM  = 0;
    const STATUS_CONFIRM    = 1;


    public static function tableName()
    {
        return 'booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customername', 'email', 'mobile', 'customer_id','campaign_id','state_id','city_id'], 'required'],
            [['created_on'], 'safe'],
            [['customer_id','city_id','campaign_id','state_id','reason'], 'integer'],
            [['lat','lng','address','offercode','tn_answer'], 'string'],
            [['customername', 'email', 'ipaddress'], 'string', 'max' => 50],
            [['mobile'], 'string', 'min' => 10, 'max' => 10],
            ['status', 'default', 'value' => self::STATUS_UNCONFIRM],
            ['status', 'in', 'range' => [self::STATUS_UNCONFIRM, self::STATUS_CONFIRM]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'customername' => 'Customer Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'city_id'=>'City',
            'createdon' => 'Createdon',
            'ipaddress' => 'Ip Address',
        ];
    }

    public function getCustomerDataID($customer_id){
        $sql = "SELECT * FROM booking WHERE customer_id = ".$customer_id." ORDER BY id DESC ";
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        if(isset($data[0]) && !empty($data[0]))
            return $data[0];
        else
            return '';
    }

    public function getTotalRecords($campaign_id,$startdate,$enddate,$state_id){
        $sql = "SELECT COUNT(CustData.id) AS tot_count
                FROM customers AS Cust 
                      JOIN booking AS CustData ON (CustData.customer_id=Cust.id)
                      JOIN billsnap AS Billsnap ON (Billsnap.booking_id = CustData.id)
                WHERE CustData.campaign_id = ".$campaign_id."
                      AND Billsnap.status = 104
                      AND CustData.status = 1 ";
        if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate)){
            $sql.=" AND CustData.created_on::date BETWEEN '".$startdate."' AND '".$enddate."'  ";
        }

        if(isset($state_id) && !empty($state_id)){
            $sql   .= "  AND Custdata.state_id= ".$state_id." ";
        }
        else{
            $sql   .= "  AND CustData.state_id != ".Yii::$app->params['restrict_state_id']." ";
        }
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return isset($data[0]['tot_count'])?$data[0]['tot_count']:0;
    }

    public function getUnAssignedRecords($campaign_id,$startdate,$enddate,$state_id){
        $sql = "SELECT COUNT(CustData.id) AS tot_count
                FROM customers AS Cust 
                      JOIN booking AS CustData ON (CustData.customer_id=Cust.id)
                      JOIN billsnap AS Billsnap ON (Billsnap.booking_id = CustData.id)
                WHERE CustData.campaign_id = ".$campaign_id."
                      AND Billsnap.status = 104
                      AND CustData.status = 1 
                      AND CustData.internal_status = 0";
        if(isset($startdate) && !empty($startdate) && $startdate!=null  && isset($enddate) && !empty($enddate) && $enddate!=null){
            $sql.=" AND CustData.created_on::date BETWEEN '".$startdate."' AND '".$enddate."'  ";
        }

        if(isset($state_id) && !empty($state_id)){
            $sql   .= "  AND Custdata.state_id= ".$state_id." ";
        }
        else{
            $sql   .= "  AND CustData.state_id != ".Yii::$app->params['restrict_state_id']." ";
        }
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return isset($data[0]['tot_count'])?$data[0]['tot_count']:0;
    }

    public function getAssignedPendingRecords($campaign_id,$startdate,$enddate,$state_id){
        $sql = "SELECT COUNT(CustData.id) AS tot_count
                FROM customers AS Cust 
                      JOIN booking AS CustData ON (CustData.customer_id=Cust.id)
                      JOIN billsnap AS Billsnap ON (Billsnap.booking_id = CustData.id)
                WHERE CustData.campaign_id = ".$campaign_id."
                      AND Billsnap.status = 104
                      AND CustData.status = 1 
                      AND CustData.internal_status = 1";
        if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate)){
            $sql.=" AND CustData.created_on::date BETWEEN '".$startdate."' AND '".$enddate."'  ";
        }

        if(isset($state_id) && !empty($state_id)){
            $sql   .= "  AND Custdata.state_id= ".$state_id." ";
        }
        else{
            $sql   .= "  AND CustData.state_id != ".Yii::$app->params['restrict_state_id']." ";
        }
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return isset($data[0]['tot_count'])?$data[0]['tot_count']:0;
    }

    public function getAssignedProcessedRecords($campaign_id,$startdate,$enddate,$state_id){
        $sql = "SELECT COUNT(CustData.id) AS tot_count
                FROM customers AS Cust 
                      JOIN booking AS CustData ON (CustData.customer_id=Cust.id)
                      JOIN billsnap AS Billsnap ON (Billsnap.booking_id = CustData.id)
                WHERE CustData.campaign_id = ".$campaign_id."
                      AND Billsnap.status = 104
                      AND CustData.status = 1 
                      AND CustData.internal_status = 2";
        if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate)){
            $sql.=" AND CustData.created_on::date BETWEEN '".$startdate."' AND '".$enddate."'  ";
        }

        if(isset($state_id) && !empty($state_id)){
            $sql   .= "  AND Custdata.state_id= ".$state_id." ";
        }
        else{
            $sql   .= "  AND CustData.state_id != ".Yii::$app->params['restrict_state_id']." ";
        }
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return isset($data[0]['tot_count'])?$data[0]['tot_count']:0;
    }

    public function getRejectedRecords($campaign_id,$startdate,$enddate,$state_id){
        $sql = "SELECT COUNT(CustData.id) AS tot_count
                FROM customers AS Cust 
                      JOIN booking AS CustData ON (CustData.customer_id=Cust.id)
                      JOIN billsnap AS Billsnap ON (Billsnap.booking_id = CustData.id)
                WHERE CustData.campaign_id = ".$campaign_id."
                      AND Billsnap.status = 104
                      AND CustData.status = 1 
                      AND CustData.internal_status = 3";
        if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate)){
            $sql.=" AND CustData.created_on::date BETWEEN '".$startdate."' AND '".$enddate."'  ";
        }

        if(isset($state_id) && !empty($state_id)){
            $sql   .= "  AND Custdata.state_id= ".$state_id." ";
        }
        else{
            $sql   .= "  AND CustData.state_id != ".Yii::$app->params['restrict_state_id']." ";
        }
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return isset($data[0]['tot_count'])?$data[0]['tot_count']:0;
    }

    public function getBulkAssignDetailsList($campaign_id,$startdate,$enddate,$state_id){

        if(isset($startdate) && !empty($startdate)  && isset($enddate) && !empty($enddate) ){
            $condition1 =" AND CustData.created_on::date BETWEEN '".$startdate."' AND '".$enddate."'  ";
            $condition2 =" AND CustDataProcessed.created_on::date BETWEEN '".$startdate."' AND '".$enddate."'  ";
            $condition3 =" AND CustDataRejected.created_on::date BETWEEN '".$startdate."' AND '".$enddate."'  ";
        }
        else{
            $condition1 = NULL;
            $condition2 = NULL;
            $condition3 = NULL;
        }

        if(isset($state_id) && !empty($state_id)){
            $condition4 =  " AND CustData.state_id = ".$state_id." ";
            $condition5 =  " AND CustDataProcessed.state_id = ".$state_id." ";
            $condition6 =  " AND CustDataRejected.state_id = ".$state_id." ";
        }
        else{
            $condition4 = " AND CustData.state_id != ".Yii::$app->params['restrict_state_id']." ";
            $condition5 = " AND CustDataProcessed.state_id != ".Yii::$app->params['restrict_state_id']." ";
            $condition6 = " AND CustDataRejected.state_id != ".Yii::$app->params['restrict_state_id']." ";
        }

        $sql = "SELECT Users.id AS user_id ,
                       Users.username,
                       COUNT(CustAssignUser.id) AS assigned_count,
                       COUNT(CustData.id) AS pending_count,
                       COUNT(CustDataProcessed.id) AS closed_count,
                       COUNT(CustDataRejected.id) AS rejected_count
                FROM \"user\" AS Users
                LEFT JOIN customer_assigned_users AS CustAssignUser ON (CustAssignUser.allocated_user_id = Users.id)
                LEFT JOIN booking AS CustData ON (CustData.id = CustAssignUser.booking_id AND CustData.status=1 AND CustData.internal_status=1 AND CustData.campaign_id = ".$campaign_id." $condition4 $condition1 )
                LEFT JOIN billsnap AS Billsnap ON (Billsnap.booking_id = CustData.id AND Billsnap.status = 104)
                LEFT JOIN booking AS CustDataProcessed ON (CustDataProcessed.id = CustAssignUser.booking_id AND CustDataProcessed.status=1 AND CustDataProcessed.internal_status=2 AND CustDataProcessed.campaign_id = ".$campaign_id." $condition5 $condition2 )
                LEFT JOIN billsnap AS BillsnapProcessed ON (BillsnapProcessed.booking_id = CustDataProcessed.id AND BillsnapProcessed.status = 104)
                LEFT JOIN booking AS CustDataRejected ON (CustDataRejected.id = CustAssignUser.booking_id AND CustDataRejected.status=1 AND CustDataRejected.internal_status=3 AND CustDataRejected.campaign_id = ".$campaign_id." $condition6 $condition3 )
                LEFT JOIN billsnap AS BillsnapRejected ON (BillsnapRejected.booking_id = CustDataRejected.id AND BillsnapRejected.status = 104)
                WHERE Users.status = ".User::STATUS_ACTIVE."
                      AND Users.confirmed_at IS NOT NULL
                      AND Users.role = ".User::ROLE_USER."
                GROUP BY Users.id ,
                         Users.username 
                ORDER BY 1 ";
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return $data;
    }


    public function getReassignList($campaign_id,$startdate,$enddate,$user_id,$state_id){
        $sql = " SELECT  Custdata.id AS booking_id,
                         Users.id AS user_id,
                         Users.username,
                         CustAssignedUser.created_date AS assigned_on,
                         Custdata.customername,
                         Custdata.mobile,
                         Custdata.email,
                         Custdata.created_on AS booking_date,
                         Custdata.reason,
                         Billsnap.bill_image,
                         Billsnap.billsnap_comments,
                         Billsnap.invoice_no,
                         Billsnap.store_name,
                         State.statename,
                         Billsnap.qty,
                         Custdata.agent_comments,
                         Custdata.agent_comments_date
                FROM customer_assigned_users AS CustAssignedUser
                        JOIN booking AS Custdata ON (Custdata.id = CustAssignedUser.booking_id AND Custdata.status = 1 AND Custdata.internal_status =1 )
                        JOIN \"user\" AS Users ON (Users.id=CustAssignedUser.allocated_user_id)
                        JOIN billsnap AS Billsnap ON (Billsnap.booking_id=Custdata.id)
                        JOIN states AS State ON (State.id=Custdata.state_id)
                WHERE  Billsnap.status = 104
                       AND Custdata.campaign_id = ".$campaign_id;

        if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate)){
            $sql   .= "  AND CustAssignedUser.created_date::date BETWEEN '".$startdate."' AND '".$enddate."' ";
        }

        if(isset($state_id) && !empty($state_id)){
            $sql   .= "  AND Custdata.state_id= ".$state_id." ";
        }
        else{
            $sql   .= "  AND CustData.state_id != ".Yii::$app->params['restrict_state_id']." ";
        }

        if(isset($user_id) && !empty($user_id)){
            $sql   .= "  AND CustAssignedUser.allocated_user_id = ".$user_id;
        }

        $sql .= " ORDER BY CustAssignedUser.created_date , 
                         Users.id ,
                         Custdata.created_on";
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return $data;
    }

    public function getAgentAssignedList($campaign_id,$startdate,$enddate,$user_id){
        $sql = " SELECT Custdata.id AS booking_id,
                        Users.id AS user_id,
                        Users.username,
                        CustAssignedUser.created_date AS assigned_on,
                        Custdata.customername,
                        Custdata.mobile,
                        Custdata.email,
                        Custdata.created_on AS booking_date,
                        Custdata.reason,
                        Billsnap.bill_image,
                        Billsnap.billsnap_comments,
                        Billsnap.invoice_no,
                        Billsnap.store_name,
                        State.statename,
                        Billsnap.qty,
                        Custdata.agent_comments,
                        Custdata.agent_comments_date
                FROM customer_assigned_users AS CustAssignedUser
                        JOIN booking AS Custdata ON (Custdata.id = CustAssignedUser.booking_id AND Custdata.status = 1 AND Custdata.internal_status =1 )
                        JOIN \"user\" AS Users ON (Users.id=CustAssignedUser.allocated_user_id)
                        JOIN billsnap AS Billsnap ON (Billsnap.booking_id=Custdata.id)
                        JOIN states AS State ON (State.id=Custdata.state_id)
                WHERE CustAssignedUser.allocated_user_id = ".$user_id." 
                      AND Billsnap.status = 104
                      AND Custdata.campaign_id = ".$campaign_id;
        if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate)){
            $sql   .= "  AND CustAssignedUser.created_date::date BETWEEN '".$startdate."' AND '".$enddate."' ";
        }
        $sql .= " ORDER BY CustAssignedUser.created_date , 
                         Users.id ,
                         Custdata.created_on";
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return $data;
    }

    public function getBookingData($campaign_id,$booking_id){
        $sql = " SELECT Custdata.id AS booking_id,
                        Users.id AS user_id,
                        Users.username,
                        CustAssignedUser.created_date AS assigned_on,
                        Custdata.customername,
                        Custdata.mobile,
                        Custdata.email,
                        Custdata.created_on AS booking_date,
                        Custdata.reason,
                        Billsnap.bill_image,
                        Billsnap.billsnap_comments,
                        Billsnap.invoice_no,
                        Billsnap.store_name,
                        Billsnap.bill_raw_data,
                        State.statename,
                        Billsnap.qty,
                        Custdata.agent_comments,
                        Custdata.agent_comments_date
                FROM customer_assigned_users AS CustAssignedUser
                        JOIN booking AS Custdata ON (Custdata.id = CustAssignedUser.booking_id AND Custdata.status = 1 AND Custdata.internal_status =1 )
                        JOIN \"user\" AS Users ON (Users.id=CustAssignedUser.allocated_user_id)
                        JOIN billsnap AS Billsnap ON (Billsnap.booking_id=Custdata.id)
                        JOIN states AS State ON (State.id=Custdata.state_id)
                WHERE Custdata.id = ".$booking_id." 
                      AND Billsnap.status = 104
                      AND Custdata.campaign_id = ".$campaign_id;
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return isset($data[0])?$data[0]:'';
    }

    public function getAssignedList($campaign_id,$startdate,$enddate,$flag,$state_id){
        if($flag == 'totalrec'){
            $condition = " AND Custdata.internal_status >= 0 ";
        }
        elseif ($flag == 'pendingrec'){
            $condition = " AND Custdata.internal_status = 1 ";
        }
        elseif ($flag == 'processedrec'){
            $condition = " AND Custdata.internal_status = 2 ";
        }
        elseif ($flag == 'rejectedrec'){
            $condition = " AND Custdata.internal_status = 3 ";
        }
        $sql = " SELECT Custdata.id AS booking_id,
                        Users.id AS user_id,
                        Users.username,
                        CustAssignedUser.created_date AS assigned_on,
                        Custdata.customername,
                        Custdata.mobile,
                        Custdata.email,
                        Custdata.created_on AS booking_date,
                        Custdata.reason,
                        Billsnap.bill_image,
                        Billsnap.billsnap_comments,
                        Billsnap.invoice_no,
                        Billsnap.store_name,
                        State.statename,
                        Billsnap.qty,
                        Custdata.agent_comments,
                        Custdata.agent_comments_date,
                        CASE WHEN Custdata.internal_status = 1 THEN 'Assigned'
                             WHEN Custdata.internal_status = 2 THEN 'Approved'
                             WHEN Custdata.internal_status = 3 THEN 'Rejected'
                             ELSE 'Not Assigned' END AS status
                FROM booking AS Custdata 
                        LEFT JOIN customer_assigned_users  AS CustAssignedUser ON (CustAssignedUser.booking_id  = Custdata.id  )
                        LEFT JOIN \"user\" AS Users ON (Users.id=CustAssignedUser.allocated_user_id)
                        JOIN billsnap AS Billsnap ON (Billsnap.booking_id=Custdata.id)
                        JOIN states AS State ON (State.id=Custdata.state_id)
                WHERE Custdata.campaign_id = ".$campaign_id."
                      AND Billsnap.status = 104
		              AND Custdata.status = 1 
		              $condition ";
        if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate)){
            $sql   .= "  AND CustAssignedUser.created_date::date BETWEEN '".$startdate."' AND '".$enddate."' ";
        }

        if(isset($state_id) && !empty($state_id)){
            $sql .=  " AND CustData.state_id = ".$state_id." ";
        }
        else{
            $sql   .= "  AND CustData.state_id != ".Yii::$app->params['restrict_state_id']." ";
        }
        $sql .= " ORDER BY CustAssignedUser.created_date , 
                         Users.id ,
                         Custdata.created_on";
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return $data;
    }

}
