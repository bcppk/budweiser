<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property integer $id
 * @property timestamp $createdon
 * @property string $access_token
 * @property timestamp $access_token_expiry
 * @property timestamp $access_token_updated
 * @property timestamp $updated_date
 * @property integer $total_num_uploads
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'customers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['createdon','access_token_expiry','access_token_updated','updated_date'], 'safe'],
            [['total_num_uploads'], 'integer'],
            [['access_token'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'access_token' => 'Access Token',
            'createdon' => 'Createdon',
        ];
    }

    public function validateOTPCustomer($campaign_id,$mobile,$otp){
        $sql = "SELECT Cust.id AS customer_id,
                       CustData.id AS custdata_id
                FROM customers AS Cust 
                JOIN booking AS CustData ON (CustData.customer_id=Cust.id)
                WHERE CustData.campaign_id = ".$campaign_id."
                      AND CustData.status = 0
                      AND CustData.mobile = '".$mobile."'
                      AND CustData.otp = '".$otp."' 
                ORDER BY CustData.id DESC ";

        $data= Yii::$app->db->createCommand($sql)->queryAll();
        if(isset($data[0]) && !empty($data[0]))
            return $data[0];
        else
            return 0;
    }

    public function getCustomerDetailsbyAccessToken($campaign_id,$access_token){
        $sql = "SELECT Cust.id AS customer_id,
                       Cust.access_token_expiry,
                       CustData.id AS custdata_id,
                       CustData.customername,
                       CustData.email,
                       CustData.mobile
                FROM customers AS Cust 
                JOIN booking AS CustData ON (CustData.customer_id=Cust.id)
                WHERE Cust.access_token = '".$access_token."'
                      AND CustData.campaign_id = ".$campaign_id."
                      
                ORDER BY CustData.id DESC LIMIT 1";

        $data= Yii::$app->db->createCommand($sql)->queryAll();
        if(isset($data[0]) && !empty($data[0]))
            return $data[0];
        else
            return 0;
    }

    public function getCustomerDataIDByCustomer($customer_id){
        $sql  = "SELECT MAX(id) AS id FROM booking WHERE status = 0 AND customer_id = ".$customer_id;
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        return isset($data[0]['id'])?$data[0]['id']:0;
    }

}
