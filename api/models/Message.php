<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "messages".
 *
 * @property integer $id
 * @property string $createdon
 * @property string $fromid
 * @property string $toid
 * @property string $sub
 * @property string $body
 * @property integer $msgtype
 * @property string $senton
 * @property integer $status
 * @property integer $campaign_id
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['createdon', 'toid', 'body', 'msgtype', 'status'], 'required'],
            [['createdon', 'senton'], 'safe'],
            [['msgtype', 'status','campaign_id'], 'integer'],
            [['fromid', 'toid'], 'string', 'max' => 50],
            [['sub'], 'string', 'max' => 250],
            //[['body'], 'string', 'max' => 5000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'createdon' => 'Created On',
            'fromid' => 'From ID',
            'toid' => 'To ID',
            'sub' => 'Sub',
            'body' => 'Body',
            'msgtype' => 'Msgtype',
            'senton' => 'Sent On',
            'status' => 'Status',
        ];
    }
	
	public function sendEmail($sub, $msg, $to)
	{
		$this->createdon = date('Y-m-d H:i:s');
		$this->fromid = Yii::$app->params['adminEmail'];
		$this->toid = $to;
		$this->body = $msg;
		$this->msgtype = 1; // email
		$this->sub = $sub;
		$this->status = 0; // 0 is new, 5 and more for sent, less than 0 for failed.
		return $this->save();
	}
	
	public function sendSMS($campaign_id,$to, $msg,$sub)
	{
        $this->campaign_id = $campaign_id;
	    $this->createdon = date('Y-m-d H:i:s');
		$this->fromid = 'BIGCITY';
		$this->toid = $to;
		$this->body = $msg;
		$this->msgtype = 2; // SMS
		$this->sub = $sub;
		$this->status = 0; // 0 is new, 5 and more for sent, less than 0 for failed.
		$this->save();
	}	
	
	public function cronSend()
	{
		$msg = Yii::$app->mailer->compose('', [
                'mName' => 'Bobby',
                'mEmail' => $to,
                'mSubject' => $sub,
                'mBody' => $body]);
		return $msg->setFrom(Yii::$app->params['adminEmail'])
			->setTo($to)
			->setSubject($sub)
			->setTextBody($body)
			//->setHtmlBody($body)
			->send();
	}


}
