<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "offercodes".
 *
 * @property integer $id
 * @property string $code
 * @property integer $status
 * @property timestamp $usedon
 * @property integer $usedbybooking
 * @property integer $reward_id
 */
class Offercode extends \yii\db\ActiveRecord
{
	const STATUS_NOTSENT = 0;
	const STATUS_SENT = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'offercodes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'reward_id', 'status'], 'required'],
            [['status'], 'integer'],
            [['reward_id'],'integer'],
        ];
    }

     /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'status' => 'Status',
            'usedon' => 'Used On',
            'usedbybooking' => 'Used By Booking',
            'reward_id'=> 'Reward ID'
        ];
    }

    public function getReward()
    {
        return $this->hasOne(Reward::className(), ['id' => 'reward_id']);
    }

    public function getCampaign()
    {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }

    public function fields()
    {
        return [
            'id',
            'code' => function($model) {
                return $model->code;
            },
            'usedon' => function($model) {
                return $model->usedon;
            },
            'campaign_id' => function($model) {
                return $model->campaign_id;
            },
            'usedbybooking' => function($model) {
                return $model->usedbybooking;
            },
            'reward_id' => function($model) {
                return $model->reward_id;
            },
            'status' => function($model) {
                return $model->status;
            },
            'campaign_title' => function($model) {
                return $model->campaign->title;
            },
            'product' => function($model) {
                return $model->reward->prod_name;
            },
        ];
    }

    public function getOffercodebyCampaignId($campign_id){
        $sql = "SELECT Offercode.id,
                       Offercode.code 
                FROM offercodes AS Offercode 
                WHERE Offercode.campaign_id = ".$campign_id."
                      AND Offercode.status=0
                LIMIT 1";
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        if(isset($data[0]) && !empty($data[0]))
            return $data[0];
        else
            return 0;
    }
}
