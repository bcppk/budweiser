<?php
namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "states".
 *
 * @property integer $id
 * @property string $statename
 */
class State extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'states';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['statename'], 'required'],
            [['statename'], 'string'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'statename' => 'State Name',
        ];
    }


}
