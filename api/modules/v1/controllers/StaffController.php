<?php
    namespace app\modules\v1\controllers;

    use app\filters\auth\HttpBearerAuth;
    use app\helpers\MsgHelper;
    use app\models\Billsnap;
    use app\models\Booking;
    use app\models\Customer;
    use app\models\CustomerData;
    use app\models\Message;
    use app\models\Offercode;
    use app\models\RewardMessage;
    use Yii;
    use yii\helpers\Json;
    use yii\data\ActiveDataProvider;
    use yii\filters\AccessControl;
    use yii\filters\auth\CompositeAuth;
    use yii\helpers\Url;
    use yii\rbac\Permission;
    use yii\rest\ActiveController;

    use yii\web\HttpException;
    use yii\web\NotFoundHttpException;
    use yii\web\ServerErrorHttpException;

    use app\models\User;
    use app\models\LoginForm;
    use app\models\Campaign;

    class StaffController extends ActiveController
    {
        public $modelClass = 'app\models\User';

        public function __construct($id, $module, $config = [])
        {
            parent::__construct($id, $module, $config);

        }

        public function actions()
        {
            return [];
        }

        public function behaviors()
        {
            $behaviors = parent::behaviors();

            $behaviors['authenticator'] = [
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    HttpBearerAuth::className(),
                ],

            ];

            $behaviors['verbs'] = [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'index'  => ['get'],
                    'view'   => ['get'],
                    'create' => ['post'],
                    'update' => ['put'],
                    'delete' => ['delete'],
                    'login'  => ['post'],
                    'getPermissions'    =>  ['get'],
                    'campaigns' => ['get'],
                    'campaign' =>['get'],
                    'createcampaign'=>['post'],
                    'updatecampaigns'=>['put'],
                    'users' => ['get', 'post'],
                    'user'=>['get'],
                    'createuser'=>['post'],
                    'updateUser' => ['put'],
                    'tldashboard' => ['get'],
                    'bulkassignlist' =>['get'],
                    'bulkassigntoagent' => ['post'],
                    'agentlist'         =>['get'],
                    'reassignlist' => ['get'],
                    'reassigntoagent' => ['post'],
                    'agentcampaignlist' => ['get'],
                    'agentdatalist' => ['get'],
                    'getbookingdata' => ['get'],
                    'getassignedlist' =>['get'],
                    'agentapprovedbooking'=>['post'],
                    'agentrejectbooking'=>['post']
                ],
            ];

            // remove authentication filter
            $auth = $behaviors['authenticator'];
            unset($behaviors['authenticator']);

            // add CORS filter
            $behaviors['corsFilter'] = [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                ],
            ];

            // re-add authentication filter
            $behaviors['authenticator'] = $auth;
            // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
            $behaviors['authenticator']['except'] = ['options', 'login'];


	        // setup access
	        $behaviors['access'] = [
		        'class' => AccessControl::className(),
		        'only' => ['getPermissions','campaigns' , 'campaign','createcampaign','updatecampaigns','users','user','createuser','updateUser','tldashboard','bulkassignlist','bulkassigntoagent','agentlist','reassignlist','reassigntoagent','agentcampaignlist','agentdatalist','getcustomerdata','getassignedlist','agentapprovedbooking','agentrejectbooking'], //only be applied to
		        'rules' => [
			        [
				        'allow' => true,
				        'actions' => ['getPermissions','campaigns' , 'campaign','createcampaign','updatecampaigns'],
				        'roles' => ['admin'],
			        ],
                    [
                        'allow' => true,
                        'actions' => ['getPermissions','campaigns' , 'campaign','createcampaign','updatecampaigns','users','user','createuser','updateUser'],
                        'roles' => ['superadmin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['campaigns','tldashboard','bulkassignlist','bulkassigntoagent','agentlist','reassignlist','reassigntoagent','getassignedlist'],
                        'roles' => ['staff'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['agentcampaignlist','agentdatalist','getcustomerdata','agentapprovedbooking','agentrejectbooking'],
                        'roles' => ['user'],
                    ],
		        ],
	        ];

            return $behaviors;
        }

	    /**
	     * Return list of staff members
	     *
	     * @return ActiveDataProvider
	     */
        public function actionIndex(){
            return new ActiveDataProvider([
                'query' =>  User::find()->where([
	                '!=', 'status', -1
                ])->andWhere([
	                'in', 'role', [User::ROLE_STAFF, User::ROLE_ADMIN]
                ])
            ]);
        }

	    /**
	     * Return requested staff member information
	     *
	     * Request: /v1/staff/2
	     *
	     * Sample Response:
	     * {
		 *   "success": true,
		 *   "status": 200,
		 *   "data": {
		 *	        "id": 2,
		 *		    "username": "staff",
		 *		    "email": "staff@staff.com",
		 *		    "unconfirmed_email": "lygagohur@hotmail.com",
		 *		    "role": 50,
		 *		    "role_label": "Staff",
		 *		    "last_login_at": "2017-05-20 18:58:40",
		 *		    "last_login_ip": "127.0.0.1",
		 *		    "confirmed_at": "2017-05-15 09:20:53",
		 *		    "blocked_at": null,
		 *		    "status": 10,
		 *		    "status_label": "Active",
		 *		    "created_at": "2017-05-15 09:19:02",
		 *		    "updated_at": "2017-05-21 23:31:32"
		 *	    }
		 *   }
	     *
	     * @param $id
	     *
	     * @return array|null|\yii\db\ActiveRecord
	     * @throws NotFoundHttpException
	     */
        public function actionView($id){
            $staff = User::find()->where([
	            'id'    =>  $id
            ])->andWhere([
	            '!=', 'status', -1
            ])->andWhere([
	            'in', 'role', [User::ROLE_STAFF, User::ROLE_ADMIN]
            ])->one();
            if($staff){
                return $staff;
            } else {
                throw new NotFoundHttpException("Object not found: $id");
            }
        }

	    /**
	     * Create new staff member from backend dashboard
	     *
	     * Request: POST /v1/staff/1
	     *
	     * @return User
	     * @throws HttpException
	     */
        public function actionCreate(){
            $model = new User();
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');

            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(201);
                $id = implode(',', array_values($model->getPrimaryKey(true)));
                $response->getHeaders()->set('Location', Url::toRoute([$id], true));
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }

	    /**
	     * Update staff member information from backend dashboard
	     *
	     * Request: PUT /v1/staff/1
	     *  {
		 *  	"id": 20,
		 *  	"username": "testuser",
		 *  	"email": "test2@test.com",
		 *  	"unconfirmed_email": "test2@test.com",
	     *  	"password": "{password}",
		 *  	"role": 50,
		 *  	"role_label": "Staff",
		 *  	"last_login_at": null,
		 *  	"last_login_ip": null,
		 *  	"confirmed_at": null,
		 *  	"blocked_at": null,
		 *  	"status": 10,
		 *  	"status_label": "Active",
		 *  	"created_at": "2017-05-27 17:30:12",
		 *  	"updated_at": "2017-05-27 17:30:12",
		 *  	"permissions": [
		 *  		{
		 *  			"name": "manageSettings",
		 *  			"description": "Manage settings",
		 *  			"checked": false
		 *  		},
		 *  		{
		 *  			"name": "manageStaffs",
		 *  			"description": "Manage staffs",
		 *  			"checked": false
		 *  		},
		 *  		{
		 *  			"name": "manageUsers",
		 *  			"description": "Manage users",
		 *  			"checked": true
		 *  		}
		 *  	]
		 *  }
	     *
	     *
	     * @param $id
	     *
	     * @return array|null|\yii\db\ActiveRecord
	     * @throws HttpException
	     */
        public function actionUpdate($id) {
            $model = $this->actionView($id);

            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');

            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }

	    /**
	     * Delete requested staff member from backend dashboard
	     *
	     * Request: DELETE /v1/staff/1
	     *
	     * @param $id
	     *
	     * @return string
	     * @throws ServerErrorHttpException
	     */
        public function actionDelete($id) {
            $model = $this->actionView($id);

            $model->status = User::STATUS_DELETED;

            if ($model->save(false) === false) {
                throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
            }

            $response = \Yii::$app->getResponse();
            $response->setStatusCode(204);
            return "ok";
        }

	    /**
	     * Handle the login process for staff members for backend dashboard
	     *
	     * Request: POST /v1/staff/login
	     *
	     *
	     * @return array
	     * @throws HttpException
	     */
        public function actionLogin(){
            $model = new LoginForm();

	        $model->roles = [
	            User::ROLE_ADMIN,
	            User::ROLE_STAFF,
                User::ROLE_SUPERADMIN,
                User::ROLE_USER,
	        ];
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                $user = $model->getUser();
                $user->generateAccessTokenAfterUpdatingClientInfo(true);

                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
                $id = implode(',', array_values($user->getPrimaryKey(true)));
                $user_role = User::findIdentity($id);
                $responseData = [
                    'id'    =>  $id,
                    'access_token' => $user->access_token,
                    'role'=>$user_role->role
                ];

                return $responseData;
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }
        }


	    /**
	     * Return list of available permissions for the staff.  The function will be called when staff form is loaded in backend.
	     *
	     * Request: GET /v1/staff/get-permissions
	     *
	     * Sample Response:
	     * {
		 *		"success": true,
		 *		"status": 200,
		 *		"data": {
		 *			"manageSettings": {
		 *				"name": "manageSettings",
		 *				"description": "Manage settings",
		 *				"checked": false
		 *			},
		 *			"manageStaffs": {
		 *				"name": "manageStaffs",
		 *				"description": "Manage staffs",
	     *				"checked": false
		 *			}
		 *		}
		 *	}
	     */
	    public function actionGetPermissions(){
		    $authManager = Yii::$app->authManager;

		    /** @var Permission[] $permissions */
		    $permissions = $authManager->getPermissions();

		    /** @var array $tmpPermissions to store list of available permissions */
		    $tmpPermissions = [];

		    /**
		     * @var string $permissionKey
		     * @var Permission $permission
		     */
		    foreach($permissions as $permissionKey => $permission) {
			    $tmpPermissions[] = [
		            'name'          =>  $permission->name,
		            'description'   =>  $permission->description,
		            'checked'       =>  false,
		        ];
		    }

		    return $tmpPermissions;
	    }

        public function actionOptions($id = null) {
            return "ok";
        }

        public function actionCampaigns() {
            $user                  = User::findIdentity(\Yii::$app->user->getId());
            if(isset($user->campaign_id) && !empty($user->campaign_id))
            {
                $campaignid            = explode(',', $user->campaign_id);
                $campaigns             = Campaign::find()->where(['IN','id',$campaignid])->all();
            }
            else{
                $campaigns             = Campaign::find()->all();
            }
            $responseData = [
                'campaigns'  => $campaigns,
                'user_role'  => $user->role
            ];

            return $responseData;
        }

        public function actionCampaign($id) {
            $user         = User::findIdentity(\Yii::$app->user->getId());
            $campaigns    = Campaign::find()->where(['id'=>$id])->one();
            $responseData = [
                'campaigns'  => $campaigns,
                'user_role'  => $user->role,
            ];
            return $responseData;
        }

        public function actionCreatecampaign(){
            $model = new Campaign();
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
            $model->status          = Campaign::STATUS_ACTIVE;
            $model->created_date    = date('Y-m-d H:i:s');
            $model->updated_date    = date('Y-m-d H:i:s');
            if (Yii::$app->request->post('sitebanner')) {
                $file = Yii::$app->request->post('sitebanner');
                if (isset($file)) {
                    // Decode base64 data
                    list($type, $data) = explode(';', $file);
                    list(, $data) = explode(',', $data);
                    $file_data = base64_decode($data);

                    // Get file mime type
                    $finfo = finfo_open();
                    $file_mime_type = finfo_buffer($finfo, $file_data, FILEINFO_MIME_TYPE);

                    // File extension from mime type
                    if ($file_mime_type == 'image/jpeg' || $file_mime_type == 'image/jpg')
                        $file_type = 'jpeg';
                    else if ($file_mime_type == 'image/png')
                        $file_type = 'png';
                    else if ($file_mime_type == 'image/gif')
                        $file_type = 'gif';
                    else
                        $file_type = 'other';

                    if (in_array($file_type, ['jpeg', 'png', 'gif'])) {
                        $file_name = uniqid() . '.' . $file_type;
                        file_put_contents('uploads/' . $file_name, $file_data);

                        if (isset($model->sitebanner)) {
                            if (file_exists('uploads/' . $model->sitebanner)) {
                                unlink('uploads/' . $model->sitebanner);
                            }
                        }

                        $uploaded_file_url = \yii\helpers\Url::home(true) . 'uploads/' . $file_name;
                        $model->sitebanner = $uploaded_file_url;
                    }
                }
            }
            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }

        public function actionUpdatecampaigns($id) {
            $model = Campaign::findOne($id);
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
            $model->updated_date = date('Y-m-d H:i:s');
            if (Yii::$app->request->post('sitebanner')) {
                $file = Yii::$app->request->post('sitebanner');
                if (isset($file) && !empty($file)) {
                    // Decode base64 data
                    list($type, $data) = explode(';', $file);
                    list(, $data) = explode(',', $data);
                    $file_data = base64_decode($data);

                    // Get file mime type
                    $finfo = finfo_open();
                    $file_mime_type = finfo_buffer($finfo, $file_data, FILEINFO_MIME_TYPE);

                    // File extension from mime type
                    if ($file_mime_type == 'image/jpeg' || $file_mime_type == 'image/jpg')
                        $file_type = 'jpeg';
                    else if ($file_mime_type == 'image/png')
                        $file_type = 'png';
                    else if ($file_mime_type == 'image/gif')
                        $file_type = 'gif';
                    else
                        $file_type = 'other';

                    if (in_array($file_type, ['jpeg', 'png', 'gif'])) {
                        $file_name = uniqid() . '.' . $file_type;
                        file_put_contents('uploads/' . $file_name, $file_data);

                        if (isset($model->sitebanner)) {
                            if (file_exists('uploads/' . $model->sitebanner)) {
                                unlink('uploads/' . $model->sitebanner);
                            }
                        }

                        $uploaded_file_url = \yii\helpers\Url::home(true) . 'uploads/' . $file_name;
                        $model->sitebanner = $uploaded_file_url;
                    }
                }
                else{
                     $model->sitebanner = $model->sitebanner;
                }
            }

            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }

        public function actionUsers($action) {
            if($action=='index'){
                $users = User::find()->all();
                return $users;
            }
            else{
                $campaigns  = Campaign::find()->where(['status'=>Campaign::STATUS_ACTIVE])->orderBy(['id'=>SORT_ASC])->all();
                return $campaigns;
            }
        }

        public function actionUser($id){
            $user = User::findOne($id);
            $campaigns  = Campaign::find()->where(['status'=>Campaign::STATUS_ACTIVE])->orderBy(['id'=>SORT_ASC])->all();
            if($user){
                $responseData = [
                    'user'    => $user,
                    'campaigns'  => $campaigns
                ];
                return $responseData;
            } else {
                throw new NotFoundHttpException("Object not found: $id");
            }
        }

        public function actionCreateuser(){
            $model = new User();
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
            //  $model->load(Yii::$app->request->post());
            $model->status          = User::STATUS_ACTIVE;
            $model->confirmed_at    = date('Y-m-d H:i:s');
            $model->campaign_id     = Yii::$app->request->post('campaign_id');

            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $sql = "INSERT INTO auth_item(name, type, description) VALUES ('$model->username',1,'$model->username')";
                $query = Yii::$app->db->createCommand($sql)->execute();
                $response->setStatusCode(201);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }

        public function actionUpdateUser($id) {
            $model = User::findOne($id);
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
            $model->campaign_id     = Yii::$app->request->post('campaign_id');
            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }
            return $model;
        }

        public function actionTldashboard($campaign_id,$startdate,$enddate,$state_id){
            if(isset($campaign_id) && !empty($campaign_id)){
                $model              = new Booking();
                $totalrecords       = $model->getTotalRecords($campaign_id,$startdate,$enddate,$state_id);
                $unassignedrecords  = $model->getUnAssignedRecords($campaign_id,$startdate,$enddate,$state_id);
                $pendingrecords     = $model->getAssignedPendingRecords($campaign_id,$startdate,$enddate,$state_id);
                $processedrecords   = $model->getAssignedProcessedRecords($campaign_id,$startdate,$enddate,$state_id);
                $rejectedrecords    = $model->getRejectedRecords($campaign_id,$startdate,$enddate,$state_id);
                $responseData = [
                    'totalrecords'                => $totalrecords,
                    'unassignedrecords'           => $unassignedrecords,
                    'pendingrecords'              => $pendingrecords,
                    'processedrecords'            => $processedrecords,
                    'rejectedrecords'             => $rejectedrecords,

                ];
                return $responseData;
            }
            else{
                throw new HttpException(422, json_encode("Camapign ID should not be empty!!"));
            }
        }

        public function actionGetassignedlist($campaign_id,$startdate,$enddate,$flag,$state_id){
            if(isset($campaign_id) && !empty($campaign_id)){
                $campaigns = Campaign::findOne($campaign_id);
                if(isset($flag) && !empty($flag)){
                    $model  = new Booking();
                    if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate)){
                        $date_range = $startdate.'|'.$enddate ;
                    }
                    else{
                        $date_range = '';
                    }
                    $data   = $model->getAssignedList($campaign_id,$startdate,$enddate,$flag,$state_id);
                    $responseData = [
                        'assignedlist'    => $data,
                        'campaign_name'   => $campaigns->title,
                        'date_range'      => $date_range
                    ];
                    return $responseData;
                }
                else{
                    throw new HttpException(422, json_encode("Permission denied."));
                }
            }
            else{
                throw new HttpException(422, json_encode("Camapign ID should not be empty!!"));
            }
        }

        public function actionBulkassignlist($campaign_id,$startdate,$enddate,$state_id){
            if(isset($campaign_id) && !empty($campaign_id)) {
                $campaigns = Campaign::findOne($campaign_id);
                if(isset($campaigns))
                {
                    $model = new Booking();
                    if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate)  ){
                        $date_range = $startdate.'|'.$enddate ;
                    }
                    else{
                        $date_range = '';
                    }
                    $bulklistdetails    = $model->getBulkAssignDetailsList($campaign_id,$startdate,$enddate,$state_id);
                    $unassignedrecords  = $model->getUnAssignedRecords($campaign_id,$startdate,$enddate,$state_id);
                    $responseData = [
                        'bulklistdetails'    => $bulklistdetails,
                        'unassignedrecords'  => $unassignedrecords,
                        'campaign_name'      => $campaigns->title,
                        'date_range'         => $date_range
                    ];
                    return $responseData;
                }
            }
            else{
                throw new HttpException(422, json_encode("Camapign ID should not be empty!!"));
            }
        }


        public function actionBulkassigntoagent(){
            if(Yii::$app->request->post('campaign_id')){
                $campaign_id    = Yii::$app->request->post('campaign_id');
                $campaigns      = Campaign::findOne($campaign_id);
                if(isset($campaigns)) {
                    $state_id        = Yii::$app->request->post('state_id');
                    if (Yii::$app->request->post('bulkassignvalue')) {
                        $bulkassignvalue = Yii::$app->request->post('bulkassignvalue');
                        $startdate       = Yii::$app->request->post('startdate');
                        $enddate         = Yii::$app->request->post('enddate');
                        $decode          = Json::decode($bulkassignvalue);
                        $model           = new Booking();
                        $connection      =  \Yii::$app->db;
                        $transaction     = $connection->beginTransaction();
                        try {
                            foreach($decode as $key=>$value){
                                $sql = " INSERT INTO customer_assigned_users(booking_id, allocated_user_id, created_date, updated_date)
                                 SELECT CustData.id,".$key.",now(),now() 
                                 FROM booking AS CustData
                                 JOIN billsnap AS Billsnap ON (Billsnap.booking_id = CustData.id)
                                 WHERE campaign_id = ".$campaign_id." 
                                       AND Billsnap.status = 104
                                       AND CustData.internal_status = 0
                                       AND CustData.status = 1 ";
                                if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate))
                                {
                                    $sql .=  " AND CustData.created_on::date BETWEEN '".$startdate."' AND '".$enddate."' ";
                                }

                                if(isset($state_id) && !empty($state_id)){
                                    $sql .=  " AND CustData.state_id = ".$state_id." ";
                                }
                                else{
                                    $sql .=  " AND CustData.state_id != ".Yii::$app->params['restrict_state_id']." ";
                                }
                                $sql .=  " ORDER BY CustData.created_on
                                LIMIT ".$value." ";
                                $connection->createCommand($sql)->execute();
                                $upadtesql = " UPDATE booking 
                                               SET internal_status = 1, updated_date = now()
                                       FROM
                                       (
                                        SELECT CustData.id,CustData.campaign_id
                                        FROM booking AS CustData 
                                        JOIN billsnap AS Billsnap ON (Billsnap.booking_id = CustData.id)
                                        WHERE campaign_id = ".$campaign_id." 
                                              AND Billsnap.status = 104
                                              AND CustData.internal_status = 0
                                              AND CustData.status = 1 ";
                                if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate))
                                {
                                    $upadtesql .=  " AND CustData.created_on::date BETWEEN '".$startdate."' AND '".$enddate."' ";
                                }

                                if(isset($state_id) && !empty($state_id)){
                                    $upadtesql .=  " AND CustData.state_id = ".$state_id." ";
                                }
                                else{
                                    $upadtesql .=  " AND CustData.state_id != ".Yii::$app->params['restrict_state_id']." ";
                                }
                                $upadtesql .=  " ORDER BY CustData.created_on
                                        LIMIT ".$value."
                                       ) AS ExistingCustdata 
                                        WHERE ExistingCustdata.id=booking.id
                                              AND ExistingCustdata.campaign_id=booking.campaign_id ";
                                $connection->createCommand($upadtesql)->execute();
                            }
                            $transaction->commit();
                            if ($transaction) {
                                $response = \Yii::$app->getResponse();
                                $response->setStatusCode(200);
                                $bulklistdetails    = $model->getBulkAssignDetailsList($campaign_id,$startdate,$enddate,$state_id);
                                $unassignedrecords  = $model->getUnAssignedRecords($campaign_id,$startdate,$enddate,$state_id);
                                $responseData = [
                                    'bulklistdetails'    => $bulklistdetails,
                                    'unassignedrecords'  => $unassignedrecords,
                                ];
                                return $responseData;
                            } else {
                                throw new HttpException(422, json_encode('Server error! Please try after some time.'));
                            }
                        }
                        catch(Exception $e) {
                            $transaction->rollback();
                        }
                    } else {
                        throw new HttpException(422, json_encode("Atleast assign a single record!!"));
                    }
                }
            }
            else{
                throw new HttpException(422, json_encode("Camapign ID should not be empty!!"));
            }
        }

        public function actionAgentlist(){
            $users=User::find()->where(['=', 'status', User::STATUS_ACTIVE])->andWhere(['=', 'role', User::ROLE_USER]) ->andWhere(['not', ['confirmed_at' => null]])->orderBy(['id'=>SORT_DESC])->all();
            return $users;
        }

        public function actionReassignlist($campaign_id,$startdate,$enddate,$user_id,$state_id){
            if(isset($campaign_id) && !empty($campaign_id)){
                $model          = new Booking();
                $data     = $model->getReassignList($campaign_id,$startdate,$enddate,$user_id,$state_id);
                 $responseData = [
                    'reassignlist'  => $data,
                 ];
                 return $responseData;
            }
            else{
                throw new HttpException(422, json_encode("Camapign ID should not be empty!!"));
            }
        }

        public function actionReassigntoagent(){
            if(Yii::$app->request->post('user_id')){
                if(Yii::$app->request->post('booking_id')){
                    $user_id           = Yii::$app->request->post('user_id');
                    $customer_data_id  = Yii::$app->request->post('booking_id');
                    $sql            = " UPDATE customer_assigned_users SET allocated_user_id = ".$user_id." WHERE booking_id in(".$customer_data_id.") ";
                    $query          = Yii::$app->db->createCommand($sql)->execute();
                    if($query){
                        $response = \Yii::$app->getResponse();
                        $response->setStatusCode(200);
                    }
                    else{
                        throw new HttpException(422, json_encode('Server error! Please try after some time.'));
                    }
                }
                else{
                    throw new HttpException(422, json_encode("Atleast select one record!!"));
                }
            }
            else{
                throw new HttpException(422, json_encode("Please choose User!!"));
            }
        }

        public function actionAgentcampaignlist(){
            $user = User::findIdentity(\Yii::$app->user->getId());
            $campaignmodel  = new Campaign();
            $campaign_id    = $campaignmodel->getAgentCampaignId($user->id);
            $campaignid     = explode(',', $campaign_id);
            $campaigns      = Campaign::find()->where(['status'=>Campaign::STATUS_ACTIVE])->andWhere(['IN','id',$campaignid])->orderBy(['id'=>SORT_ASC])->all();
            return $campaigns;
        }

        public function actionAgentdatalist($campaign_id,$startdate,$enddate){
            if(isset($campaign_id) && !empty($campaign_id)){
                $user = User::findIdentity(\Yii::$app->user->getId());
                $model  = new Booking();
                $data   = $model->getAgentAssignedList($campaign_id,$startdate,$enddate,$user->id);
                $responseData = [
                     'agentdatalist'  => $data,
                ];
                return $responseData;
            }
            else{
                throw new HttpException(422, json_encode("Camapign ID should not be empty!!"));
            }
        }

        public function actionGetbookingdata($campaign_id,$booking_id){
            if(isset($campaign_id) && !empty($campaign_id)){
                $campaigns  = Campaign::findOne($campaign_id);
                if(isset($booking_id) && !empty($booking_id)){
                    $model  = new Booking();
                    $data   = $model->getBookingData($campaign_id,$booking_id);
                    $responseData = [
                        'customerdata'  => $data,
                        'campaign_name' => $campaigns->title,
                    ];
                    return $responseData;
                }
                else{
                    throw new HttpException(422, json_encode("Customer Data should not be empty!!"));
                }
            }
            else{
                throw new HttpException(422, json_encode("Camapign ID should not be empty!!"));
            }
        }

        public function actionAgentapprovedbooking(){
            $billsnapModel          = new Billsnap();
            if(Yii::$app->request->post('campaign_id') && Yii::$app->request->post('user_id') && Yii::$app->request->post('booking_id')){
                $campaign_id        = Yii::$app->request->post('campaign_id');
                $user_id            = Yii::$app->request->post('user_id');
                $booking_id         = Yii::$app->request->post('booking_id');
                if(Yii::$app->request->post('qty')) {
                    $qty            = Yii::$app->request->post('qty');
                    $bookingdetails = Booking::find()->where(['id'=>$booking_id])->one();
                    if(Yii::$app->request->post('agent_comments')) {
                        $agent_comments  = Yii::$app->request->post('agent_comments');
                        if(Yii::$app->request->post('invoice_no') && Yii::$app->request->post('store_name')){
                            $invoice_no      = Yii::$app->request->post('invoice_no');
                            $store_name      = Yii::$app->request->post('store_name');
                            $checkDuplicate  = $billsnapModel->checkDuplicateInvoice($invoice_no,$store_name);
                            if(isset($checkDuplicate) && !empty($checkDuplicate)){
                                throw new HttpException(422, json_encode("Duplicate Bill!!"));
                            }
                            else{
                                $connection      =  \Yii::$app->db;
                                $transaction     = $connection->beginTransaction();
                                try {
                                    $qtyupdate   = "UPDATE billsnap SET qty = ".$qty.",updated_date = now(),invoice_no = '".$invoice_no."',store_name = '".$store_name."' WHERE booking_id = ".$booking_id;
                                    $connection->createCommand($qtyupdate)->execute();

                                    $customers   = Customer::find()->where(['id'=>$bookingdetails->customer_id])->one();
                                    if($customers->num_offercode_sent >= 3){
                                        $this->getandSendExcessMessage($campaign_id,$bookingdetails->customername,$bookingdetails->mobile);
                                    }
                                    else{

                                        if(isset($customers->num_offercode_sent) && !empty($customers->num_offercode_sent)){
                                            $check_num_offer_sent = $qty - $customers->num_offercode_sent;
                                            if($check_num_offer_sent <= 0){
                                                $this->getandSendOfferCode($campaign_id,$bookingdetails->customername,$bookingdetails->mobile);
                                                $numoffupdate = "UPDATE customers SET num_offercode_sent = num_offercode_sent + 1,updated_date = now() WHERE id = ".$bookingdetails->customer_id;
                                                $connection->createCommand($numoffupdate)->execute();
                                            }
                                            else{
                                                $this->getandSendOfferCode($campaign_id,$bookingdetails->customername,$bookingdetails->mobile);
                                                $this->getandSendOfferCode($campaign_id,$bookingdetails->customername,$bookingdetails->mobile);
                                                $numoffupdate = "UPDATE customers SET num_offercode_sent = num_offercode_sent + 2,updated_date = now() WHERE id = ".$bookingdetails->customer_id;
                                                $connection->createCommand($numoffupdate)->execute();
                                            }
                                        }
                                        else{
                                            if($qty == 1){
                                                $this->getandSendOfferCode($campaign_id,$bookingdetails->customername,$bookingdetails->mobile);
                                                $numoffupdate = "UPDATE customers SET num_offercode_sent = 1,updated_date = now() WHERE id = ".$bookingdetails->customer_id;
                                                $connection->createCommand($numoffupdate)->execute();
                                            }
                                            else if($qty == 2){
                                                $this->getandSendOfferCode($campaign_id,$bookingdetails->customername,$bookingdetails->mobile);
                                                $this->getandSendOfferCode($campaign_id,$bookingdetails->customername,$bookingdetails->mobile);
                                                $numoffupdate = "UPDATE customers SET num_offercode_sent = 2,updated_date = now() WHERE id = ".$bookingdetails->customer_id;
                                                $connection->createCommand($numoffupdate)->execute();
                                            }
                                            else{
                                                $this->getandSendOfferCode($campaign_id,$bookingdetails->customername,$bookingdetails->mobile);
                                                $this->getandSendOfferCode($campaign_id,$bookingdetails->customername,$bookingdetails->mobile);
                                                $this->getandSendOfferCode($campaign_id,$bookingdetails->customername,$bookingdetails->mobile);
                                                $numoffupdate = "UPDATE customers SET num_offercode_sent = 3,updated_date = now() WHERE id = ".$bookingdetails->customer_id;
                                                $connection->createCommand($numoffupdate)->execute();
                                            }
                                        }
                                    }

                                    $sql   = "UPDATE booking SET internal_status = 2 , agent_comments = '".$agent_comments."',agent_comments_date = now()  WHERE id = ".$booking_id." AND campaign_id = ".$campaign_id;
                                    $connection->createCommand($sql)->execute();

                                    $upadtesql = "  UPDATE customer_assigned_users SET status = 1, updated_date = now()  WHERE booking_id = ".$booking_id." AND allocated_user_id = ".$user_id;
                                    $connection->createCommand($upadtesql)->execute();

                                    $numqtyupdate = "UPDATE customers SET total_qty = total_qty + ".$qty.",updated_date = now() WHERE id = ".$bookingdetails->customer_id;
                                    $connection->createCommand($numqtyupdate)->execute();

                                    $transaction->commit();
                                    if ($transaction) {
                                        $response = \Yii::$app->getResponse();
                                        $response->setStatusCode(200);
                                    } else {
                                        throw new HttpException(422, json_encode('Server error! Please try after some time.'));
                                    }
                                }
                                catch(Exception $e) {
                                    $transaction->rollback();
                                }
                            }
                        }
                        else{
                            throw new HttpException(422, json_encode("Invoice & Store should not be empty!!"));
                        }
                    }
                    else{
                        throw new HttpException(422, json_encode("Agent comments should not be empty!!"));
                    }
                }
                else{
                    throw new HttpException(422, json_encode("Qty should not be empty!"));
                }
            }
            else{
                throw new HttpException(422, json_encode("Permission denied."));
            }
        }

        private function getandSendExcessMessage($campaign_id,$customername,$mobile){
            if(isset($campaign_id) && !empty($campaign_id)){
                $campainmodel            = new Campaign();
                $campains                = Campaign::find()->where(['id'=>$campaign_id])->one();
                $booking_excess_msg      = $campains->booking_excess_msg;
                $arrParams = array(
                    array('name' => 'customerName', 'value' => $customername),
                );

                $msg         = $campainmodel->parseCampginSmsTemplate($booking_excess_msg,$arrParams);
                $message     = new Message();
                $message->sendSMS($campaign_id,$mobile,$msg,'Excess Message');
               /* $smsmsg      = urlencode($msg);
                $msgHelper   = new MsgHelper();
                $msgHelper->sendSMS($mobile,$smsmsg);*/
                $sql = "INSERT INTO logs(campaign_id, category,description, created_date ) VALUES ($campaign_id,'Excess Message Sent','Mobile No :$mobile ; ',now());";
                $query = Yii::$app->db->createCommand($sql)->execute();
            }
            else{
                throw new HttpException(422, json_encode("Campaign ID should not be empty!"));
            }
        }

        private function getandSendOfferCode($campaign_id,$customername,$mobile){
            if(isset($campaign_id) && !empty($campaign_id)){
                $offercodemodel = new Offercode();
                $rewardmsgmodel = new RewardMessage();
                $campainmodel   = new Campaign();
                $offercode      = $offercodemodel->getOffercodebyCampaignId($campaign_id);
                $rewardmsg      = $rewardmsgmodel->getRewardMessageByCampaignandReward($campaign_id,1);
                if(isset($offercode) && !empty($offercode) && isset($rewardmsg) && !empty($rewardmsg)){
                    $offer_code = $offercode['code'];
                    $arrParams = array(
                        array('name' => 'customerName', 'value' => $customername),
                        array('name' => 'offercode', 'value' => $offer_code),
                    );

                    $msg         = $campainmodel->parseCampginSmsTemplate($rewardmsg,$arrParams);
                    $message     = new Message();
                    $message->sendSMS($campaign_id,$mobile,$msg,'Offercode Sent Message');
                    /*$smsmsg      = urlencode($msg);
                    $msgHelper   = new MsgHelper();
                    $msgHelper->sendSMS($mobile,$smsmsg);*/
                    $offercode_status_update	= " UPDATE offercodes SET status=1,senton=now() WHERE id = ".$offercode['id'];
                    $update    = Yii::$app->db->createCommand($offercode_status_update)->execute();
                    $sql = "INSERT INTO logs(campaign_id, category,description, created_date ) VALUES ($campaign_id,'Offercode Sent Message','Mobile No :$mobile ; offercode : $offer_code',now());";
                    $query = Yii::$app->db->createCommand($sql)->execute();
                }
                else{
                    throw new HttpException(422, json_encode("Offer code & Reward setting not configured!"));
                }
            }
            else{
                throw new HttpException(422, json_encode("Campaign ID should not be empty!"));
            }
        }

        public function actionAgentrejectbooking(){
            if(Yii::$app->request->post('campaign_id') && Yii::$app->request->post('user_id') && Yii::$app->request->post('booking_id')){
                $campaign_id         = Yii::$app->request->post('campaign_id');
                $user_id             = Yii::$app->request->post('user_id');
                $booking_id          = Yii::$app->request->post('booking_id');
                $bookingdetails      = Booking::find()->where(['id'=>$booking_id])->one();
                if(Yii::$app->request->post('agent_comments')) {
                    $agent_comments  = Yii::$app->request->post('agent_comments');
                    if(Yii::$app->request->post('reason')){
                        $reason      = Yii::$app->request->post('reason');
                        $connection  =  \Yii::$app->db;
                        $transaction = $connection->beginTransaction();
                        try {
                            $this->getandSendRejectMessage($campaign_id,$bookingdetails->customername,$bookingdetails->mobile);

                            $sql     = "UPDATE booking SET internal_status = 3 , agent_comments = '".$agent_comments."',agent_comments_date = now(),reason = '".$reason."'  WHERE id = ".$booking_id." AND campaign_id = ".$campaign_id;
                            $connection->createCommand($sql)->execute();

                            $upadtesql = "  UPDATE customer_assigned_users SET status = 1, updated_date = now()  WHERE booking_id = ".$booking_id." AND allocated_user_id = ".$user_id;
                            $connection->createCommand($upadtesql)->execute();

                            $transaction->commit();
                            if ($transaction) {
                                $response = \Yii::$app->getResponse();
                                $response->setStatusCode(200);
                            } else {
                                throw new HttpException(422, json_encode('Server error! Please try after some time.'));
                            }
                        }
                        catch(Exception $e) {
                            $transaction->rollback();
                        }
                    }
                    else{
                        throw new HttpException(422, json_encode("Please Select Any Reason of Rejection!"));
                    }
                }
                else{
                    throw new HttpException(422, json_encode("Agent comments should not be empty!!"));
                }
            }
            else{
                throw new HttpException(422, json_encode("Permission denied."));
            }
        }

        private function getandSendRejectMessage($campaign_id,$customername,$mobile){
            if(isset($campaign_id) && !empty($campaign_id)){
                $campainmodel            = new Campaign();
                $campains                = Campaign::find()->where(['id'=>$campaign_id])->one();
                $booking_reject_msg      = $campains->booking_reject_msg;
                $arrParams = array(
                    array('name' => 'customerName', 'value' => $customername),
                );

                $msg         = $campainmodel->parseCampginSmsTemplate($booking_reject_msg,$arrParams);
                $message     = new Message();
                $message->sendSMS($campaign_id,$mobile,$msg,'Reject Message');
               /* $smsmsg      = urlencode($msg);
                $msgHelper   = new MsgHelper();
                $msgHelper->sendSMS($mobile,$smsmsg);*/
                $sql = "INSERT INTO logs(campaign_id, category,description, created_date ) VALUES ($campaign_id,'Reject Message Sent','Mobile No :$mobile Message :$booking_reject_msg ; ',now());";
                $query = Yii::$app->db->createCommand($sql)->execute();
            }
            else{
                throw new HttpException(422, json_encode("Campaign ID should not be empty!"));
            }
        }

    }