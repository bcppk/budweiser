<?php
    namespace app\modules\v1\controllers;

    use app\filters\auth\HttpBearerAuth;
    use app\helpers\AppHelper;
    use app\helpers\GetOcrDataHelper;
    use app\helpers\MsgHelper;
    use app\helpers\OcrImagetoTextHelper;
    use app\models\Billsnap;
    use app\models\Booking;
    use app\models\Campaign;
    use app\models\City;
    use app\models\Customer;
    use app\models\Message;
    use app\models\Offercode;
    use app\models\RewardMessage;
    use app\models\State;
    use Yii;
    use yii\helpers\Json;

    use yii\data\ActiveDataProvider;
    use yii\filters\AccessControl;
    use yii\filters\auth\CompositeAuth;
    use yii\helpers\Url;
    use yii\rest\ActiveController;

    use yii\web\HttpException;
    use app\models\User;


    class UserController extends ActiveController
    {
        public $modelClass = 'app\models\User';

        public function __construct($id, $module, $config = [])
        {
            parent::__construct($id, $module, $config);

        }

        public function actions()
        {
            return [];
        }

        public function behaviors()
        {
            $behaviors = parent::behaviors();

            $behaviors['authenticator'] = [
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    HttpBearerAuth::className(),
                ],

            ];

            $behaviors['verbs'] = [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'index'  => ['get'],
                    'getsitesettings'=>['get'],
                    'registration'=>['post'],
                    'validateotp'=>['post'],
                    'resendotp'=> ['post'],
                    'uploadbill' => ['post'],
                    'getstatelist' => ['get'],
                    'getcitylist'  => ['get'],
                    'getcitylistbystate'  => ['get'],
                    'getstatelistwithouttn' => ['get']
                ],
            ];

            // remove authentication filter
            $auth = $behaviors['authenticator'];
            unset($behaviors['authenticator']);

            // add CORS filter
            $behaviors['corsFilter'] = [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Allow-Origin' => ['*'],
                ],
            ];

            // re-add authentication filter
            $behaviors['authenticator'] = $auth;
            // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
            $behaviors['authenticator']['except'] = ['options', 'getsitesettings','registration','validateotp','resendotp','uploadbill','getstatelist','getcitylist','getcitylistbystate','getstatelistwithouttn'];


	        // setup access
	        $behaviors['access'] = [
		        'class' => AccessControl::className(),
		        'only' => ['index'], //only be applied to
		        'rules' => [
			        [
				        'allow' => true,
				        'actions' => ['index'],
				        'roles' => ['admin', 'manageUsers'],
			        ]
		        ],
	        ];

            return $behaviors;
        }
        public function auth()
        {
            return [
                'bearerAuth' => [
                    'class' => \yii\filters\auth\HttpBearerAuth::className(),
                ],
            ];
        }

        public function actionIndex(){
            return new ActiveDataProvider([
                'query' =>  User::find()->where([
                    '!=', 'status', -1
                ])->andWhere([
                    'role'  =>  User::ROLE_USER
                ])
            ]);
        }

        public function actionOptions($id = null) {
            return "ok";
        }

        public function getBearerAccessToken(){
            $bearer = null;
            $headers = apache_request_headers();
            if(isset($headers['Authorization'])){
                $matches = array();
                preg_match('/^Bearer\s+(.*?)$/', $headers['Authorization'], $matches);
                if(isset($matches[1])){
                    $bearer = $matches[1];
                }
            } elseif(isset($headers['authorization'])){
                $matches = array();
                preg_match('/^Bearer\s+(.*?)$/', $headers['authorization'], $matches);
                if(isset($matches[1])){
                  $bearer = $matches[1];
                }
            }
            return $bearer;
        }

        public function actionGetsitesettings($campaign_id){
            if(isset($campaign_id)){
                $sitesettings = Campaign::find()->where(['id'=>$campaign_id])->one();
                return $sitesettings;
            }
            else{
                throw new HttpException(422, json_encode("Permission denied."));
            }
        }

        public function actionRegistration(){
            $model         = new Customer();
            $custdatamodel = new Booking();
            $date          = date('Y-m-d H:i:s');
            if(Yii::$app->request->post()){
                $campaign_id                    = Yii::$app->request->post('campaign_id');
                $custdatamodel->customername    = Yii::$app->request->post('customername');
                $custdatamodel->email           = Yii::$app->request->post('email');
                $custdatamodel->mobile          = Yii::$app->request->post('mobile');
                $custdatamodel->state_id        = Yii::$app->request->post('state_id');
                $custdatamodel->city_id         = Yii::$app->request->post('city_id');
                $restrict_state_id              = Yii::$app->params['restrict_state_id'];
                if(!empty($custdatamodel->customername) && !empty($custdatamodel->email) && !empty($custdatamodel->mobile) && !empty($campaign_id) && !empty($custdatamodel->state_id) && !empty($custdatamodel->city_id) ){
                    if($custdatamodel->state_id  ==  $restrict_state_id){
                        $custdatamodel->tn_answer = Yii::$app->request->post('tn_answer');
                        if(empty($custdatamodel->tn_answer)){
                            throw new HttpException(422, json_encode('Please enter your answer !'));
                            exit;
                        }
                    }
                    $check_customer = Booking::find()->where(['mobile'=>$custdatamodel->mobile])->one();
                    if(isset($check_customer) && !empty($check_customer)) {
                        $otp = $this->generateOTP($campaign_id, $custdatamodel->mobile);
                        $custdatamodel->otp = $otp;
                        $custdatamodel->customer_id = $check_customer->customer_id;
                        $custdatamodel->ipaddress = Yii::$app->request->userIP;
                        $custdatamodel->created_on = $date;
                        $custdatamodel->campaign_id = $campaign_id;
                        if ($custdatamodel->validate() && $custdatamodel->save()) {
                            $response = \Yii::$app->getResponse();
                            $response->setStatusCode(200);
                            $responseData = [
                                'customername' => $custdatamodel->customername,
                                'mobile' => $custdatamodel->mobile,
                                'email' => $custdatamodel->email,
                                'customer_id' => $check_customer->customer_id
                             //   'otp'       => $otp
                            ];
                            return $responseData;
                        } else {
                            throw new HttpException(422, json_encode($custdatamodel->errors));
                        }
                    }
                    else {
                        $model->createdon              = $date;
                        if($model->validate() && $model->save()){
                            $otp                           = $this->generateOTP($campaign_id,$custdatamodel->mobile);
                            $custdatamodel->otp            = $otp;
                            $custdatamodel->customer_id    = $model->id;
                            $custdatamodel->ipaddress      = Yii::$app->request->userIP;
                            $custdatamodel->created_on     = $date;
                            $custdatamodel->campaign_id    = $campaign_id;
                            if($custdatamodel->validate() && $custdatamodel->save()){
                                $response                  = \Yii::$app->getResponse();
                                $response->setStatusCode(200);
                                $sql = "INSERT INTO logs(campaign_id, category,description, created_date ) VALUES ($campaign_id,'Regsiteration','Username :$custdatamodel->customername ; ',now());";
                                $query = Yii::$app->db->createCommand($sql)->execute();
                                $responseData = [
                                    'customername' => $custdatamodel->customername,
                                    'mobile' => $custdatamodel->mobile,
                                    'email' => $custdatamodel->email,
                                    'customer_id'=>$model->id
                                //    'otp'       => $otp
                                ];
                                return $responseData;
                            }
                            else{
                                throw new HttpException(422, json_encode($custdatamodel->errors));
                            }
                        }
                        else{
                            throw new HttpException(422, json_encode($model->errors));
                        }
                    }
                }
                else{
                    throw new HttpException(422, json_encode('Please enter the mandatory details to proceed further'));
                }
            }

            else {
                // Validation error
                throw new HttpException(422, json_encode("Permission denied."));
            }
        }

        private function generateOTP($campaign_id,$mobile){
            if(isset($campaign_id) && isset($mobile)){
                $date           = date('Y-m-d H:i:s');
                $appHelper   = new AppHelper();
                $otp         = $appHelper->getUniqueRandomNum();
                $campains    = Campaign::findOne($campaign_id);
                $arrParams   = array(
                    array('name' => 'otp', 'value' => $otp),
                );
                $msg         = $campains->parseCampginSmsTemplate($campains->regtext,$arrParams);
                $message     = new Message();
                $message->sendSMS($campaign_id,$mobile,$msg,'Regsiteration SMS');
               /* $smsmsg      = urlencode($msg);
                $msgHelper   = new MsgHelper();
                $msgHelper->sendSMS($mobile,$smsmsg);*/
                $sql = "INSERT INTO logs(campaign_id, category,description, created_date ) VALUES ($campaign_id,'Generated OTP','Mobile No :$mobile ; OTP : $otp',now());";
                $query = Yii::$app->db->createCommand($sql)->execute();
                return $otp;
             }
            else {
                // Validation error
                throw new HttpException(422, json_encode("Permission denied."));
            }
        }

        public function actionResendotp(){
            if(Yii::$app->request->post('campaign_id') && Yii::$app->request->post('mobile') && Yii::$app->request->post('customer_id')){
                $model              = new Customer();
                $campaign_id        = Yii::$app->request->post('campaign_id');
                $mobile             = Yii::$app->request->post('mobile');
                $customer_id        = Yii::$app->request->post('customer_id');
                $otp                = $this->generateOTP($campaign_id,$mobile);
                $customer_data_id   = $model->getCustomerDataIDByCustomer($customer_id);
                $sql                = "UPDATE booking SET otp = '$otp',updated_date=now() WHERE id = ".$customer_data_id;
                $query              = Yii::$app->db->createCommand($sql)->execute();
                if($query){
                    $response = \Yii::$app->getResponse();
                    $response->setStatusCode(200);
                    $responseData = [
                        'code'          => 100,
                        'message'       => 'OTP has been sent successfully',
                        'customer_id'   =>  $customer_id,
                    ];
                    return $responseData;
                }
                else{
                    throw new HttpException(422, json_encode('Server error! Please try after some time.'));
                }
            }
            else{
                throw new HttpException(422, json_encode("Please enter the mandatory details to proceed further"));
            }
        }

        public function actionValidateotp(){
            if(Yii::$app->request->post('campaign_id') && Yii::$app->request->post('mobile') && Yii::$app->request->post('otp')){
                $campaign_id    = Yii::$app->request->post('campaign_id');
                $mobile         = Yii::$app->request->post('mobile');
                $otp            = Yii::$app->request->post('otp');
                $date           = date('Y-m-d H:i:s');
                $model          = new Customer();
                $customers      = $model->validateOTPCustomer($campaign_id,$mobile,$otp);
                if(isset($customers['customer_id']) && isset($customers['custdata_id'])){
                        $cusomter_id = $customers['customer_id'];
                        $usermodel = new User();
                        $usermodel->generateAccessToken();
                        $update = Customer::updateAll(['access_token' => $usermodel->access_token, 'access_token_expiry' => $usermodel->access_token_expired_at, 'access_token_updated' => $date, 'updated_date' => $date], ['id' => $cusomter_id]);
                        if($update){
                            $responseData = [
                                'verified'     => true,
                                'message'      => '',
                                'access_token' =>  $usermodel->access_token,
                            ];
                            return $responseData;
                        }
                }
                else{
                    $responseData = [
                        'verified'     => false,
                        'message'      => 'The OTP entered by you is incorrect. Please enter a correct OTP.',
                    ];
                    return $responseData;
                }
            }
            else{
                // Validation error
                throw new HttpException(422, json_encode("Please enter the mandatory details to proceed further"));
            }
        }

        public function actionUploadbill(){
            set_time_limit(0);
            $access_token  = $this->getBearerAccessToken();
            if(Yii::$app->request->post('campaign_id') && isset($access_token)){
                $campaign_id    = Yii::$app->request->post('campaign_id');
                $date           = date('Y-m-d H:i:s');
                $model          = new Customer();
                $customers      = $model->getCustomerDetailsbyAccessToken($campaign_id,$access_token);
                if(isset($customers['customer_id']) && isset($customers['access_token_expiry']) && $customers['access_token_expiry'] > $date){
                    $customer_id        = $customers['customer_id'];
                    $customer_data_id   = $customers['custdata_id'];
                    $customername       = $customers['customername'];
                    $mobile             = $customers['mobile'];
                    $bills_url          = '';
                    if(Yii::$app->request->post('bill')){
                        $file = Yii::$app->request->post('bill');
                        // Decode base64 data
                        list($type, $data) = explode(';', $file);
                        list(, $data) = explode(',', $data);
                        $file_data = base64_decode($data);

                        // Get file mime type
                        $finfo = finfo_open();
                        $file_mime_type = finfo_buffer($finfo, $file_data, FILEINFO_MIME_TYPE);

                        // File extension from mime type
                        if ($file_mime_type == 'image/jpeg' || $file_mime_type == 'image/jpg')
                            $file_type = 'jpeg';
                        else if ($file_mime_type == 'image/png')
                            $file_type = 'png';
                        else if ($file_mime_type == 'image/gif')
                            $file_type = 'gif';
                        else
                            $file_type = 'other';

                        if (in_array($file_type, ['jpeg', 'png', 'gif'])) {
                            $file_name = uniqid() . '.' . $file_type;
                            file_put_contents('uploads/bills/' . $file_name, $file_data);

                            $bills_url = \yii\helpers\Url::home(true) . 'uploads/bills/' . $file_name;
                        }
                        $billsnapmodel                      = new Billsnap();
                        $billsnapmodel->bill_image          = $bills_url;
                        $billsnapmodel->booking_id          = $customer_data_id;
                        $billsnapmodel->created_date        = $date;
                        $billsnapmodel->updated_date        = $date;
                        if($billsnapmodel->validate() && $billsnapmodel->save()){
                            $billsnap_id = $billsnapmodel->id;
                            $getocrhelper  = new GetOcrDataHelper();
                            $ocrdata       = $getocrhelper->ocrStep1($billsnap_id);
                            $campaigns     = Campaign::find()->where(['id'=>$campaign_id])->one();
                            if($ocrdata){
                                $billsnap = Billsnap::find()->where(['id'=>$billsnap_id])->one();
                                if(isset($billsnap) && !empty($billsnap)){
                                    if($billsnap->status == 100){
                                        $message = 'Maximum filesize accepted is 4 MB';
                                    }
                                    else if($billsnap->status == 101){
                                        $message = $campaigns->postregmsginvalid;
                                    }
                                    else{
                                        $message = $campaigns->postregmsgvalid;
                                        $this->sentValidMsg($campaign_id,$mobile);
                                    }
                                    $sql                = "UPDATE booking SET status = 1,updated_date=now() WHERE id = ".$customer_data_id;
                                    $query              = Yii::$app->db->createCommand($sql)->execute();
                                    $responseData = [
                                        'code'         =>  $billsnap->status ,
                                        'message'      =>  $message
                                    ];
                                    return $responseData;
                                }
                                else{
                                    throw new HttpException(422, json_encode('Server error! Please try after some time.'));
                                }
                            }
                        }
                        else{
                            throw new HttpException(422, json_encode($billsnapmodel->errors));
                        }
                    }
                    else{
                        throw new HttpException(422, json_encode("Please upload the bill!"));
                    }
                }
                else{
                    throw new HttpException(401, json_encode("Unauthorized user access!!"));
                }
            }
            else{
                throw new HttpException(422, json_encode("Please enter the mandatory details to proceed further"));
            }
        }

        private function sentValidMsg($campaign_id,$mobile){
            if(isset($campaign_id) && !empty($campaign_id)){
                $campaigns               = Campaign::find()->where(['id'=>$campaign_id])->one();
                $postregmsgvalid         = $campaigns->postregmsgvalid;
                $message     = new Message();
                $message->sendSMS($campaign_id,$mobile,$postregmsgvalid,'Post Booking Message');
               /* $smsmsg      = urlencode($postregmsgvalid);
                $msgHelper   = new MsgHelper();
                $msgHelper->sendSMS($mobile,$smsmsg);*/
                $sql = "INSERT INTO logs(campaign_id, category,description, created_date ) VALUES ($campaign_id,'Post Booking Message','Mobile No :$mobile Message :$postregmsgvalid ; ',now());";
                $query = Yii::$app->db->createCommand($sql)->execute();
            }
            else{
                throw new HttpException(422, json_encode("Campaign ID should not be empty!"));
            }
        }

        public function actionGetstatelist(){
            $states = State::find()->all();
            return $states;
        }

        public function actionGetcitylist(){
            $cities = City::find()->all();
            return $cities;
        }

        public function actionGetcitylistbystate($state_id){
            $stateid = !empty($state_id)?$state_id:0;
            $cities = City::find()->where(['state_id' => $stateid ])->all();
            return $cities;
        }

        public function actionGetstatelistwithouttn(){
            $restrict_state_id   = Yii::$app->params['restrict_state_id'];
            $states = State::find()->where('id != :id ',['id'=>$restrict_state_id])->all();
            return $states;
        }


    }