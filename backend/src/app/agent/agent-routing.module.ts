import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TasklistComponent } from './tasklist.component';
import { TaskComponent } from './task.component';

const routes: Routes = [
  {
      path: '',
      data: {
        title: 'Agent'
      },
      children: [
          {
              path: 'tasklist',
              component: TasklistComponent,
              data: {
                  title: 'Task List'
              }
          },
          {
              path: 'task/:id',
              component: TaskComponent,
              data: {
                  title: 'Update'
              }
          },
          {
              path: '',
              pathMatch: 'full',
              redirectTo: 'tasklist'
          }
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgentRoutingModule {}
