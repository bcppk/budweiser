import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import {ContainsValidators} from "../shared/contains-validator.directive";
import { Router } from '@angular/router';

import {ReportDataService} from "../model/report-data.service";
import {StaffService} from "../model/staff.service";
import {Reassign} from "../model/reassign";
import {GlobalService} from "../model/global.service";
import {Campaign} from "../model/campaign";

import * as moment from "moment";

@Component({
    selector: 'app-tasklist',
    templateUrl: 'tasklist.component.html'
})
export class TasklistComponent implements OnInit {

    private _form:FormGroup;
    private _formErrors:any;
    private _reassigndetails:Reassign[];
    private _campaigns:Campaign[];
    private _campaignId:number;
    private _campaignName:string;
    private _unassignedwinnerlist:number;
    private _reportData:any = {};
    private _errorMessage:string;
    private _successMessage:string;
    private _submitted:boolean = false;
    private _frmsubmitted:boolean = false;
    private _csvdata = [];
    private _startdate:string = '';
    private _enddate:string = '';
    private itemRows:any = {};
    private _daterange:any;
    private _userid:number;

  constructor(private _reportDataService:ReportDataService,
              private _staffService:StaffService,
              private _router:Router,
              private _formBuilder:FormBuilder,
              private _globalService:GlobalService) {

      this._form = this._formBuilder.group({
          campaign_id: ['', Validators.compose([
              Validators.required,
          ])],
          filterDate: ['', Validators.compose([])]
      });

  }

  ngOnInit() {
      sessionStorage.removeItem('configCmpId');
      this.getCampaigns();
  }

    public getCampaigns() {
        this._campaigns = null;
        this._reportDataService.getMyCampaigns()
            .subscribe(
                campaigns => {
                    this._campaigns = campaigns;
                },
                error =>  {
                    // unauthorized access
                    if(error.status == 401 || error.status == 403) {
                        this._staffService.unauthorizedAccess(error);
                    } else {
                        this._errorMessage = error.data.message;
                    }
                }
            );
    }

    public tasklist(campaignId:number, daterange:any) {
        this._submitted = true;
        this._reassigndetails = null;
        this._daterange = daterange;

        sessionStorage.setItem('configCmpId', campaignId.toString());

        if(daterange) {
            this._startdate = moment(daterange[0]).format("YYYY-MM-DD");
            this._enddate = moment(daterange[1]).format("YYYY-MM-DD");
        }

        this._reportDataService.getMyTasklist(campaignId, this._startdate, this._enddate)
            .subscribe(
                data => {
                    this._reassigndetails = data.agentdatalist;
                    this._submitted = false;
                },
                error =>  {
                    this._submitted = false;
                    // unauthorized access
                    if(error.status == 401 || error.status == 403) {
                        this._staffService.unauthorizedAccess(error);
                    } else {
                        this._errorMessage = error.data.message;
                    }
                }
            );
    }

    public viewTask(resd:Reassign):void {
        this._router.navigate(['/agent/task', resd.booking_id]);
    }
}
