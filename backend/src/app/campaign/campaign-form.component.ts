import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {CustomValidators} from 'ng2-validation';
import {ContainsValidators} from "../shared/contains-validator.directive";
import {FormGroup, FormBuilder, FormControl, FormArray, Validators} from "@angular/forms";
import { TabsetComponent } from 'ngx-bootstrap';


import {CampaignDataService} from "../model/campaign-data.service";
import {Campaign} from "../model/campaign";
import {StaffService} from "../model/staff.service";
import {GlobalService} from "../model/global.service";

import * as moment from "moment";

@Component({
    selector: 'app-campaign',
    templateUrl: './campaign-form.component.html'
})
export class CampaignFormComponent implements OnInit, OnDestroy{
    private _mode:string = '';

    private _id:number;
    private _parameters:any;
    private _campaign:Campaign;
    private _minDate:any;
    private _maxDate:any;
    private _startDate:any;
    private _endDate:any;

    // Error and success messages
    private _errorMessage:string;

    // Forms
    private _form:FormGroup;
    private _form2:FormGroup;
    private _formErrors:any;
    private _tab1Submitted:boolean = false;
    private _tab2Submitted:boolean = false;

    // Status Types
    private _statusTypes:any = {};

    // tab object
    @ViewChild('campaignTabs') campaignTabs: TabsetComponent;

    // file object
    @ViewChild('sitebanner') sitebanner;

    constructor(private _campaignDataService:CampaignDataService,
                private _staffService:StaffService,
                private _globalService:GlobalService,
                private _router:Router,
                private _activatedRoute:ActivatedRoute,
                private _formBuilder:FormBuilder) {

        // Construct form group
        this._form = _formBuilder.group({
            title: ['', Validators.compose([
                Validators.required,
            ])],
            contactno: ['', Validators.compose([
                Validators.required,
                Validators.minLength(10),
                Validators.maxLength(10),
                CustomValidators.number,
            ])],
            startdate: ['', Validators.compose([
                Validators.required,
            ])],
            enddate: ['', Validators.compose([
                Validators.required,
            ])],
            ocr_enable: ['', Validators.compose([])],
            sitebanner: ['', Validators.compose([])],
            status: ['', Validators.compose([
                Validators.required,
                // Custom validator for checking value against list of values
                ContainsValidators.contains('value', CampaignDataService.getStatusTypes())
            ])],
        }, {
            validator: validateDateTime(['startdate', 'enddate'])
        });

        // Construct form2 group
        this._form2 = _formBuilder.group({
            regtext: ['', Validators.compose([
                Validators.required,
            ])],
            txtcontact: ['', Validators.compose([])],
            txtterms: ['', Validators.compose([
              Validators.required,
            ])],
            postregmsgvalid: ['', Validators.compose([
              Validators.required,
            ])],
            postregmsginvalid: ['', Validators.compose([
                Validators.required,
            ])],
        });

        this._statusTypes = CampaignDataService.getStatusTypes();

        this._form.valueChanges
            .subscribe(data => this.onValueChanged(data));

        this._form2.valueChanges
            .subscribe(data => this.onValueChanged2(data));

    }

    private _setFormErrors(errorFields:any):void{
        for (let key in errorFields) {
            let errorField = errorFields[key];
            // skip loop if the property is from prototype
            if (!this._formErrors.hasOwnProperty(key)) continue;

            // let message = errorFields[error.field];
            this._formErrors[key].valid = false;
            this._formErrors[key].message = errorField;
        }
    }

    private _resetFormErrors():void{
        this._formErrors = {
            title: {valid: true, message: ''},
            contactno: {valid: true, message: ''},
            startdate: {valid: true, message: ''},
            enddate: {valid: true, message: ''},
            status: {valid: true, message: ''},
            ocr_enable: {valid: true, message: ''},
            sitebanner: {valid: true, message: ''},

            regtext: {valid: true, message: ''},
            txtcontact: {valid: true, message: ''},
            txtterms: {valid: true, message: ''},
            postregmsgvalid: {valid: true, message: ''},
            postregmsginvalid: {valid: true, message: ''},
        };
    }

    private _isValid(field):boolean {
        let isValid:boolean = false;

        // If the field is not touched and invalid, it is considered as initial loaded form. Thus set as true
        if(this._form.controls[field].touched == false) {
            isValid = true;
        }
        // If the field is touched and valid value, then it is considered as valid.
        else if(this._form.controls[field].touched == true && this._form.controls[field].valid == true) {
            isValid = true;
        }

        return isValid;
    }

    private _isValid2(field):boolean {
        let isValid:boolean = false;

        // If the field is not touched and invalid, it is considered as initial loaded form. Thus set as true
        if(this._form2.controls[field].touched == false) {
            isValid = true;
        }
        // If the field is touched and valid value, then it is considered as valid.
        else if(this._form2.controls[field].touched == true && this._form2.controls[field].valid == true) {
            isValid = true;
        }

        return isValid;
    }

    public onValueChanged(data?: any) {
        if (!this._form) { return; }
        const form = this._form;
        for (let field in this._formErrors) {
            // clear previous error message (if any)
            let control = form.get(field);
            if (control && control.dirty) {
                this._formErrors[field].valid = true;
                this._formErrors[field].message = '';
            }
        }
    }

    public onValueChanged2(data?: any) {
        if (!this._form2) { return; }
        const form = this._form2;
        for (let field in this._formErrors) {
            // clear previous error message (if any)
            let control = form.get(field);
            if (control && control.dirty) {
                this._formErrors[field].valid = true;
                this._formErrors[field].message = '';
            }
        }
    }

    private _resetCampaign() {
        this._campaign = new Campaign();
        this._campaign.title = '';
        this._campaign.contactno = '';
        this._campaign.startdate = null;
        this._campaign.enddate = null;
        this._campaign.status = 1;
        this._campaign.ocr_enable = 0;
        this._campaign.sitebanner = '';
        this._campaign.regtext = '';
        this._campaign.txtcontact = '';
        this._campaign.txtterms = '';
        this._campaign.postregmsgvalid = '';
        this._campaign.postregmsginvalid = '';
    }

    private _canEdit(campaign:Campaign) {
        /*const startDate:Date = new Date(campaign.startdate);
        const endDate:Date = new Date(campaign.enddate);
        const curDate = new Date();

        if(curDate >= startDate) {
            this._errorMessage = "You can not edit this campaign, it's already has been started.";
            return false;
        }*/

        return true;
    }

    public ngOnInit() {
        this._minDate = new Date();

        this._resetFormErrors();
        this._resetCampaign();

        // _route is activated route service. this._route.params is observable.
        // subscribe is method that takes function to retrieve parameters when it is changed.
        this._parameters = this._activatedRoute.params.subscribe(params => {
            // plus(+) is to convert 'id' to number
            if(typeof params['id'] !== "undefined") {
                this._id = Number.parseInt(params['id']);
                this._errorMessage = "";
                this._campaignDataService.getCampaignById(this._id)
                    .subscribe(
                        campaign => {
                            this._campaign = campaign;
                            if(campaign.startdate !==null && campaign.enddate !==null) {
                                this._startDate = new Date(campaign.startdate);
                                this._endDate = new Date(campaign.enddate);
                                this._minDate = new Date(this._campaign.startdate);
                                this._maxDate = new Date(campaign.enddate);
                            } else {
                                this._startDate = null;
                                this._endDate = null;
                                this._maxDate = null;
                            }
                            this._mode = 'update';
                        },
                        error => {
                            // unauthorized access
                            if(error.status == 401 || error.status == 403) {
                                this._staffService.unauthorizedAccess(error);
                            } else {
                                this._errorMessage = error.data.message;
                            }
                        }
                    );
            } else {
                this._startDate = null;
                this._endDate = null;
                this._maxDate = null;
                this._mode = 'create';

            }
        });
    }

    public ngOnDestroy() {
        this._parameters.unsubscribe();
        this._campaign = new Campaign();
    }

    public handleFileChange(e) {
        const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        let reader = new FileReader();

        reader.onload = this._setFileUpload.bind(this);
        reader.readAsDataURL(file);
    }

    private _setFileUpload(e) {
        let  reader = e.target;
        this._campaign.sitebanner = reader.result;
    }

    public onSubmit() {
        this._tab1Submitted = true;
        this._resetFormErrors();
        const fileBrowser = this.sitebanner.nativeElement;
        if(fileBrowser.files.length == 0) {
            this._campaign.sitebanner = '';
        }
        if(this._mode == 'create') {
            this._campaignDataService.addCampaign(this._campaign)
                .subscribe(
                    result => {
                        if(result.success) {
                            this._campaign = result.data;
                            this._minDate = new Date(this._campaign.startdate);
                            this._maxDate = new Date(this._campaign.enddate);
                            this._errorMessage = '';
                            this.selectTab(1);
                        } else {
                            this._tab1Submitted = false;
                        }
                    },
                    error => {
                        this._tab1Submitted = false;
                        // Validation errors
                        if(error.status == 422) {
                            let errorFields = JSON.parse(error.data.message);
                            this._setFormErrors(errorFields);
                        }
                        // Unauthorized Access
                        else if(error.status == 401 || error.status == 403) {
                            this._staffService.unauthorizedAccess(error);
                        }
                        // All other errors
                        else {
                            this._errorMessage = error.data.message;
                        }
                    }
                );
        } else if(this._mode == 'update' && this._canEdit(this._campaign)) {
            this._campaignDataService.updateCampaignById(this._campaign)
                .subscribe(
                    result => {
                        if(result.success) {
                            this._campaign = result.data;
                            this._errorMessage = '';
                            this.selectTab(1);
                        } else {
                            this._tab1Submitted = false;
                        }
                    },
                    error => {
                        this._tab1Submitted = false;
                        // Validation errors
                        if(error.status == 422) {
                            let errorFields = JSON.parse(error.data.message);
                            this._setFormErrors(errorFields);
                            //this._setFormErrors(error.data);
                        }
                        // Unauthorized Access
                        else if(error.status == 401 || error.status == 403) {
                            this._staffService.unauthorizedAccess(error);
                        }
                        // All other errors
                        else {
                            this._errorMessage = error.data.message;
                        }
                    }
                );
        } else {
            this._tab1Submitted = false;
        }
    }

    public onSubmitTab2() {
        this._errorMessage = '';
        if(this._canEdit(this._campaign)) {
            this._tab2Submitted = true;
            this._resetFormErrors();
            this._campaignDataService.updateCampaignById(this._campaign)
                .subscribe(
                    result => {
                        if (result.success) {
                            this._router.navigate(['/campaign']);
                        } else {
                            this._tab2Submitted = false;
                        }
                    },
                    error => {
                        this._tab2Submitted = false;
                        // Validation errors
                        if (error.status == 422) {
                            let errorFields = JSON.parse(error.data.message);
                            this._setFormErrors(errorFields);
                        }
                        // Unauthorized Access
                        else if (error.status == 401 || error.status == 403) {
                            this._staffService.unauthorizedAccess(error);
                        }
                        // All other errors
                        else {
                            this._errorMessage = error.data.message;
                        }
                    }
                );
        }
    }

    public selectTab(tab_id: number) {
        this.campaignTabs.tabs[tab_id].disabled = false;
        this.campaignTabs.tabs[tab_id].active = true;
        this.campaignTabs.tabs[tab_id - 1].disabled = true;
    }
}

function validateDateTime(fieldKeys:any) {
    return (group: FormGroup) => {
        for(let i = 0; i < fieldKeys.length; i++) {
            let field = group.controls[fieldKeys[i]];
            if(typeof field !== "undefined" && (field.value != "" && field.value != null)) {
                if(moment(field.value, "YYYY-MM-DD", true).isValid() == false) {
                    return field.setErrors({validateDateTime: true});
                }
            }
        }
    }
}

function validateTime(control: FormControl) {
    const slottime = control.value;
    if (!slottime) {
        return null;
    }

    const hours = slottime.getHours();
    if (hours < 0 || hours > 23) {
        return { outOfRange: true };
    }

    const minutes = slottime.getMinutes();
    if (minutes < 0 || hours > 59) {
        return { outOfRange: true };
    }

    return null;
}