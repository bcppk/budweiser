import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import swal, {SweetAlertOptions} from 'sweetalert2';

import {CampaignDataService} from "../model/campaign-data.service";
import {Campaign} from "../model/campaign";
import {StaffService} from "../model/staff.service";

@Component({
    templateUrl: './campaign-list.component.html',
})
export class CampaignListComponent implements OnInit{
    private _campaigns:Campaign[];
    private _errorMessage:string;
    private _role:any;

    constructor(private _campaignDataService:CampaignDataService,
                private _staffService:StaffService,
                private _router:Router) {}

    ngOnInit() {
        this._role = this._staffService.getRole();
        this.getCampaigns();
    }

    public getCampaigns() {
        this._campaigns = null;
        this._campaignDataService.getAllCampaigns()
            .subscribe(
                campaigns => {
                    this._campaigns = campaigns
                },
                error =>  {
                    // unauthorized access
                    if(error.status == 401 || error.status == 403) {
                        this._staffService.unauthorizedAccess(error);
                    } else {
                        this._errorMessage = error.data.message;
                    }
                }
            );
    }

    public viewCampaign(campaign:Campaign):void {
        this._router.navigate(['/campaign', campaign.id]);
    }

    /*public confirmDeleteReward(reward:Reward):void {
        // Due to sweet alert scope issue, define as function variable and pass to swal

        let parent = this;
        // let getUsers = this.getUsers;
        this._errorMessage = '';

        swal({
            title: 'Are you sure?',
            text: "Once delete, you won't be able to revert this!",
            type: 'question',
            showLoaderOnConfirm: true,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    parent._rewardDataService.deleteRewardById(reward.id)
                        .subscribe(
                            result => {
                                parent.getRewards();
                                resolve();
                            },
                            error =>  {
                                // unauthorized access
                                if(error.status == 401 || error.status == 403) {
                                    parent._staffService.unauthorizedAccess(error);
                                } else {
                                    parent._errorMessage = error.data.message;
                                }
                                resolve();

                            }
                        );
                })
            }
        }).then(function(result) {
            // handle confirm, result is needed for modals with input

        }, function(dismiss) {
            // dismiss can be "cancel" | "close" | "outside"
        });
    }*/
}