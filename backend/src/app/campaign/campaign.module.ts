import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

import {CampaignListComponent} from './campaign-list.component';
import {CampaignFormComponent} from './campaign-form.component';
import {CampaignRoutingModule} from './campaign-routing.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        CampaignRoutingModule,
        AngularMultiSelectModule,
    ],
    declarations: [
        CampaignListComponent,
        CampaignFormComponent,
    ]
})
export class CampaignModule { }
