import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BaseChartDirective }   from 'ng2-charts/ng2-charts';

/*import {CampaignDataService} from "../model/campaign-data.service";
import {DashboardDataService} from "../model/dashboard-data.service";
import {Campaign} from "../model/campaign";*/
import {StaffService} from "../model/staff.service";

import * as moment from "moment";

@Component({
    selector: 'app-dashboard',
    templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {
    /*@ViewChild(BaseChartDirective) chartComponent: BaseChartDirective;
    public legendData: any;

    private _form:FormGroup;
    private _campaigns:Campaign[];
    private _campaignData:any = {};
    private _errorMessage:string;
    private _submitted:boolean = false;
    private _istn:boolean = false;

    // Time Based Report
    public TBRlineChartData:Array<any> = [{data: [], label: 'Total Message'}];
    public TBRlineChartLabels:Array<any> = [];

    // Last Week Report
    public LWRlineChartData:Array<any> = [{data: [], label: 'Total Message'}];
    public LWRlineChartLabels:Array<any> = [];

    // WOW Report
    public wowlineChartData:Array<any> = [{data: [], label: 'Total Message'}];
    public wowlineChartLabels:Array<any> = [];

    // Chart Config Options
    public lineChartOptions:any = {
        responsive: true
    };
    public lineChartColors:Array<any> = [
        { // grey
            backgroundColor: '#6241e2',
            borderColor: '#6241e2',
            pointBackgroundColor: '#6241e2',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#6241e2'
        }
    ];
    public lineChartColors2:Array<any> = [
        { // grey
            backgroundColor: '#00A65A',
            borderColor: '#00A65A',
            pointBackgroundColor: '#00A65A',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#00A65A'
        }
    ];
    public lineChartLegend:boolean = false;
    public lineChartType:string = 'bar';

    // Latest Messages Report
    public lastestMsgs:Array<any> = [];

    // Circlewise Entries Report
    public doughnutChartLabels:Array<string> = [];
    public doughnutChartData:Array<number> = [];
    public doughnutChartType:string = 'doughnut';
    public circleWiseData:Array<any> = [];

    private options: any = {
        legend: { position: 'left' }
    }*/

  constructor(/*private _campaignDataService:CampaignDataService,
              private _dashboardDataService:DashboardDataService,*/
              private _staffService:StaffService,
              private _formBuilder:FormBuilder,
              private _router:Router) {

      /*this._form = this._formBuilder.group({
          campaign_id: ['', Validators.compose([
              Validators.required,
          ])]
      });*/
  }

  ngOnInit() {
      // this.getCampaigns();
  }

  /*public getCampaigns() {
      this._campaigns = null;
      this._campaignDataService.getAllCampaigns()
          .subscribe(
              campaigns => {
                  this._campaigns = campaigns;
              },
              error =>  {
                  // unauthorized access
                  if(error.status == 401 || error.status == 403) {
                      this._staffService.unauthorizedAccess(error);
                  } else {
                      this._errorMessage = error.data.message;
                  }
              }
          );
  }

  public getCampaignData(campaignId:number) {
      this._errorMessage = '';
      this._submitted = true;

      this._dashboardDataService.getCampaignStats(campaignId)
          .subscribe(
              result => {
                  this._campaignData = result;
                  this._submitted = false;
                  this._istn = result.istn;

                  if (Object.getOwnPropertyNames(result.timebasedReport).length !== 0) {
                      this.TBRlineChartData[0].data = result.timebasedReport.data;
                      this.TBRlineChartLabels = result.timebasedReport.labels;
                  }

                  if (Object.getOwnPropertyNames(result.weekwisereport).length !== 0) {
                      this.wowlineChartData[0].data = result.weekwisereport.data;

                      let labelitem;
                      let dt;
                      let wowlabels = [];
                      for(let i=0; i<result.weekwisereport.labels.length; i++) {
                          labelitem = result.weekwisereport.labels[i];
                          dt = labelitem.split('|');

                          wowlabels.push(moment(dt[0]).format("MMM,DD") + ' - ' + moment(dt[1]).format("MMM,DD"));
                      }

                      setTimeout( () => {

                          this.wowlineChartLabels = wowlabels;

                      });
                  }

                  if (Object.getOwnPropertyNames(result.lastweekReport).length !== 0) {
                      this.LWRlineChartData[0].data = result.lastweekReport.data;
                      this.LWRlineChartLabels = this.getNumLastDays(7);// result.lastweekReport.labels;
                  }

                  if(result.lastestMsg !== 0) {
                      this.lastestMsgs = result.lastestMsg;
                  }

                  if(result.circlewiseEntries !== 0) {
                      this.doughnutChartLabels = [];
                      this.doughnutChartData = [];
                      let circleLabels:string[] = [];
                      let circleData:number[] = [];
                      for(let i=0; i<result.circlewiseEntries.length; i++) {
                          let item = result.circlewiseEntries[i];
                          circleLabels.push(item.circle);
                          this.doughnutChartData.push(Number.parseInt(item.qty));
                      }

                      setTimeout( () => {

                          this.doughnutChartLabels = circleLabels;

                      });



                      this.circleWiseData = result.circlewiseEntries;
                      //this.doughnutChartLabels = circleLabels;console.log(this.doughnutChartLabels)
                      //this.doughnutChartData = circleData;
                  }
              },
              error => {
                  this._submitted = false;
                  // unauthorized access
                  if(error.status == 401 || error.status == 403) {
                      this._staffService.unauthorizedAccess(error);
                  } else {
                      this._errorMessage = error.data.message;
                  }
              }
          );
  }

  private getNumLastDays(numDays:number) {
      let daysLabel:Array<any> = [];
      let date = new Date();
      let lastDate = new Date(date.getTime() - (numDays * 24 * 60 * 60 * 1000));

      while (lastDate < date) {
          daysLabel.push(moment(lastDate).format("ddd DD-MMM"));
          lastDate.setDate(lastDate.getDate() + 1);
      }

      return daysLabel;
  }*/
}
