import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Rx';

import {GlobalService} from './global.service';
import {StaffService} from './staff.service';
import {Campaign} from './campaign';
import {State} from './state';
import {AuthHttp} from 'angular2-jwt';

import * as moment from "moment";


@Injectable()
export class CampaignDataService {

    constructor(private _globalService:GlobalService,
                private _staffService:StaffService,
                private _authHttp: AuthHttp){
    }

    // POST /v1/user
    public addCampaign(campaign:Campaign):Observable<any>{
        let headers = this.getHeaders();
        campaign.startdate = moment(campaign.startdate).format("YYYY-MM-DD");
        campaign.enddate = moment(campaign.enddate).format("YYYY-MM-DD");

        return this._authHttp.post(
            this._globalService.apiHost+'/staff/createcampaign',
            JSON.stringify(campaign),
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return response;
            })
            .catch(this.handleError);
    }

    // PUT /v1/user/1
    public updateCampaignById(campaign:Campaign):Observable<any>{
        let headers = this.getHeaders();

        campaign.startdate = moment(campaign.startdate).format("YYYY-MM-DD");
        campaign.enddate = moment(campaign.enddate).format("YYYY-MM-DD");

        return this._authHttp.put(
            this._globalService.apiHost+'/staff/updatecampaigns?id='+campaign.id,
            JSON.stringify(campaign),
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return response;
            })
            .catch(this.handleError);
    }

    // GET /v1/campaign
    public getAllCampaigns(): Observable<Campaign[]> {
        let headers = this.getHeaders();

        return this._authHttp.get(
            this._globalService.apiHost+'/staff/campaigns',
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return <Campaign[]>response.data.campaigns;
            })
            .catch(this.handleError);
    }

    // GET /v1/user/1
    public getCampaignById(id:number):Observable<Campaign> {
        let headers = this.getHeaders();

        return this._authHttp.get(
            this._globalService.apiHost+'/staff/campaign?id='+id,
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return <Campaign>response.data.campaigns;
            })
            .catch(this.handleError);
    }

    public getAllStates(): Observable<State[]> {
        let headers = this.getHeaders();

        return this._authHttp.get(
            this._globalService.apiHost+'/user/getstatelist',
            {
              headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
              return <State[]>response.data;
            })
            .catch(this.handleError);
    }

    public static getStatusTypes():Array<any>{
        return [
            {
                label: 'Active',
                value: 1
            },
            {
                label: 'Inactive',
                value: 0
            }
        ];
    }

    public static getYesnoTypes():Array<any>{
        return [
            {
                label: 'Yes',
                value: 1
            },
            {
                label: 'No',
                value: 0
            }
        ];
    }

    private getHeaders():Headers {
        return new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+this._staffService.getToken(),
        });
    }

    private handleError (error: Response | any) {

        let errorMessage:any = {};
        // Connection error
        if(error.status == 0) {
            errorMessage = {
                success: false,
                status: 0,
                data: "Sorry, there was a connection error occurred. Please try again.",
            };
        }
        else {
            errorMessage = error.json();
        }
        return Observable.throw(errorMessage);
    }
}
