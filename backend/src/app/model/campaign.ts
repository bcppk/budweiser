export class Campaign{
    id:number;
    analyticscode: string;
    title:string;
    regtext:string;
    contactno:string;
    txtcontact:string;
    txtterms:string;
    postregmsgvalid:string;
    postregmsginvalid:string;
    startdate:string;
    enddate:string;
    disabledweekdays: string;
    sitebanner:string;
    ocr_enable: number;
    status:number;
    created_date:string;
    updated_date:string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}