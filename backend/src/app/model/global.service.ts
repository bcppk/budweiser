import {Injectable} from '@angular/core';
import * as moment from "moment";
import {environment} from '../../environments/environment';


@Injectable()
export class GlobalService{
    public apiHost:string;
    public setting:any = {};
    public limit:number;

    constructor(){
        if(environment.production == true) {
            this.apiHost = 'https://api.budbasement.in/v1';
            this.limit = 100;
        } else {
            this.apiHost = 'https://api.budbasement.in/v1';
            this.limit = 100;
        }
    }

    loadGlobalSettingsFromLocalStorage():void{
        if(localStorage.getItem('backend-setting') != null){
            this.setting = JSON.parse(localStorage.getItem('backend-setting'));
        }

    }

    public calculateDays(date1, date2) {
        date1 = new Date(date1);
        date2 = new Date(date2);

        let diffc = date1.getTime() - date2.getTime();
        //getTime() function used to convert a date into milliseconds. This is needed in order to perform calculations.

        let days = Math.round(Math.abs(diffc/(1000*60*60*24)));
        //this is the actual equation that calculates the number of days.

        days += 1;

        return days;
    }

    public calculateHours(date1, date2) {
        date1 = new Date(date1);
        date2 = new Date(date2);

        let diffc = date1.getTime() - date2.getTime();
        //getTime() function used to convert a date into milliseconds. This is needed in order to perform calculations.

        let hours = Math.round(Math.abs(diffc/(1000*60*60)));
        //this is the actual equation that calculates the number of days.

        return hours;
    }
}