import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Rx';

import {GlobalService} from './global.service';
import {StaffService} from './staff.service';
import {Campaign} from './campaign';
import {Agent} from './agent';
import {AuthHttp} from 'angular2-jwt';

import * as moment from "moment";


@Injectable()
export class ReportDataService {

    constructor(private _globalService:GlobalService,
                private _staffService:StaffService,
                private _authHttp: AuthHttp) {
    }

    getDashboardData(id:number, state_id: number, startdate:string, enddate:string) {
        let headers = this.getHeaders();

        return this._authHttp.get(
            this._globalService.apiHost+'/staff/tldashboard?campaign_id=' + id + '&state_id=' + state_id + '&startdate=' + startdate + '&enddate=' + enddate,
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return response.data;
            })
            .catch(this.handleError);
    }

    getWinnerlistData(id:number, state_id:number, startdate:string, enddate:string, userid:number) {
        let headers = this.getHeaders();

        return this._authHttp.get(
            this._globalService.apiHost+'/staff/reassignlist?campaign_id=' + id + '&state_id=' + state_id + '&startdate=' + startdate + '&enddate=' + enddate + '&user_id=' + userid,
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return response.data;
            })
            .catch(this.handleError);
    }

    getBulkAssignData(id:number, state_id:number, startdate:string, enddate:string) {
        let headers = this.getHeaders();

        return this._authHttp.get(
            this._globalService.apiHost+'/staff/bulkassignlist?campaign_id=' + id + '&state_id=' + state_id + '&startdate=' + startdate + '&enddate=' + enddate,
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return response.data;
            })
            .catch(this.handleError);
    }

    saveBulkAssignment(campaign_id:number, state_id:number, bulkassignvalue:any, startdate:string, enddate:string) {
        let headers = this.getHeaders();

        return this._authHttp
            .post(
                this._globalService.apiHost + '/staff/bulkassigntoagent',
                JSON.stringify({
                    "campaign_id": campaign_id,
                    "state_id": state_id,
                    "bulkassignvalue": bulkassignvalue,
                    "startdate": startdate,
                    "enddate": enddate
                }),
                {headers: headers}
            )
            .map(response => response.json())
            .map((response) => {
                return response;
            })
            .catch(this.handleError);
    }

    processCustomerData(campaign_id:number, user_id:number, customer_data_id:number, status:number, agent_comments:string) {
        let headers = this.getHeaders();

        return this._authHttp
            .post(
                this._globalService.apiHost + '/staff/agentdataverify',
                JSON.stringify({
                    "campaign_id": campaign_id,
                    "user_id": user_id,
                    "customer_data_id": customer_data_id,
                    "status": status,
                    "agent_comments":agent_comments
                }),
                {headers: headers}
            )
            .map(response => response.json())
            .map((response) => {
                return response;
            })
            .catch(this.handleError);
    }

    reassignToAgent(user_id:number, booking_id:string) {
        let headers = this.getHeaders();

        return this._authHttp
            .post(
                this._globalService.apiHost + '/staff/reassigntoagent',
                JSON.stringify({
                    "user_id": user_id,
                    "booking_id": booking_id
                }),
                {headers: headers}
            )
            .map(response => response.json())
            .map((response) => {
                return response;
            })
            .catch(this.handleError);
    }

    public getAllAgents(): Observable<Agent[]> {
        let headers = this.getHeaders();

        return this._authHttp.get(
            this._globalService.apiHost+'/staff/agentlist',
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return <Agent[]>response.data;
            })
            .catch(this.handleError);
    }

    public getMyCampaigns(): Observable<Campaign[]> {
        let headers = this.getHeaders();

        return this._authHttp.get(
            this._globalService.apiHost+'/staff/agentcampaignlist',
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return <Campaign[]>response.data;
            })
            .catch(this.handleError);
    }

    getMyTasklist(id:number, startdate:string, enddate:string) {
        let headers = this.getHeaders();

        return this._authHttp.get(
            this._globalService.apiHost+'/staff/agentdatalist?campaign_id=' + id + '&startdate=' + startdate + '&enddate=' + enddate,
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return response.data;
            })
            .catch(this.handleError);
    }

    getCustomerData(campaign_id:number, booking_id:number) {
        let headers = this.getHeaders();

        return this._authHttp.get(
            this._globalService.apiHost+'/staff/getbookingdata?campaign_id=' + campaign_id + '&booking_id=' + booking_id,
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return response.data;
            })
            .catch(this.handleError);
    }

    getCustomerDataByFlag(campaign_id:number, state_id: number, startdate:string, enddate:string, flag:string) {
        let headers = this.getHeaders();

        return this._authHttp.get(
            this._globalService.apiHost+'/staff/getassignedlist?campaign_id=' + campaign_id + '&state_id=' + state_id + '&startdate=' + startdate + '&enddate=' + enddate + '&flag=' + flag,
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return response.data;
            })
            .catch(this.handleError);
    }

    approveCustomerData(campaign_id:number, user_id:number, booking_id:number, qty:number, agent_comments:string) {
      let headers = this.getHeaders();

      return this._authHttp
          .post(
              this._globalService.apiHost + '/staff/agentapprovedbooking',
              JSON.stringify({
                "campaign_id": campaign_id,
                "user_id": user_id,
                "booking_id": booking_id,
                "qty": qty,
                "agent_comments":agent_comments
              }),
              {headers: headers}
          )
          .map(response => response.json())
          .map((response) => {
            return response;
          })
          .catch(this.handleError);
    }

    rejectCustomerData(campaign_id:number, user_id:number, booking_id:number, qty:number, agent_comments:string) {
      let headers = this.getHeaders();

      return this._authHttp
          .post(
              this._globalService.apiHost + '/staff/agentrejectbooking',
              JSON.stringify({
                "campaign_id": campaign_id,
                "user_id": user_id,
                "booking_id": booking_id,
                "qty": qty,
                "agent_comments":agent_comments
              }),
              {headers: headers}
          )
          .map(response => response.json())
          .map((response) => {
            return response;
          })
          .catch(this.handleError);
    }

    private handleError (error: Response | any) {

        let errorMessage:any = {};
        // Connection error
        if(error.status == 0) {
            errorMessage = {
                success: false,
                status: 0,
                data: "Sorry, there was a connection error occurred. Please try again.",
            };
        }
        else {
            errorMessage = error.json();
        }
        return Observable.throw(errorMessage);
    }

    private getHeaders():Headers {
        return new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+this._staffService.getToken(),
        });
    }

    public static getAuditorApprovalStatusTypes():Array<any>{
        return [
            {
                label: 'Select',
                value: ''
            },
            {
                label: 'Approve',
                value: 1
            },
            {
                label: 'Cancel',
                value: 2
            }
        ];
    }
}
