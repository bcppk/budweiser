export class State{
    id:number;
    statename:string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}