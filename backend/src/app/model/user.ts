export class User{
    id:number;
    campaign_id:any;
    username:string;
    email:string;
    password:string;
    role:number;
    role_label:string;
    /*department:string;
    company:string;
    phone:string;
    address:string;*/
    last_login_at:string;
    last_login_ip:string;
    confirmed_at:string;
    blocked_at:string;
    status:number;
    status_label:string;
    created_at:string;
    updated_at:string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}