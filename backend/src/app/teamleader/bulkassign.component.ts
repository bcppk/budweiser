import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import {ContainsValidators} from "../shared/contains-validator.directive";
import { Router } from '@angular/router';

import {ReportDataService} from "../model/report-data.service";
import {StaffService} from "../model/staff.service";
import {Bulkassign} from "../model/bulkassign";
import {GlobalService} from "../model/global.service";

import * as moment from "moment";

@Component({
    selector: 'app-bulkassign',
    templateUrl: 'bulkassign.component.html'
})
export class BulkassignComponent implements OnInit {

    private _form:FormGroup;
    private _formErrors:any;
    private _bulkassigndetails:Bulkassign[];
    private _campaignId:number;
    private _stateId:number;
    private _campaignName:string;
    private _unassignedwinnerlist:number;
    private _reportData:any = {};
    private _errorMessage:string;
    private _successMessage:string;
    private _submitted:boolean = false;
    private _downloadSubmitted:boolean = false;
    private _csvdata = [];
    private _startdate:string = '';
    private _enddate:string = '';
    private itemRows:any = {};

  constructor(private _reportDataService:ReportDataService,
              private _staffService:StaffService,
              private _router:Router,
              private _formBuilder:FormBuilder,
              private _globalService:GlobalService) {

  }

  ngOnInit() {
      this._campaignId = Number.parseInt(sessionStorage.getItem('campaignId'));
      this._stateId = Number.parseInt(sessionStorage.getItem('stateId'));
      this._startdate = sessionStorage.getItem('filterstartdate') || '';
      this._enddate = sessionStorage.getItem('filterenddate') || '';

      this.bulkAssignData(this._campaignId, this._stateId, this._startdate, this._enddate);
  }

  public bulkAssignData(campaignId:number, stateId:number, startdate:any, enddate:any) {
      this._bulkassigndetails = null;
      this._reportDataService.getBulkAssignData(campaignId, stateId, startdate, enddate)
          .subscribe(
              result => {
                  this._bulkassigndetails = result.bulklistdetails;
                  this._campaignName = result.campaign_name;
                  this._unassignedwinnerlist = Number.parseInt(result.unassignedrecords);
              },
              error =>  {
                  // unauthorized access
                  if(error.status == 401 || error.status == 403) {
                      this._staffService.unauthorizedAccess(error);
                  } else {
                      this._errorMessage = error.data.message;
                  }
              }
          );
  }

  public assignEntries(myForm) {
      this._submitted = true;
      let formValues = myForm.form.value.inputs;

      let count = Object.keys(formValues).length / 2;

      for(let i=0; i<count; i++) {
          if(formValues['invoice' + i] != null) {
              let userid = formValues['userid' + i];
              this.itemRows[userid] = formValues['invoice' + i];
          }
      }

      this.itemRows = JSON.stringify(this.itemRows);

      this._reportDataService.saveBulkAssignment(this._campaignId, this._stateId, this.itemRows, this._startdate, this._enddate)
          .subscribe(
              result => {
                  this._bulkassigndetails = result.data.bulklistdetails;
                  this._unassignedwinnerlist = Number.parseInt(result.data.unassignedrecords);
                  this._submitted = false;
                  this.itemRows = {};
                  myForm.reset();
              },
              error =>  {
                  this._submitted = false;
                  // unauthorized access
                  if(error.status == 401 || error.status == 403) {
                      this._staffService.unauthorizedAccess(error);
                  } else {
                      this._errorMessage = error.data.message;
                  }
              }
          );
  }
}
