import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import {ContainsValidators} from "../shared/contains-validator.directive";
import { Router } from '@angular/router';

import {CampaignDataService} from "../model/campaign-data.service";
import {ReportDataService} from "../model/report-data.service";
import {StaffService} from "../model/staff.service";
import {Campaign} from "../model/campaign";
import {State} from "../model/state";
import {GlobalService} from "../model/global.service";

import * as moment from "moment";

@Component({
    selector: 'app-dashboard',
    templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

    private _form:FormGroup;
    private _formErrors:any;
    private _campaigns:Campaign[];
    private _states:State[];
    private _campaignId:number;
    private _stateId:number;
    private _reportData:any = {};
    private _errorMessage:string;
    private _successMessage:string;
    private _submitted:boolean = false;
    private _downloadSubmitted:boolean = false;
    private _csvdata = [];
    private _startdate:string = '';
    private _enddate:string = '';

  constructor(private _campaignDataService:CampaignDataService,
              private _reportDataService:ReportDataService,
              private _staffService:StaffService,
              private _router:Router,
              private _formBuilder:FormBuilder,
              private _globalService:GlobalService) {

      this._form = this._formBuilder.group({
          campaign_id: ['', Validators.compose([
              Validators.required,
          ])],
          state_id: ['', Validators.compose([
            Validators.required,
          ])],
          filterDate: ['', Validators.compose([])]
      });

  }

  ngOnInit() {

      this.getCampaigns();
      this.getStateList();
  }

  public getStateList() {
    this._states = null;
    this._campaignDataService.getAllStates()
        .subscribe(
            states => {
              this._states = states;
            },
            error =>  {
              // unauthorized access
              if(error.status == 401 || error.status == 403) {
                this._staffService.unauthorizedAccess(error);
              } else {
                this._errorMessage = error.data.message;
              }
            }
        );
  }

  public getCampaigns() {
      this._campaigns = null;
      this._campaignDataService.getAllCampaigns()
          .subscribe(
              campaigns => {
                  this._campaigns = campaigns;
              },
              error =>  {
                  // unauthorized access
                  if(error.status == 401 || error.status == 403) {
                      this._staffService.unauthorizedAccess(error);
                  } else {
                      this._errorMessage = error.data.message;
                  }
              }
          );
  }

  public dashboardData(campaignId:number, stateId:number, daterange:any) {
      this._reportData = null;
      this._campaignId = campaignId;
      this._stateId = stateId;

      sessionStorage.setItem('campaignId', campaignId.toString());
      sessionStorage.setItem('stateId', stateId.toString());

      if(daterange) {
          this._startdate = moment(daterange[0]).format("YYYY-MM-DD");
          this._enddate = moment(daterange[1]).format("YYYY-MM-DD");

          sessionStorage.setItem('filterstartdate', this._startdate);
          sessionStorage.setItem('filterenddate', this._enddate);
      }

      this._reportDataService.getDashboardData(campaignId, stateId, this._startdate, this._enddate)
          .subscribe(
              data => {
                  this._reportData = data;
              },
              error =>  {
                  // unauthorized access
                  if(error.status == 401 || error.status == 403) {
                      this._staffService.unauthorizedAccess(error);
                  } else {
                      this._errorMessage = error.data.message;
                  }
              }
          );
  }
}
