import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import {ContainsValidators} from "../shared/contains-validator.directive";
import { Router, ActivatedRoute, Params } from '@angular/router';

import {ReportDataService} from "../model/report-data.service";
import {StaffService} from "../model/staff.service";
import {Reassign} from "../model/reassign";
import {GlobalService} from "../model/global.service";

import * as moment from "moment";

@Component({
    selector: 'app-list',
    templateUrl: 'list.component.html'
})
export class ListComponent implements OnInit {

    private _campaignId:number;
    private _stateId:number;
    private _startdate:string = '';
    private _enddate:string = '';
    private _flag:string;
    private _parameters:any;

    private _form:FormGroup;
    private _formErrors:any;
    private _reassigndetails:Reassign[];
    private _campaignName:string;
    private _errorMessage:string;
    private _successMessage:string;
    private _submitted:boolean = false;
    private _downloadSubmitted:boolean = false;
    private _csvdata = [];

  constructor(private _reportDataService:ReportDataService,
              private _staffService:StaffService,
              private _router:Router,
              private _activatedRoute:ActivatedRoute,
              private _formBuilder:FormBuilder,
              private _globalService:GlobalService) {

      /*this._form = this._formBuilder.group({
          campaign_id: ['', Validators.compose([
              Validators.required,
          ])],
          filterDate: ['', Validators.compose([])],
          userid: ['', Validators.compose([])]
      });*/

  }

  ngOnInit() {
      // this.getCampaigns();

      this._parameters = this._activatedRoute.queryParams.subscribe((params: Params) => {
          // plus(+) is to convert 'id' to number
          if(typeof params['campaign_id'] !== "undefined") {
                this._campaignId = Number.parseInt(params['campaign_id']);
                this._stateId = Number.parseInt(params['state_id']);
                this._startdate = params['startdate'];
                this._enddate = params['enddate'];
                this._flag = params['flag'];
                this._errorMessage = "";
                this._reportDataService.getCustomerDataByFlag(this._campaignId, this._stateId, this._startdate, this._enddate, this._flag)
                  .subscribe(
                      result => {
                          this._reassigndetails = result.assignedlist;
                          this._campaignName = result.campaign_name;
                      },
                      error => {
                          // unauthorized access
                          if(error.status == 401 || error.status == 403) {
                              this._staffService.unauthorizedAccess(error);
                          } else {
                              this._errorMessage = error.data.message;
                          }
                      }
                  );
          }
      });
  }

    public downloadReport() {
        this._downloadSubmitted = true;
        this._csvdata = [];

        let filename:string = 'Report';

        if(this._flag == 'totalrec') {
            filename = 'TotalRecords'
        } else if(this._flag == 'pendingrec') {
            filename = 'PendingRecords'
        } else if(this._flag == 'processedrec') {
            filename = 'ProcessedRecords'
        } else if(this._flag == 'rejectedrec') {
            filename = 'RejectedRecords'
        }

        if(this._startdate) {
            filename = filename + '_' + this._startdate + '_' + this._enddate;
        } else {
            filename = filename;
        }

        let options = {
            showLabels: true,
            headers: ['Campaign Name', 'Assigned To', 'Customer Name', 'Mobile', 'Email', 'Bill Image', 'OCR Comments', 'State', 'Assigned Date', 'Assigned Time', 'Status']
        };

        for(let i=0; i<this._reassigndetails.length; i++) {
            let item = this._reassigndetails[i];
            let dt = [null, null];
            if(item.assigned_on !== null) {
              let dt = item.assigned_on.split(' ');
            }

            this._csvdata.push({cmpname: this._campaignName, asndto: item.username, custname: item.customername, mob: item.mobile, email: item.email, bimage: item.bill_image, ocrcmnts: item.billsnap_comments, state: item.statename, asndt: dt[0], asndtime: dt[1], status: item.status});
        }

        new Angular2Csv(this._csvdata, filename, options);

        this._downloadSubmitted = false;
    }

    /*public getCampaigns() {
        this._campaigns = null;
        this._campaignDataService.getAllCampaigns()
            .subscribe(
                campaigns => {
                    this._campaigns = campaigns;
                    this.getAgents();
                },
                error =>  {
                    // unauthorized access
                    if(error.status == 401 || error.status == 403) {
                        this._staffService.unauthorizedAccess(error);
                    } else {
                        this._errorMessage = error.data.message;
                    }
                }
            );
    }

    public getAgents() {
        this._agents = null;
        this._reportDataService.getAllAgents()
            .subscribe(
                agents => {
                    this._agents = agents;
                },
                error =>  {
                    // unauthorized access
                    if(error.status == 401 || error.status == 403) {
                        this._staffService.unauthorizedAccess(error);
                    } else {
                        this._errorMessage = error.data.message;
                    }
                }
            );
    }*/
}
