import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import {ContainsValidators} from "../shared/contains-validator.directive";
import { Router } from '@angular/router';

import {ReportDataService} from "../model/report-data.service";
import {StaffService} from "../model/staff.service";
import {Reassign} from "../model/reassign";
import {GlobalService} from "../model/global.service";
import {CampaignDataService} from "../model/campaign-data.service";
import {Campaign} from "../model/campaign";
import {State} from "../model/state";
import {Agent} from "../model/agent";

import * as moment from "moment";

@Component({
    selector: 'app-reassign',
    templateUrl: 'reassign.component.html'
})
export class ReassignComponent implements OnInit {

    private _form:FormGroup;
    private _formErrors:any;
    private _reassigndetails:Reassign[];
    private _campaigns:Campaign[];
    private _states:State[];
    private _agents:Agent[];
    private _campaignId:number;
    private _stateId:number;
    private _campaignName:string;
    private _unassignedwinnerlist:number;
    private _reportData:any = {};
    private _errorMessage:string;
    private _successMessage:string;
    private _submitted:boolean = false;
    private _frmsubmitted:boolean = false;
    private _csvdata = [];
    private _startdate:string = '';
    private _enddate:string = '';
    private itemRows:any = {};
    private _daterange:any;
    private _userid:number;

  constructor(private _reportDataService:ReportDataService,
              private _campaignDataService:CampaignDataService,
              private _staffService:StaffService,
              private _router:Router,
              private _formBuilder:FormBuilder,
              private _globalService:GlobalService) {

      this._form = this._formBuilder.group({
          campaign_id: ['', Validators.compose([
              Validators.required,
          ])],
          state_id: ['', Validators.compose([
            Validators.required,
          ])],
          filterDate: ['', Validators.compose([])],
          userid: ['', Validators.compose([])]
      });

  }

  ngOnInit() {
      this.getCampaigns();
      this.getStateList();
  }

  public getStateList() {
    this._states = null;
    this._campaignDataService.getAllStates()
        .subscribe(
            states => {
              this._states = states;
            },
            error =>  {
              // unauthorized access
              if(error.status == 401 || error.status == 403) {
                this._staffService.unauthorizedAccess(error);
              } else {
                this._errorMessage = error.data.message;
              }
            }
        );
  }

    public getCampaigns() {
        this._campaigns = null;
        this._campaignDataService.getAllCampaigns()
            .subscribe(
                campaigns => {
                    this._campaigns = campaigns;
                    this.getAgents();
                },
                error =>  {
                    // unauthorized access
                    if(error.status == 401 || error.status == 403) {
                        this._staffService.unauthorizedAccess(error);
                    } else {
                        this._errorMessage = error.data.message;
                    }
                }
            );
    }

    public getAgents() {
        this._agents = null;
        this._reportDataService.getAllAgents()
            .subscribe(
                agents => {
                    this._agents = agents;
                },
                error =>  {
                    // unauthorized access
                    if(error.status == 401 || error.status == 403) {
                        this._staffService.unauthorizedAccess(error);
                    } else {
                        this._errorMessage = error.data.message;
                    }
                }
            );
    }

    public winnerlistData(campaignId:number, stateId:number, daterange:any, userid:number) {
        this._reassigndetails = null;
        this._campaignId = campaignId;
        this._daterange = daterange;
        this._userid = userid;
        this._stateId = stateId;

        if(daterange) {
            this._startdate = moment(daterange[0]).format("YYYY-MM-DD");
            this._enddate = moment(daterange[1]).format("YYYY-MM-DD");
        }

        this._reportDataService.getWinnerlistData(campaignId, stateId, this._startdate, this._enddate, userid)
            .subscribe(
                data => {
                    this._reassigndetails = data.reassignlist;
                },
                error =>  {
                    // unauthorized access
                    if(error.status == 401 || error.status == 403) {
                        this._staffService.unauthorizedAccess(error);
                    } else {
                        this._errorMessage = error.data.message;
                    }
                }
            );
    }

    public checkAll(ev) {
        this._reassigndetails.forEach(x => x.state = ev.target.checked)
    }

    public isAllChecked() {
      if(this._reassigndetails) {
          return this._reassigndetails.every(_ => _.state);
      }
    }

    public reassignEntries(myForm, userid) {
        if(userid) {
            let formValues = myForm.form.value.inputs;
            let count = Object.keys(formValues).length;
            let winnerlistids = [];

            for(let i=0; i<count; i++) {
                if(formValues['userids' + i] === true) {
                    winnerlistids.push(this._reassigndetails[i].booking_id);
                }
            }

            if(winnerlistids.length > 0) {
                this._frmsubmitted = true;
                let strWinIds = winnerlistids.join(',');

                this._reportDataService.reassignToAgent(userid, strWinIds)
                    .subscribe(
                        result => {
                            this._frmsubmitted = false;
                            this.winnerlistData(this._campaignId, this._stateId, this._daterange, this._userid);
                        },
                        error =>  {
                            this._frmsubmitted = false;
                            // unauthorized access
                            if(error.status == 401 || error.status == 403) {
                                this._staffService.unauthorizedAccess(error);
                            } else {
                                this._errorMessage = error.data.message;
                            }
                        }
                    );
            }
        }
    }
}
