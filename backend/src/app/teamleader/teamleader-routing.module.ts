import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { BulkassignComponent } from './bulkassign.component';
import { ReassignComponent } from './reassign.component';
import { ListComponent } from './list.component';

const routes: Routes = [
  {
      path: '',
      data: {
        title: 'Team Leader'
      },
      children: [
          {
              path: 'dashboard',
              component: DashboardComponent,
              data: {
                  title: 'Dashboard'
              }
          },
          {
              path: 'bulkassign',
              component: BulkassignComponent,
              data: {
                  title: 'Bulk Assign'
              }
          },
          {
              path: 'reassign',
              component: ReassignComponent,
              data: {
                  title: 'Re Assign'
              }
          },
          {
              path: 'list',
              component: ListComponent,
              data: {
                  title: 'View List'
              }
          },
          {
              path: '',
              pathMatch: 'full',
              redirectTo: 'dashboard'
          }
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamleaderRoutingModule {}
