import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
// import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {SharedModule} from '../shared/shared.module';

import { DashboardComponent } from './dashboard.component';
import { BulkassignComponent } from './bulkassign.component';
import { ReassignComponent } from './reassign.component';
import { ListComponent } from './list.component';

import { TeamleaderRoutingModule } from './teamleader-routing.module';

@NgModule({
  imports: [
      CommonModule,
      // InfiniteScrollModule,
      SharedModule,
      TeamleaderRoutingModule,
  ],
  declarations: [
      DashboardComponent,
      BulkassignComponent,
      ReassignComponent,
      ListComponent,
  ]
})
export class TeamleaderModule { }
