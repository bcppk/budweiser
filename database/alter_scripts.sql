ALTER TABLE public.booking
  ADD COLUMN state character varying(255);
  
  -- Table: public.states

-- DROP TABLE public.states;

CREATE TABLE public.states
(
  id serial NOT NULL ,
  statename character varying(150),
  CONSTRAINT states_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.states
  OWNER TO postgres;
  
  -- Table: public.cities

-- DROP TABLE public.cities;

CREATE TABLE public.cities
(
  id serial NOT NULL ,
  state_id integer,
  cityname character varying(150),
  CONSTRAINT cities_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.cities
  OWNER TO postgres;
  
ALTER TABLE public.booking
  DROP COLUMN state;
ALTER TABLE public.booking
  ADD COLUMN state_id integer;
  
ALTER TABLE public.billsnap
  ADD COLUMN qty integer;

ALTER TABLE public.customers
  ADD COLUMN num_offercode_sent integer;
ALTER TABLE public.campaigns
  ADD COLUMN max_num_offercodes integer;

  ALTER TABLE public.customers
  ADD COLUMN total_qty integer;
  
  ALTER TABLE public.campaigns
   ALTER COLUMN startdate TYPE date;
ALTER TABLE public.campaigns
   ALTER COLUMN enddate TYPE date;
   
ALTER TABLE public.campaigns
  DROP COLUMN ocr_enable;
ALTER TABLE public.campaigns
  ADD COLUMN booking_reject_msg text;
ALTER TABLE public.campaigns
  ADD COLUMN booking_excess_msg text;
ALTER TABLE public.campaigns
  ADD COLUMN ocr_enable boolean;
  
  ALTER TABLE public.customers
   ALTER COLUMN num_offercode_sent SET DEFAULT 0;
ALTER TABLE public.customers
   ALTER COLUMN total_qty SET DEFAULT 0;
   
   ALTER TABLE public.booking
  ADD COLUMN tn_answer text;
  
 ALTER TABLE public.booking
  ADD COLUMN reason integer;
COMMENT ON COLUMN public.booking.reason IS '1 - Invalid bill
2 - Poor image quality of the bill
3 - Invalid bill type 
4 - Product not found on the bill
';

ALTER TABLE public.billsnap
  ADD COLUMN invoice_no text;
ALTER TABLE public.billsnap
  ADD COLUMN store_name text;




