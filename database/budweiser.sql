-- Table: public.campaigns

-- DROP TABLE public.campaigns;

CREATE TABLE public.campaigns
(
  id serial NOT NULL ,
  analyticscode character varying(5000),
  title character varying(100) NOT NULL,
  regtext text,
  contactno character varying(15) NOT NULL,
  txtcontact text,
  txtterms text,
  postregmsgvalid character varying(1000) DEFAULT NULL::character varying,
  postregmsginvalid character varying(1000) DEFAULT NULL::character varying,
  startdate timestamp without time zone,
  enddate timestamp without time zone,
  disabledweekdays character varying(15) DEFAULT NULL::character varying,
  sitebanner character varying(50),
  ocr_enable smallint,
  status integer,
  created_date timestamp without time zone,
  updated_date timestamp without time zone,
  CONSTRAINT campaigns_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.campaigns
  OWNER TO postgres;
  
-- Table: public.rewards

-- DROP TABLE public.rewards;

CREATE TABLE public.rewards
(
  id serial NOT NULL ,
  prod_name character varying(100) NOT NULL,
  value numeric(42,0) NOT NULL,
  status smallint NOT NULL,
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  imgurl character varying(255),
  CONSTRAINT rewards_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.rewards
  OWNER TO postgres;

-- Table: public.reward_messages

-- DROP TABLE public.reward_messages;

CREATE TABLE public.reward_messages
(
  id serial NOT NULL ,
  campaign_id integer,
  reward_id integer,
  messages character varying(500),
  status integer DEFAULT 1,
  created_date timestamp without time zone,
  updated_date timestamp without time zone,
  CONSTRAINT reward_messages_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.reward_messages
  OWNER TO postgres;

  
-- Table: public.offercodes

-- DROP TABLE public.offercodes;

CREATE TABLE public.offercodes
(
  id serial NOT NULL ,
  reward_id integer ,
  campaign_id integer,
  code character varying(1000) NOT NULL,
  status integer NOT NULL,
  senton timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  CONSTRAINT offercodes_pkey PRIMARY KEY (id),
  CONSTRAINT offercodes_code_key UNIQUE (code)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.offercodes
  OWNER TO postgres;
  
  
-- Table: public.customers

-- DROP TABLE public.customers;

CREATE TABLE public.customers
(
  id serial NOT NULL ,
  createdon timestamp without time zone ,
  updated_date timestamp without time zone,
  access_token character varying(1000),
  access_token_expiry timestamp without time zone,
  access_token_updated timestamp without time zone,
  total_num_uploads integer,
  CONSTRAINT customers_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.customers
  OWNER TO postgres;

-- Table: public.booking

-- DROP TABLE public.booking;

CREATE TABLE public.booking
(
  id serial NOT NULL ,
  customer_id integer NOT NULL,
  customername character varying(50),
  email character varying(50),
  mobile character varying(50),
  address text,
  created_on timestamp without time zone,
  ipaddress character varying(50),
  otp character varying(25),
  city_id integer,
  lat character varying(50),
  lng character varying(50),
  status integer,
  internal_status integer DEFAULT 0,
  agent_comments text,
  agent_comments_date timestamp without time zone,
  campaign_id integer,
  updated_date timestamp without time zone,
  offercode character varying(1000) ,
  CONSTRAINT customer_data_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.booking
  OWNER TO postgres;

  -- DROP TABLE public.auth_rule;

CREATE TABLE public.auth_rule
(
  name character varying(64) NOT NULL,
  data bytea,
  created_at integer,
  updated_at integer,
  CONSTRAINT auth_rule_pkey PRIMARY KEY (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.auth_rule
  OWNER TO postgres;
  
-- Table: public.auth_item

-- DROP TABLE public.auth_item;

CREATE TABLE public.auth_item
(
  name character varying(64) NOT NULL,
  type smallint NOT NULL,
  description text,
  rule_name character varying(64),
  data bytea,
  created_at integer,
  updated_at integer,
  CONSTRAINT auth_item_pkey PRIMARY KEY (name),
  CONSTRAINT auth_item_rule_name_fkey FOREIGN KEY (rule_name)
      REFERENCES public.auth_rule (name) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE SET NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.auth_item
  OWNER TO postgres;


-- Index: public."idx-auth_item-type"

-- DROP INDEX public."idx-auth_item-type";

CREATE INDEX "idx-auth_item-type"
  ON public.auth_item
  USING btree
  (type);

-- Table: public.auth_item_child

-- DROP TABLE public.auth_item_child;

CREATE TABLE public.auth_item_child
(
  parent character varying(64) NOT NULL,
  child character varying(64) NOT NULL,
  CONSTRAINT auth_item_child_pkey PRIMARY KEY (parent, child),
  CONSTRAINT auth_item_child_child_fkey FOREIGN KEY (child)
      REFERENCES public.auth_item (name) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT auth_item_child_parent_fkey FOREIGN KEY (parent)
      REFERENCES public.auth_item (name) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.auth_item_child
  OWNER TO postgres;

-- Table: public.auth_rule




-- Table: public.auth_assignment

-- DROP TABLE public.auth_assignment;

CREATE TABLE public.auth_assignment
(
  item_name character varying(64) NOT NULL,
  user_id character varying(64) NOT NULL,
  created_at integer,
  CONSTRAINT auth_assignment_pkey PRIMARY KEY (item_name, user_id),
  CONSTRAINT auth_assignment_item_name_fkey FOREIGN KEY (item_name)
      REFERENCES public.auth_item (name) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.auth_assignment
  OWNER TO postgres;






-- Table: public."user"

-- DROP TABLE public."user";

CREATE TABLE public."user"
(
  id serial NOT NULL ,
  username character varying(200),
  auth_key character varying(255),
  access_token_expired_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  password_hash character varying(255),
  password_reset_token character varying(255),
  email character varying(255),
  unconfirmed_email character varying(255),
  confirmed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  registration_ip character varying(20),
  last_login_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  last_login_ip character varying(20),
  blocked_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  status integer DEFAULT 10,
  role integer,
  created_at timestamp(0) without time zone DEFAULT now(),
  updated_at timestamp(0) without time zone DEFAULT now(),
  campaign_id character varying(150),
  CONSTRAINT user_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."user"
  OWNER TO postgres;

-- Index: public."idx-user"

-- DROP INDEX public."idx-user";

CREATE INDEX "idx-user"
  ON public."user"
  USING btree
  (username COLLATE pg_catalog."default", auth_key COLLATE pg_catalog."default", password_hash COLLATE pg_catalog."default", status);


 -- Table: public.messages

-- DROP TABLE public.messages;

CREATE TABLE public.messages
(
  id serial NOT NULL ,
  createdon timestamp(0) without time zone NOT NULL,
  fromid character varying(50) DEFAULT NULL::character varying,
  toid character varying(50) NOT NULL,
  sub character varying(250) DEFAULT NULL::character varying,
  body character varying(1500) NOT NULL,
  msgtype smallint NOT NULL,
  senton timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  status smallint NOT NULL,
  campaign_id integer,
  CONSTRAINT messages_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.messages
  OWNER TO postgres;

-- Table: public.customer_assigned_users

-- DROP TABLE public.customer_assigned_users;

CREATE TABLE public.customer_assigned_users
(
  id serial NOT NULL ,
  booking_id integer,
  allocated_user_id integer,
  status integer DEFAULT 0,
  created_date timestamp without time zone,
  updated_date timestamp without time zone,
  CONSTRAINT booking_assigned_users_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.customer_assigned_users
  OWNER TO postgres;
  
-- Table: public.billsnap

-- DROP TABLE public.billsnap;

CREATE TABLE public.billsnap
(
  id serial NOT NULL ,
  billsnap_data json,
  billsnap_comments text,
  bill_image character varying(255),
  status smallint DEFAULT 0, -- 0=Pending, 1=Processed, 2=Rejected, 3=Duplicate Bill, 4=Manual , 5 = Inprogress
  created_date timestamp without time zone,
  updated_date timestamp without time zone,
  store_verify smallint NOT NULL DEFAULT '0'::smallint,
  store_id integer,
  booking_id integer,
  bill_raw_data text,
  CONSTRAINT billsnap_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.billsnap
  OWNER TO postgres;
COMMENT ON COLUMN public.billsnap.status IS '0=Pending, 1=Processed, 2=Rejected, 3=Duplicate Bill, 4=Manual , 5 = Inprogress';

-- Table: public.billsnap_data

-- DROP TABLE public.billsnap_data;

CREATE TABLE public.billsnap_data
(
  all_bill_data json
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.billsnap_data
  OWNER TO postgres;

-- Table: public.billsnap_valid_data

-- DROP TABLE public.billsnap_valid_data;

CREATE TABLE public.billsnap_valid_data
(
  id serial NOT NULL ,
  billsnap_id integer,
  totalamt numeric(10,2),
  totalqty integer,
  created_date timestamp without time zone,
  CONSTRAINT billsnap_valid_data_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.billsnap_valid_data
  OWNER TO postgres;

  
 -- Table: public.stores

-- DROP TABLE public.stores;

CREATE TABLE public.stores
(
  id serial NOT NULL  ,
  store_name character varying(150),
  store_code character varying(50),
  status integer DEFAULT 1,
  created_date timestamp without time zone,
  updated_date timestamp without time zone,
  CONSTRAINT stores_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.stores
  OWNER TO postgres;

-- Table: public.store_products

-- DROP TABLE public.store_products;

CREATE TABLE public.store_products
(
  id serial NOT NULL ,
  store_id integer,
  article_no character varying(50),
  product_name character varying(150),
  sku character varying(50),
  mrp_price numeric(10,2),
  status smallint,
  created_date timestamp without time zone,
  CONSTRAINT store_products_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.store_products
  OWNER TO postgres;
  
-- Table: public.logs

-- DROP TABLE public.logs;

CREATE TABLE public.logs
(
  id serial NOT NULL ,
  campaign_id integer,
  description character varying(1000),
  created_date timestamp without time zone,
  category character varying(25),
  CONSTRAINT logs_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.logs
  OWNER TO postgres;


