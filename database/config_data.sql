INSERT INTO "user" ("id", "username", "auth_key", "access_token_expired_at", "password_hash", "password_reset_token", "email", "unconfirmed_email", "confirmed_at", "registration_ip", "last_login_at", "last_login_ip", "blocked_at", "status", "role", "created_at", "updated_at", "campaign_id") VALUES (1, E'admin', E'dVN8fzR_KzJ_lBrymfXI6qyH2QzyXYUU', E'2018-03-11 19:57:44', E'$2y$13$9Gouh1ZbewVEh4bQIGsifOs8/RWW/7RIs0CAGNd7tapXFm9.WxiXS', NULL, E'admin@demo.com', E'admin@demo.com', E'2017-05-05 16:38:48', E'127.0.0.1', E'2018-03-10 19:57:44', E'127.0.0.1', NULL, 10, 99, E'2017-05-05 16:38:03', E'2018-03-10 19:57:43', E'1');
INSERT INTO "user" ("id", "username", "auth_key", "access_token_expired_at", "password_hash", "password_reset_token", "email", "unconfirmed_email", "confirmed_at", "registration_ip", "last_login_at", "last_login_ip", "blocked_at", "status", "role", "created_at", "updated_at", "campaign_id") VALUES (2, E'staff', E'Xm-zZRREtAIKsFlINVRLSw3U7llbx_5a', E'2018-04-07 14:34:48', E'$2y$13$TKh5pEy0RFTmkC9Kjvb9A.WR/I1QVzYHdfYDw0m7MnHnN0bsv96Jq', NULL, E'staff@demo.com', E'staff@demo.com', E'2017-05-15 09:20:53', E'127.0.0.1', E'2018-04-06 14:34:49', E'127.0.0.1', NULL, 10, 50, E'2017-05-15 09:19:02', E'2018-04-06 14:34:47', E'1');
INSERT INTO "user" ("id", "username", "auth_key", "access_token_expired_at", "password_hash", "password_reset_token", "email", "unconfirmed_email", "confirmed_at", "registration_ip", "last_login_at", "last_login_ip", "blocked_at", "status", "role", "created_at", "updated_at", "campaign_id") VALUES (3, E'user', E'rNXSqIas_43RdpG0e5_7d1W06iK8pXJ8', E'2017-06-04 00:13:02', E'$2y$13$nd/F3g6jjIa1/Sk6JZxZ5uVq0OpsbOmW1OdnbDG6BpFqgkFbQotjm', NULL, E'user@demo.com', E'user@demo.com', E'2017-06-03 00:12:16', E'127.0.0.1', E'2017-06-03 00:13:02', E'127.0.0.1', NULL, 10, 10, E'2017-05-21 23:31:53', E'2017-06-03 13:34:52', E'1');
INSERT INTO "user" ("id", "username", "auth_key", "access_token_expired_at", "password_hash", "password_reset_token", "email", "unconfirmed_email", "confirmed_at", "registration_ip", "last_login_at", "last_login_ip", "blocked_at", "status", "role", "created_at", "updated_at", "campaign_id") VALUES (4, E'superadmin', E'dVN8fzR_KzJ_lBrymfXI6qyH2QzyXYUU', E'2018-03-06 20:40:56', E'$2y$13$9Gouh1ZbewVEh4bQIGsifOs8/RWW/7RIs0CAGNd7tapXFm9.WxiXS', NULL, E'user@demo.com', E'user@demo.com', E'2017-06-03 00:12:16', E'127.0.0.1', E'2018-03-05 20:40:57', E'127.0.0.1', NULL, 10, 1, E'2017-05-21 23:31:53', E'2018-03-05 20:40:55', E'1');
INSERT INTO "user" ("id", "username", "auth_key", "access_token_expired_at", "password_hash", "password_reset_token", "email", "unconfirmed_email", "confirmed_at", "registration_ip", "last_login_at", "last_login_ip", "blocked_at", "status", "role", "created_at", "updated_at", "campaign_id") VALUES (5, E'siva', E'y7KArMKyjCOBOcIZVFnfNL9i7tZLHhJu', E'2018-01-18 11:49:16', E'$2y$13$ODxT9.N78ec0t.IFzcxgDuxLOmWb6WN7UbLnDZDG4ISTDeW17fveW', NULL, E'sivakumar@bigcity.in', E'sivakumar@bigcity.in', E'2017-06-03 00:12:16', E'127.0.0.1', E'2018-01-17 11:49:17', E'127.0.0.1', NULL, 10, 1, E'2018-01-08 17:39:56', E'2018-01-17 11:49:16', E'1');
INSERT INTO "user" ("id", "username", "auth_key", "access_token_expired_at", "password_hash", "password_reset_token", "email", "unconfirmed_email", "confirmed_at", "registration_ip", "last_login_at", "last_login_ip", "blocked_at", "status", "role", "created_at", "updated_at", "campaign_id") VALUES (7, E'agent1', E'kwNBQhLC6xmYBG5pEGs-H2GPTraAxrP0', E'2018-03-21 13:30:31', E'$2y$13$9Gouh1ZbewVEh4bQIGsifOs8/RWW/7RIs0CAGNd7tapXFm9.WxiXS', NULL, E'agent@bigcity.com', E'agent@bigcity.com', E'2018-01-10 15:41:16', E'127.0.0.1', E'2018-03-20 13:30:32', E'127.0.0.1', NULL, 10, 10, E'2018-01-10 15:41:15', E'2018-03-20 13:30:30', E'1');
INSERT INTO "user" ("id", "username", "auth_key", "access_token_expired_at", "password_hash", "password_reset_token", "email", "unconfirmed_email", "confirmed_at", "registration_ip", "last_login_at", "last_login_ip", "blocked_at", "status", "role", "created_at", "updated_at", "campaign_id") VALUES (8, E'agent2', E'7ayTVSSYQ5fn6ijZ0FYtko1f7cEmC3Cq', E'2018-04-06 17:27:17', E'$2y$13$9Gouh1ZbewVEh4bQIGsifOs8/RWW/7RIs0CAGNd7tapXFm9.WxiXS', NULL, E'agent2@bigcity.com', E'agent2@bigcity.com', E'2018-01-10 18:21:56', E'127.0.0.1', E'2018-04-05 17:27:18', E'127.0.0.1', NULL, 10, 10, E'2018-01-10 18:21:56', E'2018-04-05 17:27:17', E'1');
INSERT INTO "user" ("id", "username", "auth_key", "access_token_expired_at", "password_hash", "password_reset_token", "email", "unconfirmed_email", "confirmed_at", "registration_ip", "last_login_at", "last_login_ip", "blocked_at", "status", "role", "created_at", "updated_at", "campaign_id") VALUES (9, E'agent', E'2vjkpltsFXgpRZxU3qK-7yojZZ2AiQ78', E'2018-03-07 17:40:51', E'$2y$13$Fp4As./rJ.aI8MH2lF0lo.nNI/reJ87HHUKuhKbxLwkAbLo5RR69m', NULL, E'agentnew@bigcity.com', E'agentnew@bigcity.com', E'2018-01-17 11:52:59', E'127.0.0.1', E'2018-03-06 17:40:51', E'127.0.0.1', NULL, 10, 10, E'2018-01-17 11:52:59', E'2018-03-06 17:40:50', E'1');

INSERT INTO "auth_item" ("name", "type", "description", "rule_name", "data", "created_at", "updated_at") VALUES (E'manageSettings', 2, E'Manage settings', NULL, NULL, 1512630620, 1512630620);
INSERT INTO "auth_item" ("name", "type", "description", "rule_name", "data", "created_at", "updated_at") VALUES (E'manageStaffs', 2, E'Manage staffs', NULL, NULL, 1512630620, 1512630620);
INSERT INTO "auth_item" ("name", "type", "description", "rule_name", "data", "created_at", "updated_at") VALUES (E'manageUsers', 2, E'Manage users', NULL, NULL, 1512630620, 1512630620);
INSERT INTO "auth_item" ("name", "type", "description", "rule_name", "data", "created_at", "updated_at") VALUES (E'user', 1, E'User', NULL, NULL, 1512630620, 1512630620);
INSERT INTO "auth_item" ("name", "type", "description", "rule_name", "data", "created_at", "updated_at") VALUES (E'staff', 1, E'Staff', NULL, NULL, 1512630620, 1512630620);
INSERT INTO "auth_item" ("name", "type", "description", "rule_name", "data", "created_at", "updated_at") VALUES (E'admin', 1, E'Administrator', NULL, NULL, 1512630620, 1512630620);
INSERT INTO "auth_item" ("name", "type", "description", "rule_name", "data", "created_at", "updated_at") VALUES (E'superadmin', 1, E'Super Admin', NULL, NULL, NULL, NULL);
INSERT INTO "auth_item" ("name", "type", "description", "rule_name", "data", "created_at", "updated_at") VALUES (E'siva', 1, E'siva', NULL, NULL, NULL, NULL);
INSERT INTO "auth_item" ("name", "type", "description", "rule_name", "data", "created_at", "updated_at") VALUES (E'agent1', 1, E'agent1', NULL, NULL, NULL, NULL);
INSERT INTO "auth_item" ("name", "type", "description", "rule_name", "data", "created_at", "updated_at") VALUES (E'agent2', 1, E'agent2', NULL, NULL, NULL, NULL);
INSERT INTO "auth_item" ("name", "type", "description", "rule_name", "data", "created_at", "updated_at") VALUES (E'agent', 1, E'agent', NULL, NULL, NULL, NULL);


INSERT INTO "auth_assignment" ("item_name", "user_id", "created_at") VALUES (E'staff', E'2', 1512630620);
INSERT INTO "auth_assignment" ("item_name", "user_id", "created_at") VALUES (E'manageUsers', E'2', 1512630620);
INSERT INTO "auth_assignment" ("item_name", "user_id", "created_at") VALUES (E'manageStaffs', E'2', 1512630620);
INSERT INTO "auth_assignment" ("item_name", "user_id", "created_at") VALUES (E'manageSettings', E'2', 1512630620);
INSERT INTO "auth_assignment" ("item_name", "user_id", "created_at") VALUES (E'user', E'3', 1512630620);
INSERT INTO "auth_assignment" ("item_name", "user_id", "created_at") VALUES (E'superadmin', E'4', NULL);
INSERT INTO "auth_assignment" ("item_name", "user_id", "created_at") VALUES (E'superadmin', E'5', NULL);
INSERT INTO "auth_assignment" ("item_name", "user_id", "created_at") VALUES (E'user', E'7', 1515579076);
INSERT INTO "auth_assignment" ("item_name", "user_id", "created_at") VALUES (E'user', E'8', 1515588716);
INSERT INTO "auth_assignment" ("item_name", "user_id", "created_at") VALUES (E'user', E'9', 1516170180);
INSERT INTO "auth_assignment" ("item_name", "user_id", "created_at") VALUES (E'admin', E'1', 1518518315);

INSERT INTO "rewards" ("id", "prod_name", "value", "status", "created_at", "updated_at", "imgurl") VALUES (1, E'One month Gaana Subscription', 100, 1, NULL, NULL, NULL);

INSERT INTO "reward_messages" ("id", "campaign_id", "reward_id", "messages", "status", "created_date", "updated_date") VALUES (1, 1, 1, E'Dear {{customerName}} , Your One month Gaana Subscription code : {{offercode}}  . Regards , Team Bigcity', 1, NULL, NULL);

INSERT INTO "states" ("id", "statename") VALUES (1, E'Assam');
INSERT INTO "states" ("id", "statename") VALUES (2, E'Andhra Pradesh');
INSERT INTO "states" ("id", "statename") VALUES (3, E'Odisha');
INSERT INTO "states" ("id", "statename") VALUES (4, E'Punjab');
INSERT INTO "states" ("id", "statename") VALUES (5, E'Delhi');
INSERT INTO "states" ("id", "statename") VALUES (6, E'Gujarat');
INSERT INTO "states" ("id", "statename") VALUES (7, E'Karnataka');
INSERT INTO "states" ("id", "statename") VALUES (8, E'Haryana');
INSERT INTO "states" ("id", "statename") VALUES (9, E'Rajasthan');
INSERT INTO "states" ("id", "statename") VALUES (10, E'Himachal Pradesh');
INSERT INTO "states" ("id", "statename") VALUES (11, E'Uttarakhand');
INSERT INTO "states" ("id", "statename") VALUES (12, E'Jharkhand');
INSERT INTO "states" ("id", "statename") VALUES (13, E'Chhattisgarh');
INSERT INTO "states" ("id", "statename") VALUES (14, E'Kerala');
INSERT INTO "states" ("id", "statename") VALUES (15, E'Tamil Nadu');
INSERT INTO "states" ("id", "statename") VALUES (16, E'Madhya Pradesh');
INSERT INTO "states" ("id", "statename") VALUES (17, E'West Bengal');
INSERT INTO "states" ("id", "statename") VALUES (18, E'Bihar');
INSERT INTO "states" ("id", "statename") VALUES (19, E'Maharashtra');
INSERT INTO "states" ("id", "statename") VALUES (20, E'Uttar Pradesh');
INSERT INTO "states" ("id", "statename") VALUES (21, E'Chandigarh');
INSERT INTO "states" ("id", "statename") VALUES (22, E'Telangana');
INSERT INTO "states" ("id", "statename") VALUES (23, E'Jammu and Kashmir');
INSERT INTO "states" ("id", "statename") VALUES (24, E'Tripura');
INSERT INTO "states" ("id", "statename") VALUES (25, E'Meghalaya');
INSERT INTO "states" ("id", "statename") VALUES (26, E'Goa');
INSERT INTO "states" ("id", "statename") VALUES (27, E'Arunachal Pradesh');
INSERT INTO "states" ("id", "statename") VALUES (28, E'Manipur');
INSERT INTO "states" ("id", "statename") VALUES (29, E'Mizoram');
INSERT INTO "states" ("id", "statename") VALUES (30, E'Sikkim');
INSERT INTO "states" ("id", "statename") VALUES (31, E'Puducherry');
INSERT INTO "states" ("id", "statename") VALUES (32, E'Nagaland');
INSERT INTO "states" ("id", "statename") VALUES (33, E'Andaman and Nicobar Islands');
INSERT INTO "states" ("id", "statename") VALUES (34, E'Dadra and Nagar Haveli');
INSERT INTO "states" ("id", "statename") VALUES (35, E'Daman and Diu');
INSERT INTO "states" ("id", "statename") VALUES (36, E'Lakshadweep');

UPDATE "public"."campaigns" SET "regtext"=E'Dear Customer, your OTP for Budweiser Sensation Promotion is {{otp}}',postregmsgvalid = 'Thank you for your registration. You will receive your Gaana Code in 48 business hours, after successful verification for your details.',postregmsginvalid='Sorry, the bill uploaded by you is not valid. Please upload a valid bill.' WHERE  "id"=1;

UPDATE "public"."reward_messages" SET "messages"=E'Dear {{customerName}}, your 1 month Gaana subscription code is {{offercode}}. For Android: Open Gaana App, navigate to your account settings & click on Redeem Coupon. Enter the offer code & click Proceed to claim your subscription. For iOS Users please visit (http://gaana.com/settings/redeem). Enter the offer code & click Submit to claim your subscription. If you are a winner in the lucky draw, we will contact you. For T&C\'s please visit budbasement.in' WHERE  "id"=1;

UPDATE "public"."campaigns" SET "max_num_offercodes"=E'3', "booking_reject_msg"=E'Dear {{customerName}}, the bill uploaded by you for the Budweiser Sensation Promotion is rejected due to poor image quality / product not mentioned on the bill / invalid bill. Please reupload a valid bill on budbasement.in', "booking_excess_msg"=E'Dear {{customerName}}, you have exceeded the number of limit for assured reward. However, you stand a chance to win exciting prize. If you are a winner, we will contact you. For T&Cs, please visit budbasement.in' WHERE  "id"=1;

UPDATE "public"."campaigns" SET "postregmsgvalid"=E'Thank you for your registration. You will receive your Gaana Code in 48 business hours, after successful verification of your details.' WHERE  "id"=1;
