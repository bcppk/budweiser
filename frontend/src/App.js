import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Loadable from 'react-loadable';
import Auth from './utils/Auth';
import Loader from './components/Loader';

class App extends Component {
    constructor(props) {
        super(props);

        this._auth = new Auth();
    }

    render() {
      const AuthenticatedRoute = ({component: Component, ...rest}) => (
          <Route {...rest} render={props => (
              this._auth.loggedIn() ? (
                  <Component {...props}/>
              ) : (
                  <Redirect to={{
                    pathname: '/',
                    state: {from: props.location}
                  }}/>
              )
          )}/>
      );

      const Homepage = Loadable({
        loader: () => import('./components/Homepage'),
        loading: Loader,
      });

      const Verify = Loadable({
        loader: () => import('./components/Verify'),
        loading: Loader,
      });

      const Bill = Loadable({
        loader: () => import('./components/Bill'),
        loading: Loader,
      });

      const Success = Loadable({
        loader: () => import('./components/Success'),
        loading: Loader,
      });

      const Terms = Loadable({
        loader: () => import('./components/Pages/Terms'),
        loading: Loader,
      });

      const Privacy = Loadable({
        loader: () => import('./components/Pages/Privacy'),
        loading: Loader,
      });

        return (
            <Router>
                <Switch>
                    <Route exact path="/" component={Homepage}/>
                    <Route path="/verify" component={Verify}/>
                    <Route path="/terms" component={Terms}/>
                    <Route path="/privacy" component={Privacy}/>
                    <AuthenticatedRoute path="/camera" component={Bill}/>
                    <AuthenticatedRoute path="/success" component={Success}/>
                </Switch>
            </Router>
        );
    }
}

export default App;
