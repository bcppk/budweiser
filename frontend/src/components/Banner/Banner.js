import React, { Component } from 'react';
import './Banner.css';
import bannerImg from './banner.jpeg';

class Banner extends Component {
  render() {
    return(
      <div className="wrap">
        <div>
          <img src={bannerImg} alt="banner" width="100%" />
        </div>
      </div>
    );
  }
}

export default Banner;