import React, { Component } from 'react';
import ReactCrop from 'react-image-crop';
import imageCompression from 'browser-image-compression';
import 'react-image-crop/dist/ReactCrop.css';
import Auth from '../../utils/Auth';
import './Bill.css';
//import Header from '../Header';
import Footer from '../Footer';
import Banner from '../Homepage/Banner.jpg';

/*import Camera, { FACING_MODES, IMAGE_TYPES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';*/

class Bill extends Component {
  constructor(props) {
    super(props);

    this._auth = new Auth();

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCrop = this.handleCrop.bind(this);
    this.handleTakePic = this.handleTakePic.bind(this);
    this.handleCamera = this.handleCamera.bind(this);
    this.handleUpload = this.handleUpload.bind(this);

    this.state = {
      initCam: true,
      capturedImg: null,
      croppedImage: null,
      crop: {
        x: 10,
        y: 10,
        width: 80,
        height: 80,
      },
      errorMsg: '',
      successMsg: '',
      isSubmitted: false,
      showSubmit: false,
      src: '',
      err: null,
      imgPreview: false,
      processCrop: false,
      photoTaken: false
    };
  }

  componentWillUnmount() {
    this.setState({
      errorMsg: '',
      successMsg: ''
    });
  }

  handleCamera() {
    this.setState({photoTaken: true});
    setTimeout(() => {
      document.getElementById('file').click();
    }, 100)
  }

  handleUpload() {
    const file = this.fileUpload.files[0];
    let reader  = new FileReader();
    const that = this;

    reader.addEventListener("load", function () {
      that.rotateBase64Image90Degree(reader.result, function(resetBase64) {
        that.setState({
          initCam: false,
          capturedImg: resetBase64,
          croppedImage: null
        });
      });
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }

  rotateBase64Image90Degree(base64data, callback) {
    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext("2d");
    let image = new Image();

    image.src = base64data;
    image.onload = function() {
      if (image.naturalWidth > image.naturalHeight) {
        canvas.width = image.height;
        canvas.height = image.width;
        ctx.rotate(90 * Math.PI / 180);
        ctx.translate(0, -canvas.width);
        ctx.drawImage(image, 0, 0);
        callback(canvas.toDataURL());
      } else {
        callback(base64data);
      }
    };
  }

  onTakePhoto (dataUri) {
    // Do stuff with the dataUri photo...
    this.turnOffCamera();

    setTimeout(() => {
      this.setState({
        initCam: false,
        capturedImg: dataUri,
        croppedImage: null
      });
    }, 1000)
  }

  turnOffCamera() {
    let stream = document.querySelector('video').srcObject;
    let tracks = stream.getTracks();

    tracks.forEach(function(track) {
      track.stop();
    });

    document.querySelector('video').srcObject = null;
  }

  onCameraError (error) {
    console.error('onCameraError', error);
  }

  onCameraStart (stream) {
    console.log('onCameraStart');
  }

  onCameraStop () {
    console.log('onCameraStop');
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({
      isSubmitted: true
    });

    const that = this;

    imageCompression.getFilefromDataUrl(this.state.croppedImage, 'budweiserbill')
        .then(croppedBill => {
          const maxSizeMB = 1;
          const maxWidthOrHeight = 950; // compressedFile will scale down by ratio to a point that width or height is smaller than maxWidthOrHeight
          imageCompression(croppedBill, maxSizeMB, maxWidthOrHeight) // maxSizeMB, maxWidthOrHeight are optional
              .then(function (compressedFile) {
                imageCompression.getDataUrlFromFile(compressedFile)
                    .then(base64Image => {
                      that._auth.submitBill(base64Image)
                          .then(res => {
                            if (res.data.code === 104) {
                              that.props.history.replace('/success');
                            } else {
                              that.setState({errorMsg: res.data.message, isSubmitted: false});
                            }
                          })
                          .catch(err => {
                            that.setState({
                              errorMsg: 'Server error! Please try again.',
                              isSubmitted: false
                            });
                          })
                    });
              })
              .catch(function (error) {
                console.log(error.message);
              });
        });
  }

  onCropComplete = (crop, pixelCrop) => {
    console.log('onCropComplete', pixelCrop);
  };

  onCropChange = crop => {
    this.setState({ crop })
  };

  handleCrop = () => {
    this.setState({processCrop: true});

    setTimeout(() => {
      let { crop } = this.state;
      const croppedImg = this.getCroppedImg(this.refImageCrop, crop);
      this.setState({ croppedImage: croppedImg, showSubmit: true, imgPreview: true, processCrop: false });
    }, 100);
  };

  getCroppedImg = (srcImage, pixelCrop) => {
    let img = new Image();
    img.src = this.state.capturedImg;
    const targetX = srcImage.width * pixelCrop.x / 100;
    const targetY = srcImage.height * pixelCrop.y / 100;
    const targetWidth = srcImage.width * pixelCrop.width / 100;
    const targetHeight = srcImage.height * pixelCrop.height / 100;

    const canvas = document.createElement('canvas');
    canvas.width = targetWidth;
    canvas.height = targetHeight;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
        img,
        targetX,
        targetY,
        targetWidth,
        targetHeight,
        0,
        0,
        targetWidth,
        targetHeight
    );

    return canvas.toDataURL('image/png');
  };

  handleTakePic = () => {
    this.setState({imgPreview: false, initCam: true, errorMsg: '', isSubmitted: false, photoTaken: false});
  };

  render() {
    /*const WebCam = () => {
      return(
          <Camera
              onTakePhoto = { (dataUri) => { this.onTakePhoto(dataUri); } }
              onCameraError = { (error) => { this.onCameraError(error); } }
              idealFacingMode = {FACING_MODES.ENVIRONMENT}
              idealResolution = {{width: 640, height: 480}}
              imageType = {IMAGE_TYPES.PNG}
              imageCompression = {0.97}
              isMaxResolution = {false}
              isImageMirror = {false}
              isDisplayStartCameraError = {true}
              sizeFactor = {1}
              onCameraStart = { (stream) => { this.onCameraStart(stream); } }
              onCameraStop = { () => { this.onCameraStop(); } }
          />
      );
    };*/

    const WebCam = () => {
      if(!this.state.photoTaken) {
        return(
            <div>
              <div className="wrap">
                <img src={Banner} className="banner" alt="Banner" />
                {/*<Header/>*/}
                <div className="container">
                  <div className="site-signup">
                    <div className="col-lg-5 bs-component">
                      <h1>Please Ensure</h1>
                      <ul className="fa-ul">
                        <li><i className="fa fa-check"></i> You are able to clearly read the contents of the bill in the image.</li>
                        <li><i className="fa fa-check"></i> Date and invoice/bill number is visible in the image.</li>
                        <li><i className="fa fa-check"></i> Bill image has all 4 corners visible.</li>
                        <li><i className="fa fa-check"></i> You capture the entire bill in the image.</li>
                      </ul>
                      <p className="text-center">Submission of tempered or unclear bills will result in a delay or cancelation of your reward claim.</p>
                      <div className="take-photo">
                        <button type="button" className="btn btn-primary btn-block" onClick={this.handleCamera}>Take picture</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <Footer />
            </div>
        );
      } else {
        return(
            <div>
              <div className="wrap">
                <img src={Banner} className="banner" alt="Banner" />
                {/*<Header/>*/}
                <div className="container">
                  <div className="site-signup">
                    <div className="col-lg-5 bs-component">
                      <div className="billloadingDiv">
                        <div className="billloader">Loading...</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <input type="file" id="file" accept="image/*" capture="environment" className="hide" ref={(ref) => this.fileUpload = ref} onChange={this.handleUpload} />
              <Footer />
            </div>
        );
      }
    };

    if (this.state.initCam) {
      return (
          <WebCam />
      );
    } else {
      return (
          <div>
            <div className="wrap">
              <img src={Banner} className="banner" alt="Banner" />
              {/*<Header/>*/}
              <div className="container">
                <div className="site-signup">
                  {!this.state.imgPreview &&
                    <div className="col-md-12 bs-component">
                      <h1>Crop Bill</h1>
                      <div className="form-group">
                        <ReactCrop
                            src={this.state.capturedImg}
                            crop={this.state.crop}
                            keepSelection={true}
                            onComplete={this.onCropComplete}
                            onChange={this.onCropChange}
                        />
                      </div>
                      <div id="form-error" className="form-group">{this.state.errorMsg}</div>
                      <div className="form-group">
                        <button type="button" className="btn btn-block btn-primary" disabled={this.state.processCrop} onClick={this.handleCrop}>
                          {this.state.processCrop &&
                          <i className="fa fa-circle-o-notch fa-spin"></i>}
                          {!this.state.processCrop &&
                          <span>Crop Bill</span>}
                        </button>
                      </div>
                    </div>
                  }

                  {this.state.imgPreview &&
                    <div className="col-md-12 bs-component">
                      <h1>Confirm Bill</h1>
                      <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                          <img id="finalImg" src={this.state.croppedImage} className="preview-bill" alt="" ref={(ref) => this.finalImg = ref}/>
                        </div>
                        <div className="form-group">
                          <p className="text-center">
                            Make sure you have the complete bill in the picture. <br />
                            Are you sure, you want to submit this?
                          </p>
                        </div>
                        <p className="text-center error-text">{this.state.errorMsg}</p>
                        <div className="form-group text-center">
                          <button type="submit" className="btn btn-primary btn-block" disabled={this.state.isSubmitted}>
                            {this.state.isSubmitted &&
                            <i className="fa fa-circle-o-notch fa-spin"></i>}
                              {!this.state.isSubmitted &&
                              <span>Submit</span>}
                              </button>

                        </div>
                        <div className="form-group text-center">
                          <button type="button" className="btn btn-default btn-block" onClick={this.handleTakePic} disabled={this.state.isSubmitted}>Back to camera</button>
                        </div>
                      </form>
                    </div>
                  }

                  <img src={this.state.capturedImg} style={{display: "none"}} ref={(img) => {
                    this.refImageCrop = img
                  }} alt=""/>
                </div>
              </div>
            </div>
            <Footer />
          </div>
      );
    }
  }
}

export default Bill;