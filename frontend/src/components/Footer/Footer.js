import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './Footer.css';

class Footer extends Component {
  render() {
    return(
        <footer className="footer">
          <div className="container">
            <p className="pull-left"><Link to="/terms">Terms & Conditions</Link></p>
            <p className="pull-right"><Link to="/privacy">Privacy Policy</Link></p>
          </div>
          <div className="container">
            <p className="pull-left">&copy; BigCity Promotions 2018,</p>
            <p className="pull-left">All Rights Reserved</p>
          </div>
        </footer>
    );
  }
}

export default Footer;