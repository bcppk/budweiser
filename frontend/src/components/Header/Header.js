import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './Header.css';
import Logo from './Logo.jpg';

class Header extends Component {
  render() {
    return(
        <nav className="navbar-default navbar-fixed-top navbar">
          <div className="container">
            <div className="navbar-header">
              {/*<button type="button" className="navbar-toggle">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
              </button>*/}
              <Link className="navbar-brand" to="/"><img src={Logo} className="logo" alt="Logo" /></Link>
            </div>
            {/*<div className="collapse navbar-collapse">
              <ul className="navbar-nav navbar-right nav">
                <li><Link to="/">Home</Link></li>
              </ul>
            </div>*/}
          </div>
        </nav>
    );
  }
}

export default Header;