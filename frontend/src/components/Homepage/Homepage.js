import React, { Component } from 'react';
import Auth from '../../utils/Auth';
import {Link} from 'react-router-dom';
import './Homepage.css';
//import Header from '../Header';
import Footer from '../Footer';
import Banner from './Banner.jpg';

class Homepage extends Component {
  constructor(props) {
    super(props);

    this._auth = new Auth();

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.state = {
      errors: {},
      errorMsg: '',
      isSubmitted: false,
      /*provinces: [
        { label: 'Andaman and Nicobar Islands', value: 'Andaman and Nicobar Islands' },
        { label: 'Andhra Pradesh', value: 'Andhra Pradesh' },
        { label: 'Arunachal Pradesh', value: 'Arunachal Pradesh' },
        { label: 'Assam', value: 'Assam' },
        { label: 'Bihar', value: 'Bihar' },
        { label: 'Chandigarh', value: 'Chandigarh' },
        { label: 'Chattisgarh', value: 'Chattisgarh' },
        { label: 'Dadra and Nagar Haveli', value: 'Dadra and Nagar Haveli' },
        { label: 'Daman and Diu', value: 'Daman and Diu' },
        { label: 'Delhi', value: 'Delhi' },
        { label: 'Goa', value: 'Goa' },
        { label: 'Gujarat', value: 'Gujarat' },
        { label: 'Haryana', value: 'Haryana' },
        { label: 'Himachal Pradesh', value: 'Himachal Pradesh' },
        { label: 'Jammu and Kashmir', value: 'Jammu and Kashmir' },
        { label: 'Jharkhand', value: 'Jharkhand' },
        { label: 'Karnataka', value: 'Karnataka' },
        { label: 'Kerala', value: 'Kerala' },
        { label: 'Lakshadweep', value: 'Lakshadweep' },
        { label: 'Madhya Pradesh', value: 'Madhya Pradesh' },
        { label: 'Maharashtra', value: 'Maharashtra' },
        { label: 'Manipur', value: 'Manipur' },
        { label: 'Meghalaya', value: 'Meghalaya' },
        { label: 'Mizoram', value: 'Mizoram' },
        { label: 'Nagaland', value: 'Nagaland' },
        { label: 'Odisha', value: 'Odisha' },
        { label: 'Puducherry', value: 'Puducherry' },
        { label: 'Punjab', value: 'Punjab' },
        { label: 'Rajasthan', value: 'Rajasthan' },
        { label: 'Sikkim', value: 'Sikkim' },
        { label: 'Tamil Nadu', value: 'Tamil Nadu' },
        { label: 'Telangana', value: 'Telangana' },
        { label: 'Tripura', value: 'Tripura' },
        { label: 'Uttar Pradesh', value: 'Uttar Pradesh' },
        { label: 'Uttarakhand', value: 'Uttarakhand' },
        { label: 'West Bengal', value: 'West Bengal' }
      ]*/
      provinces: [],
      isTn: false,
      isMobile: false
    };
  }

  componentWillMount() {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      this.setState({isMobile: true});
    }
  }

  componentDidMount() {
    this._auth.getStateList()
        .then(res => {
          this.setState({
            provinces: res.data
          });
        })
        .catch(err => {
          this.setState({
            errorMsg: 'Problem in loading state list'
          });
        })
  }

  handleValidation() {
    let errors = {};
    let formIsValid = true;

    if(!this.state.customername) {
      formIsValid = false;
      errors["customername"] = "Please enter your name.";
    }

    if(!this.state.email) {
      formIsValid = false;
      errors["email"] = "Please enter your email.";
    }

    if(typeof this.state.email !== "undefined") {
      let lastAtPos = this.state.email.lastIndexOf('@');
      let lastDotPos = this.state.email.lastIndexOf('.');

      if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state.email.indexOf('@@') === -1 && lastDotPos > 2 && (this.state.email.length - lastDotPos) > 2)) {
        formIsValid = false;
        errors["email"] = "Please enter valid email ID.";
      }
    }

    if(!this.state.mobile) {
      formIsValid = false;
      errors["mobile"] = "Please enter 10 digit mobile number.";
    }

    if(typeof this.state.mobile !== "undefined") {
      if(!this.state.mobile.match(/^[0-9]+$/)) {
        formIsValid = false;
        errors["mobile"] = "Please enter valid mobile number";
      }
    }

    if(typeof this.state.mobile !== "undefined") {
      if(this.state.mobile.length !== 10) {
        formIsValid = false;
        errors["mobile"] = "Please enter 10 digit mobile number.";
      }
    }

    if(!this.state.agreeterms) {
      formIsValid = false;
      errors["agreeterms"] = "You must agree with terms & conditions.";
    }

    if(!this.state.agreeage) {
      formIsValid = false;
      errors["agreeage"] = "Please confirm your age.";
    }

    if(!this.state.userState) {
      formIsValid = false;
      errors["userState"] = "Please select state.";
    }

    if(this.state.isTn) {
      if(!this.state.tnQues) {
        formIsValid = false;
        errors["tnQues"] = "Please answer the above question to proceed.";
      }
    }

    this.setState({errors: errors});
    return formIsValid;
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });

    setTimeout(() => {
      if(parseInt(this.state.userState) === 15) {
        this.setState({isTn: true});
      } else {
        this.setState({isTn: false});
      }
    }, 100);
  }

  handleSubmit(e) {
    e.preventDefault();

    if(this.handleValidation()) {
      this.setState({
        isSubmitted: true
      });

      let tnAns = '';
      if(this.state.tnQues) {
        tnAns = this.state.tnQues;
      }

      this._auth.customerDetails(this.state.customername, this.state.email, this.state.mobile, this.state.userState, 2, tnAns)
          .then(res => {
            localStorage.setItem('usrmob', res.data.mobile);
            this.props.history.replace('/verify');
          })
          .catch(err => {
            this.setState({
              errorMsg: JSON.parse(err.message),
              isSubmitted: false
            });
          })
    }
  }

  render() {
    if (this.state.isMobile) {
      return (
          <div>
            <div className="wrap">
              <img src={Banner} className="banner" alt="Banner"/>
              {/*<Header/>*/}
              <div className="container">
                <div className="site-signup">
                  <div className="col-lg-5 bs-component">
                    <ol className="reg-steps">
                      <li>Buy Budweiser</li>
                      <li>Log on to www.budbasement.in</li>
                      <li>Upload the picture of your restaurant/pub bill for a chance to win exciting merchandize and tickets for a sensational experience in
                        Hyderabad
                      </li>
                    </ol>
                    <h1>Register Here</h1>
                    <form onSubmit={this.handleSubmit}>
                      <div className="form-group required">
                        <input type="text" id="customername" className="form-control" name="customername" aria-required="true" placeholder="Full Name"
                               onChange={this.handleChange}/>
                        <span className="error-text">{this.state.errors["customername"]}</span>
                      </div>
                      <div className="form-group required">
                        <input type="text" id="email" className="form-control" name="email" aria-required="true" placeholder="Email Address"
                               onChange={this.handleChange}/>
                        <span className="error-text">{this.state.errors["email"]}</span>
                      </div>
                      <div className="form-group required">
                        <input type="text" id="mobile" className="form-control" name="mobile" aria-required="true" placeholder="Mobile Number"
                               onChange={this.handleChange}/>
                        <span className="error-text">{this.state.errors["mobile"]}</span>
                      </div>
                      <div className="form-group required">
                        <select id="userState" className="form-control" name="userState" aria-required="true" onChange={this.handleChange}>
                          <option value="">Select State</option>
                          {this.state.provinces.map(province => <option key={province.id} value={province.id}>{province.statename}</option>)}
                        </select>
                        <span className="error-text">{this.state.errors["userState"]}</span>
                      </div>
                      {this.state.isTn &&
                      <div className="form-group required">
                        <textarea id="tnQues" className="form-control tn-input" name="tnQues" aria-required="true"
                                  placeholder="Budweiser gives me a Sensational experience because?" onChange={this.handleChange}></textarea>
                        <span className="error-text">{this.state.errors["tnQues"]}</span>
                      </div>}
                      <div className="form-group required">
                        <div className="checkbox">
                          <label>
                            <input type="checkbox" id="agreeterms" name="agreeterms" value="1" className="agreeterms" onChange={this.handleChange}/>
                            I agree to the <Link to="/terms" target="_blank">terms and conditions</Link>
                          </label>
                        </div>
                        <span className="error-text">{this.state.errors["agreeterms"]}</span>
                      </div>
                      <div className="form-group required">
                        <div className="checkbox">
                          <label>
                            <input type="checkbox" id="agreeage" name="agreeage" value="1" className="agreeterms" onChange={this.handleChange}/>
                            I confirm, I am above legal drinking age in my state.
                          </label>
                        </div>
                        <span className="error-text">{this.state.errors["agreeage"]}</span>
                      </div>
                      <div id="form-error" className="error-text">{this.state.errorMsg}</div>
                      <div className="form-group">
                        <button type="submit" className="btn btn-block btn-primary" disabled={this.state.isSubmitted}>
                          {this.state.isSubmitted &&
                          <i className="fa fa-circle-o-notch fa-spin"></i>}
                          {!this.state.isSubmitted &&
                          <span>Submit</span>}
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <Footer/>
          </div>
      );
    } else {
      return(
          <div>
            <div className="wrap">
              <img src={Banner} className="banner" alt="Banner"/>
              {/*<Header/>*/}
              <div className="container">
                <div className="site-signup">
                  <div className="col-lg-5 bs-component">
                    <p className="text-center">This website is optimized for mobile browsers only. Pls use this website from any major mobile device *</p>
                    <div className="mobcond">*- Works across all popular and select Mobile Devices.</div>
                  </div>
                </div>
              </div>
            </div>
            <Footer/>
          </div>
      );
    }
  }
}

export default Homepage;