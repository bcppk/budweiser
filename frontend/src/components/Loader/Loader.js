import React from 'react';
import './Loader.css';
import loadingImg from './loader.gif';

const Loader = ({isLoading, error}) => {
  // Handle the loading state
  if (isLoading) {
    return(
        <div>
          <img src={loadingImg} height="31" width="31" alt="Please Wait..." />
          <br /> Loading, please Wait...
        </div>
    );
  }
  // Handle the error state
  else if (error) {
    return <div>Sorry, there was a problem loading the page.</div>;
  }
  else {
    return null;
  }
};

export default Loader;
