import React, { Component } from 'react';
//import Header from '../Header';
import Footer from '../Footer';
import Banner from '../Homepage/Banner.jpg';

class Success extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    this.props.history.replace('/');
  }

  render() {
    return (
        <div>
          <div className="wrap">
            <img src={Banner} className="banner" alt="Banner" />
            {/*<Header/>*/}
            <div className="container">
              <div className="site-signup">
                <h1>Privacy Policy</h1>
                <div className="col-lg-5 bs-component">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae gravida erat. Curabitur enim metus, semper ut vehicula nec, luctus eget tortor. Suspendisse sit amet bibendum odio, id vestibulum tellus. Morbi ut diam a lectus molestie elementum in nec lectus. Nullam diam tortor, hendrerit a pulvinar sit amet, auctor id velit. Cras porta nisi vel semper volutpat. Cras aliquam massa arcu, at imperdiet ipsum tempor imperdiet. Etiam at porta erat. Curabitur erat erat, ullamcorper non risus ac, malesuada dignissim sem. </p>
                  <p>Suspendisse convallis ultricies dignissim. Aliquam porta aliquet vehicula. Maecenas ipsum elit, aliquam ac metus ac, gravida fermentum dui. Aliquam gravida, tellus ac ornare bibendum, nulla sem faucibus leo, vitae aliquet tortor ante ac nunc. Aliquam sit amet metus tempus, laoreet libero auctor, egestas elit. Sed nunc diam, bibendum eget magna interdum, lobortis consectetur mauris. Sed tincidunt, mauris tincidunt pharetra efficitur, mi enim tempus ex, at aliquet velit nunc volutpat lorem. Fusce ut sodales justo. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse porta lectus sed sagittis pulvinar. Donec et dui vel neque consequat facilisis at ac felis. Sed tempor sagittis mauris ut fringilla. Fusce facilisis mollis est, et volutpat orci eleifend et. Sed porttitor mattis augue vitae varius. Nulla mollis vel mauris ac feugiat. Nulla tristique elit sem, vel tristique ipsum consequat sed. </p>
                  <div className="form-group required">
                    <button type="button" className="btn btn-primary btn-block" onClick={this.handleClick}>Go back home</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Footer />
        </div>
    );
  }
}

export default Success;