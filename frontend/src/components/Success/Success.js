import React, { Component } from 'react';
import Auth from '../../utils/Auth';
import './Success.css';
//import Header from '../Header';
import Footer from '../Footer';
import Banner from '../Homepage/Banner.jpg';

class Success extends Component {
  constructor(props) {
    super(props);

    this._auth = new Auth();

    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    this._auth.logout();
  }

  handleClick(e) {
    this.props.history.replace('/');
  }

  render() {
    return (
        <div>
          <div className="wrap">
            <img src={Banner} className="banner" alt="Banner" />
            {/*<Header/>*/}
            <div className="container">
              <div className="site-signup">
                <h1 className="text-center">Upload <br /> Successful!</h1>
                <div className="col-lg-5 bs-component">
                  <p className="text-center success-msg">Your bill is uploaded successfully. <br /> We will be get back to you in 48 hrs.</p>
                  <div className="form-group required">
                    <button type="button" className="btn btn-primary btn-block" onClick={this.handleClick}>Go back home</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Footer />
        </div>
    );
  }
}

export default Success;