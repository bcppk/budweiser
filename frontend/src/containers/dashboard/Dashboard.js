import React, { Component } from 'react';
import Auth from '../../utils/Auth';

class Dashboard extends Component {
    constructor(props) {
        super(props);

        this._auth = new Auth();
    }

    componentDidMount() {
        this._auth.logout();
    }

    render() {
        return (
                <div>
                    <p>This is dashboard page.</p>
                </div>
        );
    }
}

export default Dashboard;