import React, { Component } from 'react';
import Auth from '../../utils/Auth';
import './Login.css';

class Login extends Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this._auth = new Auth();

        this.state = {
            errorMsg: ''
        };
    }

    componentWillMount() {
        if (this._auth.loggedIn()) {
            this.props.history.replace('/dashboard');
        }
    }

    componentWillUnmount() {
        document.body.classList.remove('login-page');
        document.body.classList.add('sidebar-mini');
    }

    render() {
        return (
            <div className="login-box">
                <div className="login-logo">
                    <a href="/"><b>BigCity</b>Promotions</a>
                </div>
                <div className="login-box-body">
                    <p className="login-box-msg">Sign in to start your session</p>
                    <form onSubmit={this.handleSubmit}>
                        <p className="help-block error-text text-center">{this.state.errorMsg}</p>
                        <div className="form-group has-feedback">
                            <div className="form-group field-loginform-email required">
                                <input className="form-control" type="text" name="username" placeholder="Username" onChange={this.handleChange} />
                            </div>
                            <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>

                        <div className="form-group has-feedback">
                            <div className="form-group field-loginform-password required">
                                <input className="form-control" type="password" name="password" placeholder="Password" onChange={this.handleChange} />
                            </div>
                            <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>

                        <div className="row">
                            <div className="col-xs-8">
                                <div className="checkbox">
                                    <label>
                                        <input type="hidden" name="rememberMe" value="0" />
                                        <input type="checkbox" id="rememberme" name="rememberMe" value="1" defaultChecked /> Remember Me
                                    </label>
                                </div>
                            </div>

                            <div className="col-xs-4 pull-right">
                                <button type="submit" className="btn btn-primary btn-block btn-flat">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        this._auth.login(this.state.username, this.state.password)
                .then(res => {
                    this.props.history.replace('/dashboard');
                })
                .catch(err => {
                    this.setState({
                        errorMsg: err.message
                    });
                })
    }
}

export default Login;