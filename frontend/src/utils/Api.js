import axios from 'axios';
import Auth from './Auth';

export default class Api {
    constructor() {
        this._auth = new Auth();
    }

    save(url, data) {
        const headers = this.getHeaders();
        return axios.post(url, data, headers)
                .then(res => {
                    return res;
                })
                .catch(this.handleError)
    }

    findAll(url) {
        const headers = this.getHeaders();
        return axios.get(url, headers)
                .then(res => {
                    return res;
                })
                .catch(this.handleError)
    }

    findById(url, id) {
        const headers = this.getHeaders();
        if (!id) {
            throw new Error("Couldn't load resource, ID is missing.");
        }
        return axios.get(`${url}/${id}`, headers)
                .then(res => {
                    return res;
                })
                .catch(this.handleError)
    }

    updateById(url, id, data) {
        const headers = this.getHeaders();
        if (!id) {
            throw new Error("Couldn't update resource, ID is missing.");
        }
        return axios.put(`${url}/${id}`, data, headers)
                .then(res => {
                    return res;
                })
                .catch(this.handleError)
    }

    deleteById(url, id) {
        const headers = this.getHeaders();
        if (!id) {
            throw new Error("Couldn't delete resource, ID is missing.");
        }
        return axios.delete(`${url}/${id}`, headers)
                .then(res => {
                    return res;
                })
                .catch(this.handleError)
    }

    getHeaders() {
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        if (this._auth.loggedIn()) {
            headers['Authorization'] = 'Bearer ' + this._auth.getToken();
        }

        return headers;
    }

    handleError(error) {
        let errorMessage = {};
        // Connection error
        if(error.status === 0) {
            errorMessage = {
                success: false,
                status: 0,
                data: "Sorry, there was a connection error occurred. Please try again."
            };
        }
        else {
            errorMessage = error.json();
        }
        throw new Error(errorMessage);
    }
}