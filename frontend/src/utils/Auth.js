import { fetch } from 'whatwg-fetch';
import decode from "jwt-decode";
import config from "../config";

export default class Auth {
    constructor() {
        this.fetch = this.fetch.bind(this);
        this.login = this.login.bind(this);
        this.getProfile = this.getProfile.bind(this);
    }

    login(username, password) {
        return this.fetch(`${config.ApiUrl}/staff/login`, {
            method: 'POST',
            body: JSON.stringify({
                "LoginForm": {
                    "username": username,
                    "password": password
                }
            })
        }).then(res => {
            this.setToken(res.data.access_token);
            return Promise.resolve(res);
        })
    }

  customerDetails(name, email, mobile, state, city, tn_answer) {
    return this.fetch(`${config.ApiUrl}/user/registration`, {
      method: 'POST',
      body: JSON.stringify({
        "campaign_id": 1,
        "customername": name,
        "email": email,
        "mobile": mobile,
        "state_id": state,
        "city_id": city,
        "tn_answer": tn_answer
      })
    }).then(res => {
      return Promise.resolve(res);
    })
  }

  validateOtp(mobile, otp) {
    return this.fetch(`${config.ApiUrl}/user/validateotp`, {
      method: 'POST',
      body: JSON.stringify({
        "campaign_id": 1,
        "mobile": mobile,
        "otp": otp
      })
    }).then(res => {
      this.setToken(res.data.access_token);
      return Promise.resolve(res);
    })
  }

  submitBill(bill) {
    return this.fetch(`${config.ApiUrl}/user/uploadbill`, {
      method: 'POST',
      body: JSON.stringify({
        "campaign_id": 1,
        "bill": bill
      })
    }).then(res => {
      return Promise.resolve(res);
    })
  }

  getStateList() {
    return this.fetch(`${config.ApiUrl}/user/getstatelist`, {
      method: 'GET'
    }).then(res => {
      return Promise.resolve(res);
    })
  }

    loggedIn() {
        const token = this.getToken();
        return !!token && !this.isTokenExpired(token);
    }

    isTokenExpired(token) {
        try {
            const decoded = decode(token);
            if (decoded.exp < Date.now() / 1000) {
                return true;
            }
            else
                return false;
        }
        catch (err) {
            return false;
        }
    }

    setToken(idToken) {
        localStorage.setItem(config.TokenName, idToken);
    }

    getToken() {
        return localStorage.getItem(config.TokenName);
    }

    logout() {
        localStorage.removeItem(config.TokenName);
    }

    getProfile() {
        return decode(this.getToken());
    }

    fetch(url, options) {
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        if (this.loggedIn()) {
            headers['Authorization'] = 'Bearer ' + this.getToken();
        }

        return fetch(url, {
            headers,
            ...options
        })
                .then(this._checkStatus)
                .then(response => response.json())
    }

    _checkStatus(response) {
        if (response.status >= 200 && response.status < 300) {
            return response;
        } else {
            return response.json().then(resp => {
              let error = new Error(resp.data.message);
              throw error;
            });
        }
    }

  detectMobileDevice() {
    if( navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
    ){
      return true;
    }
    else {
      return false;
    }
  }
}